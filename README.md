Creare Lab Drone Project
========================

The Creare Automated Plate Transport (lab drone) project uses a canted hexarotor to transport lab dishes. The canted hexarotor design is chosen to allow level transport due to the fully-actuated control system (i.e., pitch and roll are not required to translate in x or y).

This ROS package serves to collect all of the high-level code, configuration, and documentation that is associated with the lab drone project.

## Code Organization Philosophy

The [`creare-labdrone`](https://gitlab.com/mit-acl/creare-labdrone) project relies on use of the base [ACL snap-stack autopilot](https://gitlab.com/mit-acl/fsw/snap-stack). To add the required functionality for this project, relevant repos have been forked from the [`snap-stack`](https://gitlab.com/mit-acl/fsw/snap-stack) group to this group. To ease synchronization between the base repos and these repos, the master branch is left synchronized with the parent repo and a `creare` branch has been created and set as the default branch in each of the forked repos.

Note that other packages `snap-stack` packages that do not require modification are left in the original [`snap-stack`](https://gitlab.com/mit-acl/fsw/snap-stack) group (e.g., `esc_interface`, `sfpro-dev`, etc.). This way, the `creare-labdrone` project can benefit from the updates made to these repos without requiring a manual synchronization (via the `master` branch as mentioned above).

## Getting Started

### ROS network overview

*Review the ROS tutorials and [Network Setup documentation](http://wiki.ros.org/ROS/NetworkSetup)*

The following ROS network setup is assumed. A base station computer (here named **Sikorsky**) is used as the ROS master (via `roscore`). It is connected to the same subnet (via the router) as the motion capture computer and any robots.

We use the Avahi service on the robots to broadcast their hostnames, making them available to other computer via `ssh` as *<hostname>.local*. For example, to connect to **SQ01**, you may `ssh` into it from **Sikorsky** using `ssh root@SQ01.local`.

It is crucial for all machines on the ROS network to have synchronized clocks (especially useful for debugging / plotting data). We use `ntp` for this.

<div align="center">
    <img src=".gitlab/rosnet.png" />
</div>

### Base Station and Simulation Prerequisites

- [`tmux`](https://github.com/tmux/tmux/wiki) (`sudo apt install tmux`)
- [`catkin-tools`](https://catkin-tools.readthedocs.io/en/latest/installing.html) (`sudo apt install python-catkin-tools`)
- [`wstool`](http://wiki.ros.org/wstool#Installation) (`sudo apt install python-wstool`)
- [`joy`](http://wiki.ros.org/joy) (`sudo apt install ros-melodic-joy`)

### Setting up the base station

The following packages (automatically cloned using `wstool`) are required for the labdrone ROS base station.

| Repo                                                                          | Branch            | Purpose                                                  |
|-------------------------------------------------------------------------------|-------------------|----------------------------------------------------------|
| [`mocap`](https://gitlab.com/mit-acl/fsw/mocap)                               | `master`          | ROS drivers for VICON motion capture (ground truth pose) |
| [`snapstack_msgs`](https://gitlab.com/mit-acl/creare-labdrone/snapstack_msgs) | `creare` (forked) | custom ROS msg definitions                               |
| [`labdrone`](https://gitlab.com/mit-acl/creare-labdrone/labdrone)             | `master`          | high-level scripts and launch files                      |
| [`polytraj`](https://gitlab.com/mit-acl/fsw/demos/polytraj)                   | `master`          | Polynomial trajectory generation (via qpOASES) server    |

Create a ROS workspace for Creare packages (e.g., `~/creare_ws`) with the following:

```bash
$ mkdir -p ~/creare_ws/src
$ cd ~/creare_ws
$ catkin init
$ catkin config --extend /opt/ros/melodic --cmake-args -DCMAKE_BUILD_TYPE=Release
```

The necessary packages can be cloned automatically with the following commands:

```bash
$ cd ~/creare_ws/src
$ git clone git@gitlab.com:mit-acl/creare-labdrone/labdrone.git # use https if no ssh keys
$ wstool init
$ wstool merge labdrone/.basestation.rosinstall # use .https variant if no ssh keys
$ wstool update -j8 # can be repeated to keep all repos up-to-date
```

Then, use `catkin build` (anywhere under `~/creare_ws`) to build the code.

### Setting up the Snapdragon Flight Pro flight computer

1. Follow the instructions in [`sfpro-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev) to flash and setup a Snapdragon Flight Pro for use in the MIT ACL flight space.
2. Install the [`ioboard`](https://gitlab.com/mit-acl/creare-labdrone/ioboard) to allow the snap stack to control the motors via PWM ESCs. This creates a shared library and is required before building `snap`.
3. Install the [`snap_pmb`](https://gitlab.com/mit-acl/creare-labdrone/snap_pmb) to allow power managment board communications. This creates another shared library required before building `snap`. The repo README contains instructions for building and deploying this library.
4. Install the [`snap_gpio`](https://gitlab.com/mit-acl/fsw/snap-stack/snap_gpio) to allow GPIO pin communications from the snap stack. This creates another shared library required before building `snap`. The repo README contains instructions for building and deploying this library.

##### Install Flight Code

The following packages (automatically cloned using `wstool`) are required for use on the on-board Snapdragon Flight Pro. They should be cloned into the `sfpro-dev` development environment, e.g., `~/sfpro-dev/workspace/creare_ws/src`.

| Repo                                                                          | Branch            | Purpose                                 |
|-------------------------------------------------------------------------------|-------------------|-----------------------------------------|
| [`snap`](https://gitlab.com/mit-acl/creare-labdrone/snap)                     | `creare` (forked) | low-level autopilot (attitude control)  |
| [`snapstack_msgs`](https://gitlab.com/mit-acl/creare-labdrone/snapstack_msgs) | `creare` (forked) | custom ROS msg definitions              |
| [`outer_loop`](https://gitlab.com/mit-acl/creare-labdrone/outer_loop)         | `creare` (forked) | position / trajectory tracking          |
| [`comm_monitor`](https://gitlab.com/mit-acl/fsw/snap-stack/comm_monitor)      | `master`          | measures ros latency (for debugging)    |
| [`motoralloc`](https://gitlab.com/mit-acl/creare-labdrone/motoralloc)         | `master`          | optimal motor allocation for canted hex |
| [`labdrone`](https://gitlab.com/mit-acl/creare-labdrone/labdrone)             | `master`          | high-level scripts and launch files     |
| [`polytraj`](https://gitlab.com/mit-acl/fsw/demos/polytraj)                   | `master`          | Polynomial trajectory generation (via qpOASES) |

Just as with the base station, `wstool` will be used to clone these repos. However, we will **not** manually invoke `catkin`. The necessary packages can be cloned automatically with the following commands:

```bash
$ cd ~/sfpro-dev/workspace
$ mkdir -p creare_ws/src
$ cd creare_ws/src
$ git clone git@gitlab.com:mit-acl/creare-labdrone/labdrone.git # use https if no ssh keys
$ wstool init
$ wstool merge labdrone/.onboard.rosinstall # use .https variant if no ssh keys
$ wstool update -j8 # can be repeated to keep all repos up-to-date
```

To use this code on the Snapdragon Flight Pro, it must be cross-compiled (this is the purpose of the Docker stuff in `sfpro-dev`). Assuming the instructions in `sfpro-dev` have been followed, code can be built with
```bash
$ cd ~/sfpro-dev
$ ./build.sh workspace/creare_ws --ros # this builds the code and does not require device to be connected
$ ./build.sh workspace/creare_ws --ros --load --ssh labdrone01.local # pushes code via ssh to hostname
```

### Simulation Setup

Software-in-the-loop (SIL) simulation allows us to use the same code regardless of running in hardware or simulation. To obtain the necessary ROS packages, we will use `wstool`. We will create a new workspace (if simulating on the base station, you may use the existing workspace).

```bash
$ mkdir -p ~/creare_ws/src
$ cd ~/creare_ws
$ catkin init
$ catkin config --extend /opt/ros/melodic --cmake-args -DCMAKE_BUILD_TYPE=Release
```

The necessary packages can be cloned automatically with the following commands:

```bash
$ cd ~/creare_ws/src
$ git clone git@gitlab.com:mit-acl/creare-labdrone/labdrone.git # use https if no ssh keys
$ wstool init
$ wstool merge labdrone/.simulation.rosinstall # use .https variant if no ssh keys
$ wstool update -j8 # can be repeated to keep all repos up-to-date
```

Then, use `catkin build` (anywhere under `~/creare_ws`) to build the code.

## Running the Code

After running the code as shown below, your ROS graph (via `rqt_graph`) should look similar to

<div align="center">
    <img src=".gitlab/rqtgraph.png" />
</div>

### On the base station

1. Start a ROS master for the network: `roscore`
2. Start VICON driver: `roslaunch vicon tracker.launch`
3. Labdrone base station: `roslaunch labdrone base_station.launch`

### On the Snapdragon Flight Pro

**Prerequisite:** Ensure that you can `ping labdrone01.local`.

The `remote_start.sh` script will automatically execute the necessary commands. On the base station in a new terminal window run

```bash
$ rosrun labdrone remote_start.sh
```

A tmux session will begin with multiple panes containing various ROS nodes (`snap`, `esc`, `outer_loop`, `mission_manager`, etc.)

### Simulation

When running for SIL simulation, there is no distinction between *base station* and *Snapdragon Flight Pro*. Run the following commands in separate terminals:

1. Start a ROS master: `roscore`
2. Simulate the Snapdragon: `rosrun labdrone remote_start.sh -s`
3. Use `rosrun labdrone test_mission.py` to request the mission manager to transition states. For example, `rosrun labdrone test_mission.py --takeoff`.
