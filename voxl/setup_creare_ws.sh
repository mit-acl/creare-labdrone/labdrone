#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source utils

function usage() {
    echo
    # echo -e "$0 [--ssh HX01.local]"
    echo -e "$0"
    echo
    echo -e "\t This script sets up the Creare snap-stack autopilot on VOXL."
    echo -e "\t By default, adb will be used (USB required)."
    # echo -e "\t Alternatively, --ssh hostname can be used to copy using scp."
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=h
    LONGOPTS=ssh:

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # should we use adb or ssh?
    usessh=false

    # now parse the options in order and nicely split until we see --
    while true; do
      case "$1" in
        -h)
          usage;
          exit;
          ;;
        --ssh)
          usessh=true
          HOST="$2"
          shift 2
          ;;
        --)
          shift
          break
          ;;
        *)
          echo "Invalid options"
          usage
          exit 3
          ;;
      esac
    done
}

parseargs "$@"

# adb only for now
findQualcommDevice

#
# check if snapros is available on voxl
#

hassnapros=$(adb shell which snapros | tr -d '\r')
if [ -z "$hassnapros" ]; then
  echo_error "Could not find 'snapros' on VOXL. Use 'voxl-dev/install_snapros_docker.sh' first."
  exit 1
fi

#
# libsnap_io and snap_ipc packages
#

# create a dir to store all downloaded ipks
adb shell mkdir -p /tmp/ipks

# libsnap_io
adb shell curl -s -L https://gitlab.com/mit-acl/fsw/snap-stack/snapio/libsnap_io/-/jobs/artifacts/main/download?job=build_dev -o /tmp/archive.zip
adb shell unzip -qo /tmp/archive.zip -d /tmp/ipks
# snap_ipc
adb shell curl -s -L https://gitlab.com/mit-acl/creare-labdrone/snap_ipc/-/jobs/artifacts/creare/download?job=build_dev -o /tmp/archive.zip
adb shell unzip -qo /tmp/archive.zip -d /tmp/ipks
# n.b., installs all ipks in the dir
adb shell opkg install --force-reinstall /tmp/ipks/*.ipk

# remove leftovers
adb shell rm -rf /tmp/ipks


#
# voxl-imu-server
#

# Configure IMU0 at 500 Hz sample rate, publishing every 1 sample
adb shell "sed -i /etc/modalai/voxl-imu-server.conf -re 's/(\"imu0_sample_rate_hz\":\s+)\w+/\1500/g'"
adb shell "sed -i /etc/modalai/voxl-imu-server.conf -re 's/(\"imu0_read_every_n_samples\":\s+)\w+/\11/g'"
adb shell systemctl restart voxl-imu-server

#
# Push snap-stack into creare_ws
#

adb shell mkdir -p /home/root/creare_ws/src
adb shell git clone git@gitlab.com:mit-acl/creare-labdrone/labdrone.git /home/root/creare_ws/src/labdrone

#
# fly script
#
adb shell ln -sf /home/root/creare_ws/src/labdrone/voxl/fly /usr/bin/fly

#
# catkin build
#

echo
echo_wht "Use 'wstool' in snapros to clone the remaining creare dependencies, e.g.:"
echo
echo -e "\t~$ snapros # subsequent commands will be in snapros docker"
echo -e "\t/home/root$ cd creare_ws/src"
echo -e "\t/home/root/creare_ws/src$ wstool init"
echo -e "\t/home/root/creare_ws/src$ wstool merge labdrone/.onboard.rosinstall"
echo -e "\t/home/root/creare_ws/src$ wstool up"
echo
echo_wht "Use 'catkin build' to build the creare_ws using snapros, e.g.:"
echo
echo -e "\t~$ snapros # subsequent commands will be in snapros docker"
echo -e "\t/home/root$ cd creare_ws"
echo -e "\t/home/root/creare_ws$ catkin init"
echo -e "\t/home/root/creare_ws$ catkin config --extend /opt/ros/noetic"
echo -e "\t/home/root/creare_ws$ catkin build"
echo
echo_wht "And don't forget to set creare_ws as active for snapros:"
echo
echo -e "\t~$ snapros -s creare_ws"
echo
