%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\documentclass[11pt,a4paper]{article}

\usepackage[bottom]{footmisc}
\usepackage{xltxtra}
\usepackage{amsfonts}
\usepackage{polyglossia}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{dsfont}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{physics}
\usepackage{mathtools}
\usepackage{bm}
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage{float}

\usepackage{graphicx}
\graphicspath{ {figures/} }

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=blue,
}
\urlstyle{same}

% Hack to make ieeeconf and natbib get along
% http://newsgroups.derkeiler.com/Archive/Comp/comp.text.tex/2006-02/msg00834.html
\makeatletter
\let\NAT@parse\undefined
\makeatother
\usepackage[numbers]{natbib}
\setcitestyle{aysep={}} % remove comma
% \usepackage{usebib}
% \bibinput{refs}

\geometry{a4paper,left=15mm,right=15mm,top=20mm,bottom=20mm}
\pagestyle{fancy}
\lhead{Parker C. Lusk}
\chead{Creare Labdrone: Fully-Actuated Hexarotor}
\rhead{\today}
\cfoot{\thepage}

\setlength{\headheight}{23pt}
% \setlength{\parindent}{0.0in}
\setlength{\parskip}{0.03in}

\newtheorem*{prop}{Proposition}
\newtheorem*{defn}{Definition}
\newtheorem*{thm}{Theorem}
\newtheorem*{cor}{Corollary}
\newtheorem*{lem}{Lemma}
\newtheorem*{rem}{Remark}

\DeclarePairedDelimiterX{\inn}[2]{\langle}{\rangle}{#1, #2}

\begin{document}
\section{Overview}
The Creare labdrone hexarotor is a fully-actuated vehicle due to the cant of each of its motors.
The canted motors create thrust vectors in each direction as opposed to only the $z$ direction as with standard multirotors.
The purpose of this document is to model the forces and moments (i.e., the wrench) acting on the vehicle due to the normalized PWM commands sent to each motor.
Additionally, we discuss an optimal control allocation scheme to prevent attitude destabilization and a method for estimating the center of mass of the vehicle.
Further, details on aspects of the Mission Manager and Nest interactions are given.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Coordinate Frames}
To have a consistent discussion on the physical properties of the vehicle, the coordinate frames must be well defined.
Leaving the North-East-Down / front-right-down aerospace convention, we consider the inertial world frame to be an East-North-Up (ENU) coordinate frame with a body front-left-up (flu) coordinate frame, consistent with ROS\footnote{c.f., \href{https://www.ros.org/reps/rep-0105.html}{REP 105} and \href{https://www.ros.org/reps/rep-0103.html\#axis-orientation}{REP 103}} and as shown in Fig.~\ref{fig:geometry}.
We assume the body frame is attached to the geometric center of the vehicle in the $x$-$y$ plane with $z$ on the same plane as the top of the vehicle carbon-fiber frame.
Note that this is unlikely to be the same position as the center of mass, which is modeled as offset from the body origin by $\mathbf{r}_{bM}$.
\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
  \includegraphics[width=\textwidth]{figures/hex_configA.pdf}
  \caption{Configuration A}
  \label{fig:geometry_configA}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
  \includegraphics[width=\textwidth]{figures/hex_configB.pdf}
  \caption{Configuration B}
  \label{fig:geometry_configB}
  \end{subfigure}
  \caption{
  Coordinate frames of two hex configurations.
  The difference in configurations is the orientation of the Snapdragon Flight Pro autopilot and hench the body flu coordinate frame.
  Note that in (b), the vehicle is yawed by $-90$ degrees w.r.t the world ENU coordinate frame.
  In both configurations, motors are numbered the same and spin in the same directions.
  The orange vectors show each motor's thrust direction projected onto the $x$-$y$ plane.
  }
  \label{fig:geometry}
\end{figure}

Configuration B was chosen as the configuration of the labdrone hexarotor so that both the camera and the gripper are aligned with ``forward''.
However, in the analysis that follows, configuration A is considered for simplicity (it seems natural for body $x$ to align with motor 1).

\subsection{Motor Positions}

\begin{figure}[h]
  \centering
  \includegraphics[width=0.4\textwidth]{figures/cant.pdf}
  \caption{
  Diagram of a canted motor.
  }
  \label{fig:cant}
\end{figure}

At the end of each of the six arms, is the \textit{motor mount} frame $m_i$ for $i\in\{1,\dots,6\}$ with its origin attached to the base of the $i^\text{th}$ motor (and assumed to be on the top side of the carbon-fiber frame).
From the body frame, this vector is $\mathbf{r}_{bm_i}$.
The actual thrust point of motor $i$ is offset from the motor mount point due to the cant and height of the motor; from the body frame, each motor's thrust point is located at $\mathbf{r}_{bt_i}$.
We often refer to this as the ``motor position'', but to be precise we really care about the point of thrust due to each motor.

These vectors can be found either through measuring, CAD inspection, or through parameterization.
Here, we consider how to parameterize each thrust point vector.
Expressed in the body frame, we have
\begin{equation}
\mathbf{r}^b_{bt_i} = \mathbf{r}^b_{bm_i} + R^b_{m_i}\mathbf{r}^{m_i}_{m_it_i}.
\end{equation}
Using Fig.~\ref{fig:cant}, we have that (\textit{this isn't an ideal parameterization since $e_1$ and $e_2$ change with $\alpha$})
\begin{equation}
\mathbf{r}^{m_i}_{m_it_i} =
\begin{bmatrix} 0 \\ (-1)^i h \sin\alpha \\ -(h\cos\alpha + \frac{e_1+e_2}{2} + d) \end{bmatrix}.
\end{equation}
Also note that given the arm length $l$, (and assuming body origin and motor mount origins are on the same plane) we can write
\begin{equation}
\mathbf{r}^b_{bm_i} =
R^b_{m_i} r^{m_i}_{bm_i} =
R^b_{m_i} \begin{bmatrix} l \\ 0 \\ 0\end{bmatrix}.
\end{equation}
The rotation matrix $R^b_{m_i}$ encodes the $i^\text{th}$ motor motor mount frame w.r.t the body frame.
Since this vehicle has six equally spaced arms, this rotation matrix can be parameterized for the $i^\text{th}$ motor.
Note that this rotation matrix is the only quantity that changes between configurations (see Fig.~\ref{fig:geometry}).
Thus, depending on the configuration in use, we have
\begin{equation}
R^b_{m_i} \equiv R^{b_\text{A}}_{m_i} =
\begin{bmatrix}
\cos((i-1)\frac{\pi}{3}) & -\sin((i-1)\frac{\pi}{3}) & 0 \\
\sin((i-1)\frac{\pi}{3}) & \hphantom{-}\cos((i-1)\frac{\pi}{3}) & 0 \\
0         & 0          & 1
\end{bmatrix},
\end{equation}
or,
\begin{equation}
R^b_{m_i} \equiv R^{b_\text{B}}_{m_i} =
\begin{bmatrix}
-\sin((i-1)\frac{\pi}{3}) & -\cos((i-1)\frac{\pi}{3}) & 0 \\
\hphantom{-}\cos((i-1)\frac{\pi}{3}) & -\sin((i-1)\frac{\pi}{3}) & 0 \\
0         & 0          & 1
\end{bmatrix}.
\end{equation}
Therefore, the thrust point vector of each motor can be expressed as
\begin{equation}
\mathbf{r}^b_{bt_i} = R^b_{m_i}
\begin{bmatrix} l \\ (-1)^i h \sin\alpha \\ -(h\cos\alpha + \frac{e_1+e_2}{2} + d) \end{bmatrix}.
\end{equation}

For configuration A and $\alpha=20$ degree motor mounts, the thrust point in the $x$-$y$ plane is conveniently calculated by rotating $\begin{bmatrix}l&0\end{bmatrix}^\top$ by $(i-1)60 + (-1)^i3$ degrees for the $i^\text{th}$ motor.

\subsection{Center of Mass}
When calculating the moment due to each of the motor thrusts, we will need to know $\mathbf{r}_{Mt_i}$, the position of each motor's thrust point w.r.t the vehicle's center of mass.
If we know the offset $\mathbf{r}_{bM}$ from the body origin to the center of mass, we can write
\begin{equation}
\mathbf{r}_{Mt_i} = -\mathbf{r}_{bM} + \mathbf{r}_{bt_i}.
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motor Modeling}
Before understanding how the motors exert forces and moments on the vehicle, we must model how normalized PWM values map to thrust and torque from each propulsion unit (i.e., the ESC + motor + propeller combo) using data from the thrust stand.
There are three common ways of modeling how normalized PWM maps to thrust and torque.
Additional accuracy could be gained by adding battery capacity and voltage to the model.

\subsection{Using Motor Speed}\label{sec:motor-speed}
Given a normalized PWM value $u_i\in[0,1]$, we can calculate the speed of each motor using an $n^\text{th}$-order polynomial model
\begin{equation}
\Omega_i = a_n u_i^n + \cdots + a_0.
\end{equation}
Then the thrust and torque created by each motor can then be modeled as the scaled quadratics
\begin{equation}
f_i = k_f \Omega_i^2,
\qquad
\tau_i = k_\tau \Omega_i^2.
\end{equation}
This model is useful when it is possible to measure motor speeds onboard.
These measurements can come from ESC protocols (can be quite noisy) or optical encoders (requires extra hardware).

\subsection{Using Normalized PWM Directly}\label{sec:motor-direct}
Given a normalized PWM value $u_i\in[0,1]$, we can \textit{directly} calculate the thrust and torque of each motor using $n^\text{th}$-order polynomial models
\begin{equation}
f_i = a_n u_i^n + \cdots + a_0,
\qquad
\tau_i = b_n u_i^n + \cdots + b_0.
\end{equation}

\subsection{Using Motor Thrust}\label{sec:motor-thrust}
Given a normalized PWM value $u_i\in[0,1]$, we can first calculate the thrust of each motor using an $n^\text{th}$-order polynomial model
\begin{equation}
f_i = a_n u_i^n + \cdots + a_0,
\end{equation}
and then use thrust to calculate torque through an affine model using the propeller drag constant $c$
\begin{equation}
\tau_i = c f_i.
\end{equation}
This model will be the most advantageous in optimal allocation because we can use $f_i$ as the design variable, and then recover the normalized PWM values $u_i$.
% This is compared to Section~\ref{sec:motor-direct}, which 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Forces and Moments}
%Motor allocation is the process of determining the motor commands based on the desired forces and moments from the outer and inner control loops.
We start by modeling the wrench exerted on the body due to the motor thrusts (in Newtons), $\mathbf{f}\in\mathbb{R}^6$, and due to motor torques (in Newton-meters), $\tau\in\mathbb{R}^6$.
% We assume we have already obtained the mapping from thrust to normalized PWM data (e.g., from dynamometer data) and from thrust to torque (i.e., the propeller drag constant, $c$).
The forces and moments acting on the body (and expressed in the body frame) can be written as
\begin{equation}
\mathbf{F}^b = \mathbf{B}_F\mathbf{f},
\qquad
\mathbf{M}^b = \mathbf{B}_M\mathbf{f} + \mathbf{B}_D \tau,
\end{equation}
where
\begin{equation}
\mathbf{B}_F = \begin{bmatrix} \mathcal{F}^b_1 & \cdots & \mathcal{F}^b_6 \end{bmatrix},
\qquad
\mathbf{B}_M = \begin{bmatrix} \mathcal{M}_1 & \cdots & \mathcal{M}_6 \end{bmatrix},
\qquad
\mathbf{B}_D = \begin{bmatrix} \mathcal{D}_1 & \cdots & \mathcal{D}_6 \end{bmatrix}.
\end{equation}

% \begin{equation}
% \begin{bmatrix}\mathbf{F}\\\mathbf{M}\end{bmatrix} =
% \begin{bmatrix}\mathbf{B}_F\\\mathbf{B}_M\end{bmatrix}
% \begin{bmatrix}f_1\\\vdots\\f_6\end{bmatrix},
% \end{equation}
% where
% \begin{equation}
% \begin{bmatrix}\mathbf{B}_F\\\mathbf{B}_M\end{bmatrix} =
% \begin{bmatrix}\mathcal{F}_1 & \cdots & \mathcal{F}_6 \\ \mathcal{M}_1 & \cdots & \mathcal{M}_6 \end{bmatrix}.
% \end{equation}


To find the maps $\mathcal{F}_i\in\mathbb{R}^3$, $\mathcal{D}_i\in\mathbb{R}^3$, and $\mathcal{M}_i\in\mathbb{R}^3$ for motor $i\in\{1,\dots,6\}$, we must consider the geometry of the vehicle (see Fig.~\ref{fig:geometry}).
Each motor is canted by an angle $\alpha$ in the direction perpendicular to the arm it is attached to.
Consider the direction of thrust due to motor $i$, expressed in the $i^\text{th}$ motor mount frame.
\begin{equation}
\mathcal{F}^{m_i} =
\begin{bmatrix}
0 \\ (-1)^{i-1}\sin\alpha \\ \cos\alpha
\end{bmatrix},
\end{equation}
where the sign alternates sequentially for each motor due to its cant direction.
Therefore, we can express motor $i$'s thrust direction in the (configuration A) body frame as
\begin{equation}
\mathcal{F}^{b}_i =
R^b_{m_i}
\mathcal{F}^{m_i} =
\begin{bmatrix}
(-1)^i \sin((i-1)\frac{\pi}{3}) \sin\alpha \\
(-1)^{i-1} \cos((i-1)\frac{\pi}{3}) \sin\alpha \\
\cos\alpha
\end{bmatrix}.
\end{equation}
This unit length direction is scaled by the thrust $f_i$ to account for motor $i$'s contribution to the total force acting on the vehicle.

The moment map $\mathcal{M}_i$ maps motor thrust $i$ into a moment applied at center of mass of the vehicle and is calculated as
% It is found by considering the moment due to motor thrust and the moment due to propeller drag
\begin{equation}
\mathcal{M}_i = \mathbf{r}_{Mt_i}\times\mathcal{F}^b_i
\end{equation}

The moment map $\mathcal{D}_i$ maps propeller drag torque $i$ into a moment applied at center of mass of the vehicle (in configuration A) and is calculated as
\begin{equation}
\mathcal{D}_i = (-1)^{i-1}\mathcal{F}^b_i =
\begin{bmatrix}
- \sin((i-1)\frac{\pi}{3}) \sin\alpha \\
\phantom{-} \cos((i-1)\frac{\pi}{3}) \sin\alpha \\
(-1)^{i-1} \cos\alpha
\end{bmatrix},
\end{equation}
where the sign on the propeller drag term alternates sequentially due to spin directions defined by the geometry in Fig.~\ref{fig:geometry} (prop drag is opposite the spin direction, motor 1 spins clockwise).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Optimal Allocation}

The vehicle considered is fully actuated, which means it can translate without tilting.
During translation, attitude destabilization is likely to occur in specific directions (typically the body x direction).
To deal with this, we formulate motor allocation as a linear program (LP) that ensures the desired moment is achieved while relaxing the desired forces.

For this LP, we elect to use the motor model in Section~\ref{sec:motor-thrust} so that a $\mathbf{f}\in\mathbb{R}^6$ is the only decision variable.
The model in Section~\ref{sec:motor-direct} is unsuitable as using $\mathbf{u}\in\mathbb{R}^6$ as the decision variable would be nonlinear and using $\mathbf{f}\in\mathbb{R}^6$ and $\tau\in\mathbb{R}^6$ as decision variables would require another optimization to map in order to find a consistent $\mathbf{u}$ vector\footnote{Admittedly, this second optimization could probably be posed as a simple min-norm problem.}.
Hence, we can rewrite the wrench due to motor thrusts as
\begin{equation}
\begin{bmatrix}\mathbf{F} \\ \mathbf{M}\end{bmatrix} =
\begin{bmatrix}
\mathbf{B}_F \\
\mathbf{B}_M + c\mathbf{B}_D
\end{bmatrix}\mathbf{f} =
\begin{bmatrix}
\mathbf{C} \\
\mathbf{D}
\end{bmatrix}\mathbf{f}.
\end{equation}
The optimization problem can then be formulated as
\begin{equation}\label{eq:optimal-allocation}
\begin{aligned}
& \underset{\mathbf{f}}{\text{minimize}}
& & \|\mathbf{F} - \mathbf{C}\mathbf{f} \|^2 \\
& \text{subject to}
& & \mathbf{M} = \mathbf{D}\mathbf{f} \\
&&& 0 \le f_i \le f_\text{max} \quad \forall i
\end{aligned},
\end{equation}
which can be cast with a linear objective using slack variables
\begin{equation}\label{eq:optimal-allocation-lp}
\begin{aligned}
& \underset{\mathbf{s}\in\mathbb{R}^3,\,\mathbf{f}\in\mathbb{R}^6}{\text{minimize}}
& & s_1 + s_2 + s_3 \\
& \text{subject to}
& & \mathbf{F} - \mathbf{C}\mathbf{f} \le \mathbf{s} \\
&&& \mathbf{F} - \mathbf{C}\mathbf{f} \ge -\mathbf{s} \\
&&& \mathbf{M} = \mathbf{D}\mathbf{f} \\
&&& 0 \le f_i \le f_\text{max} \quad \forall i
\end{aligned}.
\end{equation}

We use the GLPK solver to solve \eqref{eq:optimal-allocation-lp} in the attitude loop at 500 Hz.
Written in the GLPK standard form, problem~\eqref{eq:optimal-allocation-lp} becomes
\begin{equation}\label{eq:optimal-allocation-glpk}
\begin{aligned}
& \underset{\mathbf{s}\in\mathbb{R}^3,\,\mathbf{f}\in\mathbb{R}^6}{\text{minimize}}
& & z = s_1 + s_2 + s_3 \\
& \text{subject to}
& & -\mathbf{s} - \mathbf{C}\mathbf{f} \le -\mathbf{F} \\
&&& -\mathbf{s} + \mathbf{C}\mathbf{f} \le \mathbf{F} \\
&&& \mathbf{D}\mathbf{f} = \mathbf{M} \\
&&& 0 \le f_i \le f_\text{max} \quad \forall i
\end{aligned},
\end{equation}
with $\mathbf{F}$, $\mathbf{M}$ as the inputs and $\mathbf{f}$, $\mathbf{s}$ as outputs at each iteration.
The objective provides an indication of if desired forces where not achieved, with the slack variables $\mathbf{s}$ indicating the degree to which each axis did not achieve the desired force.
Note that the problem data of \ref{eq:optimal-allocation-glpk} can be written as
\begin{equation}
\mathbf{A} =
\begin{bmatrix}
-\mathbf{I}  & -\mathbf{C} \\
-\mathbf{I}  & \phantom{-}\mathbf{C} \\
\phantom{-}0 & \phantom{-}\mathbf{D} \\
\end{bmatrix}
\end{equation}
with the inputs to this problem as the bounds.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Trajectory Generation}

Given a set of waypoints, we construct a smooth polynomial trajectory using the min-snap methodology. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mission Management}

\subsection{Nest Interaction}

Using the trajectory generation module as outlined above,


\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{figures/example_mission.pdf}
  \caption{Illustration of a mission.
  The multirotor starts at its dock (green square) and its goal is to pickup a tray from the nest (indicated with \textbf{N}).
  The operator is able to specify waypoints (black dots connected with straight-lines) to constrain the route the multirotor takes to get to the nest.
  A waypoint route is valid if the last waypoint $x_0$ is within the cone specified by $x_1$ and $\gamma$.
  For graceful flight, this path can be smoothed using polynomial trajectory generation (shown as orange curve).
  The additional points $x_1$, $x_2$, $x_3$ are pre-specified with respect to the nest frame and facilitate a transaction with the nest.
  }
  \label{fig:mission-example}
\end{figure}

% \begin{thebibliography}{9}

% \end{thebibliography}

\end{document}
