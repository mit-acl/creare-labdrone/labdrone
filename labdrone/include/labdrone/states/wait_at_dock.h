/**
 * @file wait_at_dock.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class WaitAtDock : public Hover
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 30;
    static constexpr char SNAME[] = "WAIT_AT_DOCK";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    WaitAtDock(MissionManagerPtr mm, const Payload& data = {}) : Hover(mm) {}
    ~WaitAtDock() = default;

    virtual void tick(double now) override
    {
      Hover::tick(now);
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // first, make sure that the next state even wants
      // to be transitioned into from this state
      auto error = next->canTransitionFrom(this);
      if (error.num != Error::NONE) return error;

      if (isStateOfType<DockLanding>(next)) {
        next_state_ = next;
        return {}; // no error
      } else if (isStateOfType<GoToPickup>(next)) {
        // how/where to check if route is valid?
        next_state_ = next;
        return {}; // no error
      } else if (isStateOfType<GoToDropoff>(next)) {
        next_state_ = next;
        return {}; // no error
      } else if (isStateOfType<GoToDock>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      return Hover::requestTransition(next);
    }

  };

} // ns state
} // ns labdrone
} // ns creare
