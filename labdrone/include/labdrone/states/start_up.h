/**
 * @file start_up.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <Eigen/Dense>

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class StartUp : public Wait
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 0;
    static constexpr char SNAME[] = "START_UP";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    StartUp(MissionManagerPtr mm, const Payload& data = {}) : Wait(mm) {}
    ~StartUp() = default;

    virtual void tick(double now) override
    {
      Wait::tick(now);
      
      if (safetyChecksPassed()) {
        next_state_ = createState<Docked>(mm_);
      }
    }

    virtual Goal getGoal() override
    {
      Goal goal;

      // null goal
      goal.p = Eigen::Vector3d::Zero();
      goal.v = Eigen::Vector3d::Zero();
      goal.a = Eigen::Vector3d::Zero();
      goal.Y = goal.r = 0;
      goal.flying = false;

      return goal;
    }

  private:
    bool safetyChecksPassed()
    {

      bool has_truth = (mm_->params().truth_available && mm_->data().truth.t > 0);
      bool has_state = (mm_->data().state.t > 0);

      return has_truth && has_state;
    }
    
  };

} // ns state
} // ns labdrone
} // ns creare
