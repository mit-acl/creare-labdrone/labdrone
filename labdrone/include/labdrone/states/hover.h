/**
 * @file hover.h
 * @brief Low-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/base.h"

namespace creare {
namespace labdrone {
namespace state {

  class Hover : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 4;
    static constexpr char SNAME[] = "HOVER";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Hover(MissionManagerPtr mm, const Payload& data = {}) : Base(mm) {}
    ~Hover() = default;

    virtual void tick(double now) override {}
    
  };

} // ns state
} // ns labdrone
} // ns creare
