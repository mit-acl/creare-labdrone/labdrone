/**
 * @file fast_travel.h
 * @brief Low-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 June 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class FastTravel : public FollowTrajectory
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 6;
    static constexpr char SNAME[] = "FAST_TRAVEL";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  private:
    using NotifyTravelFinishedHandle = std::function<void()>;
  public:
    FastTravel(MissionManagerPtr mm, const Payload& data = {}) : FollowTrajectory(mm)
    {
      // capture trajectory to be followed
      traj_ = data.traj;

      // setup yaw policy
      if (mm_->params().yawlaw_velocity_vector) {
        yawlaw_.policy = FollowTrajectory::YawPolicy::VELOCITY_VECTOR;
      } else {
        yawlaw_.policy = FollowTrajectory::YawPolicy::CONSTANT;
        yawlaw_.Y = utils::wrapToPi(utils::getYaw(mm_->data().state.q));
      }

      // initialize the trajectory to follow
      FollowTrajectory::setTrajectory(traj_, yawlaw_);

      FollowTrajectory::registerNotifyWaypointReachedCb(
        std::bind(&FastTravel::notifyWaypointReachedCb, this, std::placeholders::_1));
    }
    ~FastTravel() = default;

    virtual void tick(double now) override
    {
      if (first_tick_) initialize(now);
      FollowTrajectory::tick(now);
    }

    /**
     * @brief      Allows a caller to register a callback once at end of route
     */
    void registerNotifyTravelFinishedCb(NotifyTravelFinishedHandle cb)
    {
      notifyTravelFinished = cb;
    }

  private:
    FollowTrajectory::YawPolicy yawlaw_; ///< yaw policy
    acl::polytraj::Trajectory traj_; ///< trajectory being followed
    bool first_tick_ = true; ///< has this state been initialized
    NotifyTravelFinishedHandle notifyTravelFinished; ///< callback when done

    void initialize(double now)
    {
      // store for everyone else
      mm_->setdata().traj = traj_;
      mm_->notify(MissionManager::Notification::NewTrajectory);

      first_tick_ = false;
    }

    /**
     * @brief      Called whenever the current waypoint is reached
     *
     * @param[in]  idx   The index of the waypoint that was reached
     */
    void notifyWaypointReachedCb(size_t idx)
    {
      // we are seeking the *next* wp in the current trajectory
      const size_t next_idx = idx + 1;

      // Indicate that we have reached the next waypoint
      mm_->setdata().wp_idx = next_idx;
      mm_->notify(MissionManager::Notification::WaypointReached);

      // check if we have reached the last waypoint of the current route seg
      if (next_idx == traj_.q.size()) {
        notifyTravelFinished();
      }
    }

  };

} // ns state
} // ns labdrone
} // ns creare
