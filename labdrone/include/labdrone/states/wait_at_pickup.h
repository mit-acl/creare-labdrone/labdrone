/**
 * @file wait_at_pickup.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 21 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class WaitAtPickup : public Hover
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 50;
    static constexpr char SNAME[] = "WAIT_AT_PICKUP";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    WaitAtPickup(MissionManagerPtr mm, const Payload& data = {}) : Hover(mm) {}
    ~WaitAtPickup() = default;

    virtual void tick(double now) override
    {
      Hover::tick(now);
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // first, make sure that the next state even wants
      // to be transitioned into from this state
      auto error = next->canTransitionFrom(this);
      if (error.num != Error::NONE) return error;

      if (isStateOfType<Pickup>(next)) {
        next_state_ = next;
        return {}; // no error
      } else if (isStateOfType<GoToDropoff>(next)) {
        next_state_ = next;
        return {}; // no error
      } else if (isStateOfType<GoToDock>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      return Hover::requestTransition(next);
    }

  };

} // ns state
} // ns labdrone
} // ns creare
