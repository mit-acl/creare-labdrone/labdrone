/**
 * @file dock_takeoff.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <cmath>

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class DockTakeoff : public Takeoff
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 20;
    static constexpr char SNAME[] = "DOCK_TAKEOFF";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    DockTakeoff(MissionManagerPtr mm, const Payload& data = {}) : Takeoff(mm, data) {}
    ~DockTakeoff() = default;
    
    /**
     * @brief      Slowly increase the desired position of the vehicle
     *             until setpoint altitude (i.e., takeoff) is reached.
     */
    virtual void tick(double now) override
    {
      Takeoff::tick(now);

      if (Takeoff::complete()) {
        next_state_ = createState<WaitAtDock>(mm_);
      }
    }

  };

} // ns state
} // ns labdrone
} // ns creare
