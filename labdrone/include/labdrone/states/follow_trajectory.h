/**
 * @file follow_trajectory.h
 * @brief Low-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 21 May 2020
 */

#pragma once

#include <vector>

#include <polytraj/polytraj.h>

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class FollowTrajectory : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 5;
    static constexpr char SNAME[] = "FOLLOW_TRAJECTORY";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  private:
    using NotifyWaypointReachedHandle = std::function<void(size_t)>;
  public:
    /**
     * @brief      Information deciding how the yaw of veh should be set
     */
    struct YawPolicy {
      static constexpr uint8_t VELOCITY_VECTOR = 0;
      static constexpr uint8_t CONSTANT = 1;
      uint8_t policy;
      double Y; ///< constant heading angle (in radians)

      YawPolicy()
      {
        policy = VELOCITY_VECTOR;
        Y = 0.0;
      }
    };
  public:
    FollowTrajectory(MissionManagerPtr mm, const Payload& data = {}) : Base(mm) {}
    ~FollowTrajectory() = default;

    virtual void tick(double now) override
    {
      // don't do anything until we have a valid trajectory
      if (!traj_.valid) return;

      if (first_tick_) initialize(now);

      // timing stuff
      static double last = now;
      const double dt = now - last;
      last = now;

      //
      // First correct heading to be near desired
      //

      // set Y and r according to yaw policy

      // don't start trajectory yet
      if (!heading_initialized_) {
        // do slerp stuff
        yawinit_->getYawCmd(now, goal_.Y, goal_.r);

        if (yawinit_->getYawError(mm_->data().state.q) < mm_->params().yaw_init_err_thr) {
          heading_initialized_ = true;
          t0_ = now; // initialize trajectory timing

          goal_.Y = yawinit_->getDesiredYaw();
          goal_.r = 0.0;
        } else {
          return; // don't do anything else until heading is good
        }
      }

      //
      // Sample trajectory to create a goal for this timestep
      //

      const double t = std::min(now - t0_, traj_.tf);
      evalTraj(t, goal_);

      // set desired heading / yawrate
      applyYawPolicy(dt, goal_);

      //
      // Check trajectory tracking quality
      //

      const auto& state = mm_->data().state;

      // if uav is outside of delta ball around curr. goal (high tracking err)
      if ((state.p - goal_.p).norm() > mm_->params().traj_err_radius) {
        // throw some kind of error / cancel the trajectory
      }

      //
      // Check which waypoint is active
      //

      const double wpt = traj_.qt[wp_idx_]; // time of current des waypoint
      if (t >= wpt) {
        // we have achieved the current desired waypoint
        notifyWaypointReached(wp_idx_);
        wp_idx_++;
      }

    }

    virtual Goal getGoal() override
    {
      if (!traj_.valid) return Base::getGoal();
      return goal_;
    }

    /**
     * @brief      Copies the route to fly
     */
    void setTrajectory(const acl::polytraj::Trajectory& traj, const YawPolicy& policy = {})
    {
      // set the traj and indicate that we need reinitialization
      traj_ = traj;
      first_tick_ = true;
      heading_initialized_ = false;
      yaw_policy_ = policy;
    }

    /**
     * @brief      Allows a caller to register a callback once at next waypoint
     */
    void registerNotifyWaypointReachedCb(NotifyWaypointReachedHandle cb)
    {
      notifyWaypointReached = cb;
    }
    
  private:
    bool first_tick_ = true; ///< has this state been initialized
    double t0_; ///< time [s] of first tick
    Goal goal_; ///< desired goal along the trajectory
    YawPolicy yaw_policy_; ///< desired yaw policy
    NotifyWaypointReachedHandle notifyWaypointReached; ///< callback
    // Route route_; ///< copy of desired route to take
    acl::polytraj::Trajectory traj_; ///< copy of desired trajectory
    int16_t wp_idx_; ///< current desired waypoint
    std::vector<double> wp_times_; ///< expected traj time at each waypoint
    bool heading_initialized_ = false; ///< is heading where it needs to be?
    std::unique_ptr<utils::YawRegulator> yawinit_; ///< yaw initialization

    void initialize(double now)
    {
      // we will begin seeking the first waypoint in the list
      wp_idx_ = 0; // (should immediately move to wp_idx_ = 1)

      goal_ = mm_->goal();
      t0_ = now;
      first_tick_ = false;

      // Based on yaw policy, figure out initial yaw
      determineInitialDesiredYaw(now);
    }

    /**
     * @brief      Evaluates the trajectory at a specific time t
     *
     * @param[in]  t     Time to evaluate trajectory at
     * @param      goal  The resulting goal
     */
    inline void evalTraj(double t, Goal& goal)
    {
      Eigen::Matrix3d X = traj_.eval(t);
      goal.p = X.col(0);
      goal.v = X.col(1);
      goal.a = X.col(2);
      // goal.j = X.col(3);
    }

    void determineInitialDesiredYaw(double now)
    {
      double yawdes = 0.0;

      if (yaw_policy_.policy == YawPolicy::VELOCITY_VECTOR) {
        static constexpr double lookahead = 0.25;
        Goal goal;
        evalTraj(lookahead, goal);
        yawdes = std::atan2(goal.v.y(), goal.v.x());

      } else if (yaw_policy_.policy == YawPolicy::CONSTANT) {
        yawdes = yaw_policy_.Y;

      } else {
        // shouldn't be here...
      }

      // initialize yaw initializer (first tracks heading, then trajectory)
      const Eigen::Quaterniond qdes = utils::fromYaw(yawdes);
      yawinit_.reset(new utils::YawRegulator(qdes, mm_->params().yaw_rate));
      yawinit_->setInitialYaw(now, mm_->data().state.q);
    }

    /**
     * @brief      Applies a specified yaw policy to the provided goal.
     *
     *             See https://github.com/ethz-asl/mav_trajectory_generation/issues/98
     *             and https://github.com/ethz-asl/mav_voxblox_planning/blob/dc4c653934826999442babc3754383626620cc51/mav_planning_common/src/yaw_policy.cpp#L28-L174
     *
     * @param      goal  The goal to apply the yaw policy to
     */
    void applyYawPolicy(double dt, Goal& goal)
    {

      if (yaw_policy_.policy == YawPolicy::VELOCITY_VECTOR) {

        // threshold for x-y velocity to be non-zero
        static constexpr double kMinVelocityNorm = 0.1;

        if (goal.v.head<2>().norm() > kMinVelocityNorm) {
          const double yaw = utils::getYaw(mm_->data().state.q);
          const double yawdes = std::atan2(goal.v.y(), goal.v.x());

          static double last_yawdes = yawdes;
          const double r = (utils::wrapToPi(yawdes) - utils::wrapToPi(last_yawdes)) / dt;
          last_yawdes = yawdes;

          goal.Y = yawdes;
          goal.r = utils::clamp(r, -mm_->params().yaw_rate, mm_->params().yaw_rate);
        } else {
          // no velocity vector to extract yaw from---maintain last heading
          goal.r = 0.0;
        }

      } else if (yaw_policy_.policy == YawPolicy::CONSTANT) {

        // threshold on yaw error
        static constexpr double kYawErr = 0.01;
        const double yaw = utils::getYaw(mm_->data().state.q);
        const double yawdes = yaw_policy_.Y;

        // if (utils::wrapToPi(yaw - yawdes) > kYawErr) {
          static double last_yawdes = yawdes;
          const double r = (dt>0) ? (utils::wrapToPi(yawdes) - utils::wrapToPi(last_yawdes)) / dt : 0;
          last_yawdes = yawdes;

          goal.Y = yawdes;
          goal.r = utils::clamp(r, -mm_->params().yaw_rate, mm_->params().yaw_rate);
        // } else {
        //   // no velocity vector to extract yaw from---maintain last heading
        //   goal.r = 0.0;
        // }

      } else {
        // shouldn't be here...
      }
    }

  };

} // ns state
} // ns labdrone
} // ns creare
