/**
 * @file go_to_dropoff.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 6 July 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class GoToDropoff : public FastTravel
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 70;
    static constexpr char SNAME[] = "GO_TO_DROPOFF";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    GoToDropoff(MissionManagerPtr mm, const Payload& data = {}) : FastTravel(mm, data)
    {
      // set params for pickup mode

      FastTravel::registerNotifyTravelFinishedCb(
        std::bind(&GoToDropoff::notifyTravelFinishedCb, this));
    }
    ~GoToDropoff() = default;

    virtual void tick(double now) override
    {
      FastTravel::tick(now);
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // first, make sure that the next state even wants
      // to be transitioned into from this state
      auto error = next->canTransitionFrom(this);
      if (error.num != Error::NONE) return error;

      if (isStateOfType<ReroutePending>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      return FastTravel::requestTransition(next);
    }

  private:

    void notifyTravelFinishedCb()
    {
      next_state_ = createState<WaitAtDropoff>(mm_);
    }

  };

} // ns state
} // ns labdrone
} // ns creare
