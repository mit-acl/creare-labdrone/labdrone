/**
 * @file docked.h
 * @brief High-level Shutdown state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/wait.h"

namespace creare {
namespace labdrone {
namespace state {

  class Shutdown : public Wait
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 120;
    static constexpr char SNAME[] = "SHUTDOWN";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Shutdown(MissionManagerPtr mm, const Payload& data = {}) : Wait(mm) {}
    ~Shutdown() = default;

    virtual void tick(double now) override
    {
      Wait::tick(now);
    }

    virtual Goal getGoal() override
    {
      Goal goal = mm_->goal();
      goal.flying = false;
      return goal;
    }

  };

} // ns state
} // ns labdrone
} // ns creare
