/**
 * @file pickup.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 25 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Pickup : public Transaction
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 60;
    static constexpr char SNAME[] = "PICKUP";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Pickup(MissionManagerPtr mm, const Payload& data = {}) : Transaction(mm, data, true) 
    {
      Transaction::registerNotifyTransactionCompleteCb(
        std::bind(&Pickup::notifyTransactionCompleteCb, this));
    }
    ~Pickup() = default;

    virtual void tick(double now) override
    {
      Transaction::tick(now);
    }

  private:

    void notifyTransactionCompleteCb()
    {
      mm_->setLoaded(true);
      next_state_ = createState<WaitAtPickup>(mm_);
    }
  };

} // ns state
} // ns labdrone
} // ns creare
