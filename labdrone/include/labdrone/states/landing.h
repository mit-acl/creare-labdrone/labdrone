/**
 * @file landing.h
 * @brief Low-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/base.h"

namespace creare {
namespace labdrone {
namespace state {

  class Landing : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 9;
    static constexpr char SNAME[] = "LANDING";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Landing(MissionManagerPtr mm, const Payload& data = {}) : Base(mm)
    {
      land_alt_ = data.pose.p.z();
    }
    ~Landing() = default;
    
    /**
     * @brief      Slowly decrease the desired position of the vehicle
     *             until landed.
     */
    virtual void tick(double now) override
    {
      if (first_tick_) initialize(now);

      if (isLanded()) {
        cleanup();
      } else {
        // choose between fast landing and slow landing
        const double landing_fast_th = mm_->params().landing_fast_threshold
                                        + land_alt_;
        const double dec = (mm_->data().state.p.z() > landing_fast_th) ?
                              mm_->params().landing_fast_dec
                                : mm_->params().landing_slow_dec;

        goal_.p.z() = utils::clamp(goal_.p.z() - dec, 0.0, mm_->params().bounds.z_max);
      }

    }

    virtual Goal getGoal() override
    {
      return goal_;
    }

  protected:
    bool isLanded()
    {
      static constexpr double EPS = 0.005;
      return ((mm_->data().state.p.z() - land_alt_) < EPS);
    }

  private:
    bool first_tick_ = true; ///< has this state been initialized
    double takeoff_time_; ///< time [s] that takeoff cmd was first received
    double land_alt_; ///< z pos [m] where multirotor considered landed
    Goal goal_; ///< goal used to increment position for takeoff

    void initialize(double now)
    {
      goal_ = mm_->goal();
      goal_.v = Eigen::Vector3d::Zero();
      goal_.a = Eigen::Vector3d::Zero();
      goal_.r = 0;

      first_tick_ = false;
    }

    void cleanup()
    {
      // reset CoM to nominal (from snap params)
      // this is important if mission interrupted btwn pickup & dropoff
      mm_->updateCoM(mm_->params().nominal_rbM);
    }
  };

} // ns state
} // ns labdrone
} // ns creare
