/**
 * @file go_to_dock.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 30 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class GoToDock : public FastTravel
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 100;
    static constexpr char SNAME[] = "GO_TO_DOCK";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    GoToDock(MissionManagerPtr mm, const Payload& data = {}) : FastTravel(mm, data)
    {
      yawreg_.reset(new utils::YawRegulator(data.pose.q, mm_->params().yaw_rate));

      FastTravel::registerNotifyTravelFinishedCb(
        std::bind(&GoToDock::notifyTravelFinishedCb, this));
    }
    ~GoToDock() = default;

    virtual void tick(double now) override
    {
      // once yaw is finished, stop trying to follow route and focus
      // on setting the correct heading for dock landing
      if (route_finished_) {

        if (!yaw_initialized_) {
          yawreg_->setInitialYaw(now, mm_->data().state.q);
          yaw_initialized_ = true;
        }

        yawreg_->getYawCmd(now, goal_.Y, goal_.r);

        if (yawreg_->getYawError(mm_->data().state.q) < 0.05) {
          next_state_ = createState<WaitAtDock>(mm_);
          goal_.r = 0.0;
        }

      } else {
        FastTravel::tick(now);
      }
    }

    virtual Goal getGoal() override
    {
      if (route_finished_) {
        return goal_;
      } else {
        return FastTravel::getGoal();
      }
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // first, make sure that the next state even wants
      // to be transitioned into from this state
      auto error = next->canTransitionFrom(this);
      if (error.num != Error::NONE) return error;

      if (isStateOfType<ReroutePending>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      return FastTravel::requestTransition(next);
    }

  private:
    bool route_finished_ = false; ///< Once we are at the goal, set heading
    bool yaw_initialized_ = false; ///< has the initial yaw been set?
    Goal goal_; ///< desired goal to use at dock to correct heading
    std::unique_ptr<utils::YawRegulator> yawreg_; ///< final desired yaw cmds

    void notifyTravelFinishedCb()
    {
      // initialize next step: regulating heading for dock landing
      route_finished_ = true;
      goal_ = mm_->goal();
    }

  };

} // ns state
} // ns labdrone
} // ns creare
