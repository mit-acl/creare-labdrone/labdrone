/**
 * @file wait.h
 * @brief Low-level Wait state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Wait : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 3;
    static constexpr char SNAME[] = "WAIT";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Wait(MissionManagerPtr mm, const Payload& data = {}) : Base(mm) {}
    ~Wait() = default;

    virtual void tick(double now) override
    {
      // nop
    }
    
  };

} // ns state
} // ns labdrone
} // ns creare
