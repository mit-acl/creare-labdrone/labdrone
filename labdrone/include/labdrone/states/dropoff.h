/**
 * @file dropoff.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 6 July 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Dropoff : public Transaction
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 90;
    static constexpr char SNAME[] = "Dropoff";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Dropoff(MissionManagerPtr mm, const Payload& data = {}) : Transaction(mm, data, false) 
    {
      Transaction::registerNotifyTransactionCompleteCb(
        std::bind(&Dropoff::notifyTransactionCompleteCb, this));
    }
    ~Dropoff() = default;

    virtual void tick(double now) override
    {
      Transaction::tick(now);
    }

  private:

    void notifyTransactionCompleteCb()
    {
      mm_->setLoaded(false);
      next_state_ = createState<WaitAtDropoff>(mm_);
    }
  };

} // ns state
} // ns labdrone
} // ns creare
