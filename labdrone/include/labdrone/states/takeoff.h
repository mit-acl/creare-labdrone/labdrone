/**
 * @file takeoff.h
 * @brief Low-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <cmath>

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Takeoff : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 2;
    static constexpr char SNAME[] = "TAKEOFF";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Takeoff(MissionManagerPtr mm, const Payload& data = {}) : Base(mm)
    {
      alt_setpoint_ = mm_->params().nominal_alt;
    }
    ~Takeoff() = default;
    
    /**
     * @brief      Slowly increase the desired position of the vehicle
     *             until setpoint altitude (i.e., takeoff) is reached.
     */
    virtual void tick(double now) override
    {
      if (first_tick_) initialize(now);

      if (hasSpunUp(now)) {

        if (complete())
        {
          // takeoff is complete
          next_state_ = createState<Wait>(mm_);
        } else {

          if (USE_VEL_TAKEOFF) {

            // evaluate takeoff trajecotry at current timestep
            const double t = now - (takeoff_time_ + mm_->params().spinup_time);
            const auto s = takeoff_traj_->eval(t);
            goal_.p.z() = s.p;
            goal_.v.z() = s.v;
            goal_.a.z() = s.a;
            goal_.j.z() = s.j;

            // check if we have evaluated the entire trajectory
            complete_ = (t >= takeoff_traj_->getTotalTime());

          } else {
            // Increment the z cmd each timestep for a smooth takeoff.
            // This is essentially saturating tracking error so actuation is low.
            goal_.p.z() = utils::clamp(goal_.p.z() + mm_->params().takeoff_vel*0.01, 0.0, alt_setpoint_);
          }

        }

      }
    }

    virtual Goal getGoal() override
    {
      return goal_;
    }

    /**
     * @brief      Indicates if takeoff to requested altitude is complete
     */
    bool complete()
    {
      if (USE_VEL_TAKEOFF) {
        return complete_;
      } else {
        static constexpr double EPS = 0.100;
        return ((std::abs(goal_.p.z() - mm_->data().state.p.z()) < EPS) &&
                std::abs(goal_.p.z() - alt_setpoint_) < EPS);
      }
    }

  private:
    bool first_tick_ = true; ///< has this state been initialized
    double takeoff_time_; ///< time [s] that takeoff cmd was first received
    double alt_setpoint_; ///< desired altitude once takeoff is completed
    Goal goal_; ///< goal used to increment position for takeoff
    bool complete_ = false; ///< flag if trajectory completed
    std::unique_ptr<utils::SmoothVelocityProfileTrajectory> takeoff_traj_;

    static constexpr bool USE_VEL_TAKEOFF = true;

    void initialize(double now)
    {
      // Capture the pose of the multirotor before taking off. This pose is
      // used for GoToDock from Manual. Otherwise, GoToDock requires a pose.
      mm_->setdata().initial_pose = mm_->data().state;

      goal_.p = mm_->data().state.p;
      goal_.v = Eigen::Vector3d::Zero();
      goal_.a = Eigen::Vector3d::Zero();
      goal_.j = Eigen::Vector3d::Zero();
      goal_.Y = utils::getYaw(mm_->data().state.q);
      goal_.r = 0;
      goal_.flying = true;

      takeoff_time_ = now;
      first_tick_ = false;

      // initialize smooth velocity takeoff trajectory
      takeoff_traj_.reset(new utils::SmoothVelocityProfileTrajectory(
                      mm_->data().state.p.z(), alt_setpoint_,
                      mm_->params().takeoff_vel, 0.5, 1.0));
    }

    bool hasSpunUp(double now) const
    {
      return (now - takeoff_time_ >= mm_->params().spinup_time);
    }

  };

} // ns state
} // ns labdrone
} // ns creare
