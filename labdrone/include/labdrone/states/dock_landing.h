/**
 * @file dock_landing.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Wait;
  class Docked;

  class DockLanding : public Landing
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 110;
    static constexpr char SNAME[] = "DOCK_LANDING";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    DockLanding(MissionManagerPtr mm, const Payload& data = {}) : Landing(mm, data)
    {
      mm_->actuateGripper(false);
    }
    ~DockLanding() = default;
    
    virtual void tick(double now) override
    {
      Landing::tick(now);

      if (isLanded()) {
        next_state_ = createState<Docked>(mm_);
      }

    }


  };

} // ns state
} // ns labdrone
} // ns creare
