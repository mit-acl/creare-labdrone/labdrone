/**
 * @file docked.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Docked : public Wait
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 10;
    static constexpr char SNAME[] = "DOCKED";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Docked(MissionManagerPtr mm, const Payload& data = {}) : Wait(mm) {}
    ~Docked() = default;

    virtual void tick(double now) override
    {
      Wait::tick(now);
    }

    virtual Goal getGoal() override
    {
      Goal goal = mm_->goal();
      goal.flying = false;
      return goal;
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // first, make sure that the next state even wants
      // to be transitioned into from this state
      auto error = next->canTransitionFrom(this);
      if (error.num != Error::NONE) return error;

      if (isStateOfType<Shutdown>(next)) {
        next_state_ = next;
        return {}; // no error
      } else if (isStateOfType<DockTakeoff>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      return Wait::requestTransition(next);
    }

  };

} // ns state
} // ns labdrone
} // ns creare
