/**
 * @file emergency_landing.h
 * @brief High-level state
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Wait;
  class Docked;

  class EmergencyLanding : public Landing
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 230;
    static constexpr char SNAME[] = "EMERGENCY_LANDING";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    EmergencyLanding(MissionManagerPtr mm, const Payload& data = {}) : Landing(mm) {}
    ~EmergencyLanding() = default;
    
    virtual bool alwaysAllowed() const override { return true; }
    
    virtual void tick(double now) override
    {
      Landing::tick(now);

      if (isLanded()) {
        // TODO Need a 'manual intervention required' error state.
        next_state_ = createState<Shutdown>(mm_);
      }

    }


  };

} // ns state
} // ns labdrone
} // ns creare
