/**
 * @file obstacle_detected.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 20 Aug 2021
 */

#pragma once

#include <chrono>
#include <memory>
#include <vector>

#include <traj_gen/traj_gen.hpp>

#include "labdrone/states/all.h"


namespace creare {
namespace labdrone {
namespace state {

  class ObstacleDetected : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 240;
    static constexpr char SNAME[] = "OBSTACLE_DETECTED";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  private:
    /// \brief Convenience aliases
    static constexpr int dim = 3;
    using PolyTraj = trajgen::PolyTrajGen<double, dim>;
    using PolyTrajPtr = std::shared_ptr<PolyTraj>;
    using FixPin = trajgen::FixPin<double, dim>;
    using LoosePin = trajgen::LoosePin<double, dim>;
    using PinPtr = trajgen::Pin<double, dim>*;
  public:
    ObstacleDetected(MissionManagerPtr mm, const Payload& data = {}) : Base(mm, data)
    {
      // save the pose of detected obstacle
      mm_->setdata().obstacle = data.pose;

      // indicate that an obstacle was detected and we are currently
      // handling it. This will be reset later... TODO
      mm_->setdata().obstacle_detected = true;

      // trajectory generator settings
      const double t0 = 0.0;
      const double tf = mm_->params().stoptraj_tf;
      trajgen::time_knots<double> ts{ t0, tf };
      uint poly_order = 8, max_conti = 4;
      trajgen::PolyParam pp(poly_order, max_conti, trajgen::ALGORITHM::END_DERIVATIVE);  // or ALGORITHM::POLY_COEFF

      const auto t1 = std::chrono::high_resolution_clock::now();
      polytraj_ = generateStopTraj(pp, mm_->setdata().obstacle, ts);
      const auto t2 = std::chrono::high_resolution_clock::now();
      const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
      const double elapsed = static_cast<double>(duration.count()) / 1e6;
      std::cout << "solved in " << elapsed << " seconds." << std::endl;

      const Eigen::Vector3d w_obs = T_wb_ * mm_->setdata().obstacle.p;
      mm_->notifyObstruction(polytraj_, w_obs, T_wb_);
    }
    ~ObstacleDetected() = default;

    // transitions into this state are always allowed from any state,
    // unless barred by canTransitionFrom.
    virtual bool alwaysAllowed() const override { return true; }

    virtual void tick(double now) override
    {
      if (first_tick_) initialize(now);

      const double t = now - t0_;
      if (t < mm_->params().stoptraj_tf && polytraj_) {
        const Eigen::Vector3d b_pos = polytraj_->eval(t, 0).transpose();
        const Eigen::Vector3d b_vel = polytraj_->eval(t, 1).transpose();
        const Eigen::Vector3d b_accel = polytraj_->eval(t, 2).transpose();
        goal_.p = T_wb_ * b_pos;
        goal_.v = T_wb_.linear() * b_vel;
        goal_.a = T_wb_.linear() * b_accel;
        goal_.j = Eigen::Vector3d::Zero();
        goal_.r = 0;
      }

      // detect when at goal (just assume that drone gets to goal by tf)
      if (t > mm_->params().stoptraj_tf) {
        if (mm_->params().stoptraj_autoreturn) {
          Payload data;
          data.pose.q = mm_->data().initial_pose.q; // set yaw for landing
          // create straight-line trajectory to dock
          std::vector<Waypoint> waypoints;
          waypoints.push_back(mm_->data().initial_pose.p);
          waypoints.back().z() = mm_->params().nominal_alt;
          data.traj = utils::createTrajectory(mm_->data().state.p, waypoints,
                                              mm_->params().traj_fast_vmax,
                                              mm_->params().traj_fast_amax);
          next_state_ = createState<GoToDock>(mm_, data);
        }
      }
    }

    virtual Goal getGoal() override
    {
      return goal_;
    }

    virtual Error requestTransition(BasePtr next) override
    {
      if (isStateOfType<Manual>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      Error error;
      error.num = Error::TRANSITION_DISALLOWED;
      error.msg = "Cannot request transition once obstacle is detected.";
      return error;
    }

    virtual Error canTransitionFrom(const Base * const current) const override
    {
      Error error;
      if (!mm_->goal().flying) {
        error.num = Error::TRANSITION_DISALLOWED;
        error.msg = "Cannot transition into OBSTACLE_DETECTED until flying";
      }
      return error;
    }

  private:
    double t0_; ///< start with t = 0 for poly eval
    bool first_tick_ = true; ///< has this state been initialized
    Eigen::Affine3d T_wb_; ///< pose of body w.r.t world at trajgen time
    PolyTrajPtr polytraj_; ///< polynomial trajectory used for stopping
    Goal goal_; ///< desired goal from joystick

    void initialize(double now)
    {
      goal_ = mm_->goal();
      goal_.v = Eigen::Vector3d::Zero();
      goal_.a = Eigen::Vector3d::Zero();
      goal_.j = Eigen::Vector3d::Zero();
      goal_.r = 0;
      first_tick_ = false;
      t0_ = now;
    }

    PolyTrajPtr generateStopTraj(const trajgen::PolyParam& pp, const Pose& b_Obs, const trajgen::time_knots<double>& ts)
    {
      // current position and velocity of body, expressed in the body frame
      Eigen::Vector3d b_current_pos = Eigen::Vector3d::Zero();
      Eigen::Vector3d b_current_vel = mm_->data().state.q.inverse() * mm_->data().twist.v;
      T_wb_ = mm_->data().state.toAffine();

      const double t0 = ts.front();
      const double tf = ts.back();

      /////// Initial State
      FixPin x0(t0, 0, b_current_pos);
      FixPin xdot0(t0, 1, b_current_vel);
      FixPin xddot0(t0, 2, Eigen::Vector3d::Zero());

      /////// Final State (stop condition)
      // FixPin xf(0.0f, 0, Vector<double, dim>(0, 0, 0));
      FixPin xdotf(tf, 1, Eigen::Vector3d::Zero());
      FixPin xddotf(tf, 2, Eigen::Vector3d::Zero());

      /////// Corridor Constraint for "all t" (discretization)
      std::vector<LoosePin> all_passCube;
      const Eigen::Vector3d minBox = -Eigen::Vector3d(2, 2, 2);
      const double inflatedObsX_inbody = b_Obs.p.x() - mm_->params().stoptraj_vehradius;
      const Eigen::Vector3d maxBox =  Eigen::Vector3d(inflatedObsX_inbody, 2, 2);
      const double dt = mm_->params().stoptraj_corrdt;
      for (double t = t0; t < tf; t = t + dt) {
        LoosePin passCube(t, 0, minBox, maxBox);
        all_passCube.push_back(passCube);
      }

      ///////
      std::vector<PinPtr> pinSet;  // to prevent downcasting slicing, vector of pointers
      pinSet.push_back(&x0);
      pinSet.push_back(&xdot0);
      pinSet.push_back(&xddot0);
      pinSet.push_back(&xdotf);
      pinSet.push_back(&xddotf);

      for (size_t i=0; i<all_passCube.size(); i++) {
        pinSet.push_back(&all_passCube[i]);
      }

      /////// Create the object
      PolyTrajPtr pTraj = std::make_unique<PolyTraj>(ts, pp);

      static constexpr bool verbose = false;

      pTraj->setDerivativeObj(Eigen::Vector3d(0, 1, 1)); // Velocity, acceleration, jerk
      pTraj->addPinSet(pinSet);
      const bool isSolved = pTraj->solve(verbose);

      if (!isSolved) {
        std::cout << "Failed. " << std::endl;
        return nullptr;
      }

      return pTraj;
    }

  };

} // ns state
} // ns labdrone
} // ns creare
