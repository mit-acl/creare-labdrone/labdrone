/**
 * @file base_state.h
 * @brief Abstract state interface
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <memory>
#include <typeinfo>

#include <polytraj/polytraj.h>

#include "labdrone/mission_manager.h"
#include "labdrone/data_store.h"
#include "labdrone/utils.h"

namespace creare {
namespace labdrone {
namespace state {

  namespace utils = creare::labdrone::utils;

  /**
   * @brief      Incoming data payload associated with a state
   */
  struct Payload {
    // Route route; ///< the loaded route being flown
    acl::polytraj::Trajectory traj;
    Pose pose; ///< the pose associated with the next transaction or dock landing
  };

  class Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 1;
    static constexpr char SNAME[] = "BASE";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Base(MissionManagerPtr mm, const Payload& data = {}) : mm_(mm) {}
    ~Base() = default;

    /**
     * @brief      Allow state to handle any time-varying processing
     * 
     * @param[in]  now  Current time in seconds
     */
    virtual void tick(double now) = 0;

    /**
     * @brief      Indicates if transitions into this state are always allowed.
     * 
     *             For example: Manual, Emergency Landing, etc.
     *
     */
    virtual bool alwaysAllowed() const
    {
      return false;
    }

    /**
     * @brief      Handle external request for state transition.
     *
     * @param[in]  next  The proposed next state
     *
     * @return     An error if transition is disallowed.
     */
    virtual Error requestTransition(BasePtr next)
    {
      Error error;

      if (next->alwaysAllowed()) {
        next_state_ = next;
      } else {
        // by default, no transitions are allowed
        error.num = Error::TRANSITION_DISALLOWED;
        error.msg = "Not a valid transition";
      }

      return error;
    }

    /**
     * @brief      Returns an error if the transition from
     *             the current state to this state invalid.
     */
    virtual Error canTransitionFrom(const Base * const current) const
    {
      return {}; // no error
    }

    /**
     * @brief      Return the goal associated with this state. By default,
     *             each state simply returns the current goal (identity).
     *
     * @return     The goal that will get sent to the lower-level autopilot.
     */
    virtual Goal getGoal()
    {
      return mm_->goal();
    }

    BasePtr next_state_; ///< non-null if mm should transition

  protected:
    MissionManagerPtr mm_; ///< ref to mission manager for data, etc.

  };

  /**
   * @brief      Creates a new state. This templated function is a hack to
   *             allow header implementation of states while avoiding the
   *             "invalid use of incomplete type" error which occurs when
   *             using forward declaration to try and break cyclic deps.
   *
   * @param[in]  mm    Mission manager ptr
   * @param[in]  data  Incoming data payload to state
   *
   * @tparam     T     State class
   *
   * @return     A pointer
   */
  template<typename T>
  static BasePtr createState(MissionManagerPtr mm, const Payload& data = {}) {
    return BasePtr(new T(mm, data));
  }

  /**
   * @brief      Check if state is of type. This templated function is a hack
   *             to allow header implementation of states and
   *             to break cyclic deps.
   *
   * @param[in]  next  State object that we would like to compare to T
   *
   * @tparam     T     State class
   *
   * @return     A pointer
   */
  template<typename T>
  static bool isStateOfType(const BasePtr& state) {
    return typeid(*state) == typeid(T);
  }

} // ns state
} // ns labdrone
} // ns creare
