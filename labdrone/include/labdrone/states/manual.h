/**
 * @file manual.h
 * @brief high-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 May 2020
 */

#pragma once

#include "labdrone/states/base.h"

namespace creare {
namespace labdrone {
namespace state {

  class Manual : public Base
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 8;
    static constexpr char SNAME[] = "MANUAL";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    Manual(MissionManagerPtr mm, const Payload& data = {}) : Base(mm) {}
    ~Manual() = default;

    // transitions into this state are always allowed from any state,
    // unless barred by canTransitionFrom.
    virtual bool alwaysAllowed() const override { return true; }

    virtual void tick(double now) override
    {
      if (first_tick_) initialize(now);
      static double last_now = now;
      const double dt = now - last_now;

      // get latest velocity and yaw rate command from joystick
      goal_.v = mm_->data().joy.v;
      goal_.r = mm_->data().joy.r;

      makeVelocityGoalSafe(dt, goal_);
      last_now = now;

      //
      // Transitions based on joystick buttons
      //

      if (mm_->data().joy.btns[Joy::B]) { // allow joystick to kill motors
        goal_.flying = false;

      } else if (mm_->data().joy.btns[Joy::Y]) { // go back to dock and hover
        Payload data;
        data.pose.q = mm_->data().initial_pose.q; // set yaw for landing
        // create straight-line trajectory to dock
        std::vector<Waypoint> waypoints;
        waypoints.push_back(mm_->data().initial_pose.p);
        waypoints.back().z() = mm_->params().nominal_alt;
        data.traj = utils::createTrajectory(mm_->data().state.p, waypoints,
                                            mm_->params().traj_fast_vmax,
                                            mm_->params().traj_fast_amax);
        next_state_ = createState<GoToDock>(mm_, data);

      } else if (mm_->data().joy.btns[Joy::X]) { // land in place to init alt
        Payload data;
        data.pose.p.z() = mm_->data().initial_pose.p.z();
        next_state_ = createState<DockLanding>(mm_, data);

      }
      
      // emergency landing (in place)
      // if (mm_->data().joy.btns[Joy::X]) next_state_ = createState<EmergencyLanding>(mm_);

    }
    
    virtual Goal getGoal() override
    {
      return goal_;
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // only the joystick can transition out of manual
      Error error;
      error.num = Error::TRANSITION_DISALLOWED;
      error.msg = "Cannot transition out of manual";
      return error;
    }

    virtual Error canTransitionFrom(const Base * const current) const override
    {
      Error error;
      if (!mm_->goal().flying) {
        error.num = Error::TRANSITION_DISALLOWED;
        error.msg = "Cannot switch to manual until flying";
      }
      return error;
    }

  private:
    bool first_tick_ = true; ///< has this state been initialized
    Goal goal_; ///< desired goal from joystick

    void initialize(double now)
    {
      goal_ = mm_->goal();
      goal_.v = Eigen::Vector3d::Zero();
      goal_.a = Eigen::Vector3d::Zero();
      goal_.j = Eigen::Vector3d::Zero();
      goal_.r = 0;
      first_tick_ = false;
    }

    void makeVelocityGoalSafe(double dt, Goal& goal)
    {
      // The following assumes that we are dealing with velocity goals.
      // Using the current goal state, this function generates a position+yaw
      // trajectory by integrating the new desired velocity goal.

      // for convenience
      const auto& max_accel_xy = mm_->params().max_accel_xy;
      const auto& max_accel_z = mm_->params().max_accel_z;
      const auto& bounds = mm_->params().bounds;

      // unpack velocity goals so we can safely integrate them
      auto vx = goal.v.x();
      auto vy = goal.v.y();
      auto vz = goal.v.z();
      auto r = goal.r;

      //
      // Initial signal conditioning of velocity goals
      //

      // rate limit the velocities (to indirectly limit accels)
      utils::rateLimit(dt, -max_accel_xy, max_accel_xy, goal.v.x(), vx);
      utils::rateLimit(dt, -max_accel_xy, max_accel_xy, goal.v.y(), vy);
      utils::rateLimit(dt, -max_accel_z , max_accel_z , goal.v.z(), vz);

      // TODO: limit yawrate?


      //
      // Generate position goals and ensure room bounds are maintained
      //

      // n.b. this logic does not prevent a discontinuous position waypoint
      // from being generated outside the room, e.g., from a mouse click.
      // This is because we are working with velocity goals, not position goals.

      // with this goal vel, what will my predicted next goal position be?
      // Note: we use predict forward using the goal so that the goal position
      // trajectory is smooth---allowing the outer loop control to deal with
      // actually getting there.
      const double nextx = goal.p.x() + vx * dt;
      const double nexty = goal.p.y() + vy * dt;
      const double nextz = goal.p.z() + vz * dt;

      // If the predicted position is outside the room bounds, only allow
      // movements that move the vehicle back into the room.
      bool xclamped = false, yclamped = false, zclamped = false;
      goal.p.x() = utils::clamp(nextx, std::min(bounds.x_min, goal.p.x()),
                            std::max(bounds.x_max, goal.p.x()), xclamped);
      goal.p.y() = utils::clamp(nexty, std::min(bounds.y_min, goal.p.y()),
                            std::max(bounds.y_max, goal.p.y()), yclamped);
      goal.p.z() = utils::clamp(nextz, std::min(bounds.z_min, goal.p.z()),
                            std::max(bounds.z_max, goal.p.z()), zclamped);

      // if the predicted position is outside the room bounds, zero the velocities.
      if (xclamped) vx = 0;
      if (yclamped) vy = 0;
      if (zclamped) vz = 0;

      // But zero the velocities so that acceleration is bounded
      if (xclamped) utils::rateLimit(dt, -max_accel_xy, max_accel_xy, goal.v.x(), vx);
      if (yclamped) utils::rateLimit(dt, -max_accel_xy, max_accel_xy, goal.v.y(), vy);
      if (zclamped) utils::rateLimit(dt, -max_accel_z, max_accel_z, goal.v.z(), vz);

      // set linear velocity
      goal.v.x() = vx;
      goal.v.y() = vy;
      goal.v.z() = vz;

      //
      // Generate yaw goal
      //

      // with this goal yawrate, what will my predicted next goal yaw be?
      // Note: same as position---we use the goal yaw instead of current yaw.
      const double nextY = goal.Y + r * dt;
      goal.Y = utils::wrapToPi(nextY); // doesn't really matter (becomes a quat)

      // set angular rate
      goal.r = r;
    }

  };

} // ns state
} // ns labdrone
} // ns creare
