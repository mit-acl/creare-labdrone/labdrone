/**
 * @file transaction.h
 * @brief Low-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 25 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class Transaction : public FollowTrajectory
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 7;
    static constexpr char SNAME[] = "TRANSACTION";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  private:
    using NotifyTransactionCompleteHandle = std::function<void()>;
    enum class Stage { TO_X1, TO_X2, TO_X3, FROM_X3, FROM_X2, FROM_X1 };
  public:
    Transaction(MissionManagerPtr mm, const Payload& data = {}, bool pickup = true)
    : FollowTrajectory(mm), pickup_(pickup)
    {
      // set the desired nest
      nest_pose_ = data.pose;

      //
      // Create trajectories from the desired waypoints
      //

      // for convenience:
      Eigen::Vector3d x1 = mm_->params().x1; // "get lined up" point
      Eigen::Vector3d x2 = mm_->params().x2; // "ready to transact" point
      Eigen::Vector3d x3 = mm_->params().x3; // "making contact" point

      // if dropoff, apply user specified altitude shift
      if (!pickup) {
        x1.z() += mm_->params().dropoff_z_shift;
        x2.z() += mm_->params().dropoff_z_shift;
        x3.z() += mm_->params().dropoff_z_shift;
      }

      //
      // Incoming routes to nest
      //

      // fast: from end of operator trajectory (x0) to x1
      std::vector<Waypoint> waypoints_to_x1;
      waypoints_to_x1.push_back(utils::expressWrtPose(x1, nest_pose_));
      traj_to_x1_ = utils::createTrajectory(mm_->data().state.p, waypoints_to_x1,
                      mm_->params().traj_fast_vmax, mm_->params().traj_fast_amax);

      // slow: from x1 to x2
      std::vector<Waypoint> waypoints_to_x2;
      waypoints_to_x2.push_back(utils::expressWrtPose(x2, nest_pose_));
      traj_to_x2_ = utils::createTrajectory(waypoints_to_x1.back(), waypoints_to_x2,
                      mm_->params().traj_slow_vmax, mm_->params().traj_slow_amax);

      // transaction: from x2 to x3 (contact)
      std::vector<Waypoint> waypoints_to_x3;
      waypoints_to_x3.push_back(utils::expressWrtPose(x3, nest_pose_));
      traj_to_x3_ = utils::createTrajectory(waypoints_to_x2.back(), waypoints_to_x3,
                      mm_->params().traj_transact_vmax, mm_->params().traj_transact_amax);

      //
      // Outgoing routes from nest
      //

      // transaction: from x3 to x2
      std::vector<Waypoint> waypoints_from_x3;
      waypoints_from_x3.push_back(utils::expressWrtPose(x2, nest_pose_));
      traj_from_x3_ = utils::createTrajectory(waypoints_to_x3.back(), waypoints_from_x3,
                      mm_->params().traj_transact_vmax, mm_->params().traj_transact_amax);

      // slow: from x2 to x1
      std::vector<Waypoint> waypoints_from_x2;
      waypoints_from_x2.push_back(utils::expressWrtPose(x1, nest_pose_));
      traj_from_x2_ = utils::createTrajectory(waypoints_from_x3.back(), waypoints_from_x2,
                      mm_->params().traj_slow_vmax, mm_->params().traj_slow_amax);

      // fast: from x1 to end of operator trajectory (x0)
      std::vector<Waypoint> waypoints_from_x1;
      waypoints_from_x1.push_back(mm_->data().state.p);
      traj_from_x1_ = utils::createTrajectory(waypoints_from_x2.back(), waypoints_from_x1,
                      mm_->params().traj_fast_vmax, mm_->params().traj_fast_amax);

      // setup yaw policy
      yawlaw_.policy = FollowTrajectory::YawPolicy::CONSTANT;
      yawlaw_.Y = utils::wrapToPi(utils::getYaw(nest_pose_.q) + M_PI + mm_->params().nest_yaw_offset);

      // initialize the trajectory to follow
      stage_ = Stage::TO_X1;
      current_traj_ = &traj_to_x1_;
      FollowTrajectory::setTrajectory(*current_traj_, yawlaw_);

      FollowTrajectory::registerNotifyWaypointReachedCb(
        std::bind(&Transaction::notifyWaypointReachedCb, this,
                                    std::placeholders::_1));
    }
    ~Transaction() = default;

    virtual void tick(double now) override
    {
      if (first_tick_) initialize(now);

      // CoM ramp
      const double T = now - last_com_update_; // meet min period requirement
      if (stage_ == Stage::FROM_X3 && T >= mm_->params().rbm_update_period) {
        if (pickup_) {
          // time since start of segment (and after waiting for gripper)
          const double tau = now - (t0_ + mm_->params().gripper_wait_sec);
          const double s = tau / mm_->params().rbm_ramp_duration;
          if (s > 0 && s <= 1) updateCoM(s);
          last_com_update_ = now;
        } else {
          // time since start of segment (don't wait for gripper)
          const double tau = now - t0_;
          const double s = tau / mm_->params().rbm_ramp_duration;
          const double sbar = 1 - s;
          if (sbar > 0 && sbar <= 1) updateCoM(sbar);
          last_com_update_ = now;
        }
      }

      if (wait_for_gripper_) {
        if (now - t0_ >= mm_->params().gripper_wait_sec) {
          wait_for_gripper_ = false;
        }
      } else {
        // n.b., this should come last, otherwise the stage_ if-statement may
        // evaluate to true without initialize() having been run.
        FollowTrajectory::tick(now);
      }

    }

    /**
     * @brief      Allows a caller to register a cb once transaction complete
     */
    void registerNotifyTransactionCompleteCb(NotifyTransactionCompleteHandle cb)
    {
      notifyTransactionComplete = cb;
    }

  private:
    bool pickup_; ///< is this a pickup or dropoff transaction?
    FollowTrajectory::YawPolicy yawlaw_; ///< yaw policy: keep aligned with nest
    bool first_tick_ = true; ///< has this state been initialized
    bool wait_for_gripper_ = false; ///< wait for gripper before executing traj
    double t0_; ///< beginning of each TO_/FROM_ segment period [s]
    double last_com_update_ = 0; ///< time [s] of last rbm shift
    Stage stage_; ///< current stage of transaction
    acl::polytraj::Trajectory* current_traj_; ///< current route corresponding to stage
    acl::polytraj::Trajectory traj_to_x1_; ///< traj from x0 to x1 (incoming)
    acl::polytraj::Trajectory traj_to_x2_; ///< traj from x1 to x2 (incoming)
    acl::polytraj::Trajectory traj_to_x3_; ///< traj from x2 to x3 (incoming)
    acl::polytraj::Trajectory traj_from_x3_; ///< traj from x3 to x2 (outgoing)
    acl::polytraj::Trajectory traj_from_x2_; ///< traj from x2 to x1 (outgoing)
    acl::polytraj::Trajectory traj_from_x1_; ///< traj from x1 to x0 (outgoing)
    Pose nest_pose_; ///< pose of desired nest
    NotifyTransactionCompleteHandle notifyTransactionComplete; ///< callback

    void initialize(double now)
    {
      // store for everyone else
      mm_->setdata().traj = *current_traj_;
      mm_->notify(MissionManager::Notification::NewTrajectory);

      // beginning of this segment
      t0_ = now;

      first_tick_ = false;
    }

    /**
     * @brief      Called whenever the current waypoint is reached
     *
     * @param[in]  idx   The index of the waypoint that was reached
     */
    void notifyWaypointReachedCb(size_t idx)
    {
      // we are seeking the *next* wp in the current trajectory
      const size_t next_idx = idx + 1;

      // Indicate that we have reached the next waypoint
      mm_->setdata().wp_idx = next_idx;
      mm_->notify(MissionManager::Notification::WaypointReached);

      // check if we have reached the last waypoint of the trajectory
      if (next_idx == current_traj_->q.size()) {
        if (stage_ == Stage::TO_X1) {
          stage_ = Stage::TO_X2;
          current_traj_ = &traj_to_x2_;
          FollowTrajectory::setTrajectory(*current_traj_, yawlaw_);
          first_tick_ = true;
        } else if (stage_ == Stage::TO_X2) {
          stage_ = Stage::TO_X3;
          current_traj_ = &traj_to_x3_;
          FollowTrajectory::setTrajectory(*current_traj_, yawlaw_);
          first_tick_ = true;
          mm_->actuateGripper(pickup_); // pickup: open gripper [dropoff: close (nop)]
        } else if (stage_ == Stage::TO_X3) {
          stage_ = Stage::FROM_X3;
          current_traj_ = &traj_from_x3_;
          FollowTrajectory::setTrajectory(*current_traj_, yawlaw_);
          first_tick_ = true;
          mm_->actuateGripper(!pickup_); // pickup: close gripper [dropoff: open]
          wait_for_gripper_ = true; // wait for gripper to completely close / open
        } else if (stage_ == Stage::FROM_X3) {
          stage_ = Stage::FROM_X2;
          current_traj_ = &traj_from_x2_;
          FollowTrajectory::setTrajectory(*current_traj_, yawlaw_);
          first_tick_ = true;
        } else if (stage_ == Stage::FROM_X2) {
          stage_ = Stage::FROM_X1;
          current_traj_ = &traj_from_x1_;
          FollowTrajectory::setTrajectory(*current_traj_, yawlaw_);
          first_tick_ = true;
        } else if (stage_ == Stage::FROM_X1) {
          notifyTransactionComplete();
        }
      }
    }

    /**
     * @brief      Updates the CoM (rbM) via a ramp
     *
     * @param[in]  s     interpolation parameter, \in [0, 1]
     */
    void updateCoM(double s)
    {
      if (s < 0 || s > 1) return;

      const Eigen::Vector3d r_bM = mm_->params().nominal_rbM + s * mm_->params().rbm_delta;
      mm_->updateCoM(r_bM);
    }
  };

} // ns state
} // ns labdrone
} // ns creare
