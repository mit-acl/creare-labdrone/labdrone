/**
 * @file states.h
 * @brief All states and their forward declarations
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <memory>

namespace creare {
namespace labdrone {
namespace state {

  // abstract
  class Base;

  using BasePtr = std::shared_ptr<Base>;
  using BaseConstPtr = std::shared_ptr<const Base>;

  // low level
  class Takeoff;
  class Wait;
  class Hover;
  class FollowTrajectory;
  class FastTravel;
  // class GoToNest;
  class Transaction;
  class Manual;
  class Landing;

  // high level
  class Shutdown;
  class StartUp;
  class DockTakeoff;
  class DockLanding;
  class Docked;
  class WaitAtDock;
  class GoToPickup;
  class Pickup;
  class WaitAtPickup;
  class GoToDropoff;
  class Dropoff;
  class WaitAtDropoff;
  class ReroutePending;
  class GoToDock;
  class EmergencyLanding;
  class ObstacleDetected;

} // ns state
} // ns labdrone
} // ns creare


// abstract
#include "labdrone/states/base.h"

// low level
#include "labdrone/states/hover.h"
#include "labdrone/states/landing.h"
#include "labdrone/states/shutdown.h"
#include "labdrone/states/start_up.h"
#include "labdrone/states/takeoff.h"
#include "labdrone/states/wait.h"
#include "labdrone/states/follow_trajectory.h"
#include "labdrone/states/fast_travel.h"
#include "labdrone/states/transaction.h"
#include "labdrone/states/manual.h"

// high level
#include "labdrone/states/dock_takeoff.h"
#include "labdrone/states/dock_landing.h"
#include "labdrone/states/docked.h"
#include "labdrone/states/go_to_pickup.h"
#include "labdrone/states/wait_at_dock.h"
#include "labdrone/states/pickup.h"
#include "labdrone/states/wait_at_pickup.h"
#include "labdrone/states/go_to_dropoff.h"
#include "labdrone/states/dropoff.h"
#include "labdrone/states/wait_at_dropoff.h"
#include "labdrone/states/reroute_pending.h"
#include "labdrone/states/go_to_dock.h"
#include "labdrone/states/emergency_landing.h"
#include "labdrone/states/obstacle_detected.h"