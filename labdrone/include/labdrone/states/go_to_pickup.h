/**
 * @file go_to_pickup.h
 * @brief High-level state
 * @author Parker Lusk <plusk@mit.edu>
 * @date 21 May 2020
 */

#pragma once

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

  class GoToPickup : public FastTravel
  {
  public:
    /// \brief State introspection
    static constexpr uint8_t SNUM = 40;
    static constexpr char SNAME[] = "GO_TO_PICKUP";
    virtual uint8_t snum() const { return SNUM; }
    virtual std::string sname() const { return SNAME; }
  public:
    GoToPickup(MissionManagerPtr mm, const Payload& data = {}) : FastTravel(mm, data)
    {
      // set params for pickup mode

      FastTravel::registerNotifyTravelFinishedCb(
        std::bind(&GoToPickup::notifyTravelFinishedCb, this));
    }
    ~GoToPickup() = default;

    virtual void tick(double now) override
    {
      FastTravel::tick(now);
    }

    virtual Error requestTransition(BasePtr next) override
    {
      // first, make sure that the next state even wants
      // to be transitioned into from this state
      auto error = next->canTransitionFrom(this);
      if (error.num != Error::NONE) return error;

      if (isStateOfType<ReroutePending>(next)) {
        next_state_ = next;
        return {}; // no error
      }

      return FastTravel::requestTransition(next);
    }

  private:

    void notifyTravelFinishedCb()
    {
      next_state_ = createState<WaitAtPickup>(mm_);
    }

  };

} // ns state
} // ns labdrone
} // ns creare
