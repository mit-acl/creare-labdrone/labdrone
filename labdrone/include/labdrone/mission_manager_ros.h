/**
 * @file mission_manager_ros.h
 * @brief ROS wrapper for the Creare Labdrone Mission Manager
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <algorithm>
#include <cctype>
#include <memory>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

#include <ros/ros.h>
#include <ros/master.h>
#include <ros/callback_queue.h>
#include <ros/spinner.h>

#include <std_msgs/Bool.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <snapstack_msgs/Goal.h>
#include <snapstack_msgs/State.h>
#include <labdrone_msgs/SetHexSMStateReq.h>
#include <labdrone_msgs/GetBattery.h>
#include <labdrone_msgs/GetHexSMData.h>
#include <labdrone_msgs/HexSMStateNotification.h>
#include <labdrone_msgs/ObstructionNotification.h>
#include <nav_msgs/Path.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/BatteryState.h>

#include <polytraj_ros/polytraj_ros.h>
#include <traj_gen/traj_gen.hpp>

#include "labdrone/mission_manager.h"
#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {

  class MissionManagerROS
  {
  public:
    MissionManagerROS(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);
    ~MissionManagerROS() = default;
    
  private:
    ros::NodeHandle nh_, nhp_; ///< public and private, respectively
    ros::Timer tim_tick_; ///< state machine tick driver
    ros::ServiceServer srv_state_req_, srv_get_battery_, srv_get_sm_data_;
    ros::Subscriber sub_world_, sub_state_, sub_joy_, sub_obs_;
    ros::ServiceClient srv_client_state_notif_;
    ros::Publisher pub_cmdout_, pub_gripper_, pub_comoffset_;
    ros::Publisher pub_vizstoptraj_, pub_vizobstacle_;
    ros::Publisher pub_viztraj_, pub_vizwps_, pub_vizcorridor_;

    /// \brief Data members
    std::string frame_id_ = "world"; ///< frame of state and trajectory
    std::unique_ptr<MissionManager> mission_manager_;
    std::vector<ros::Subscriber> subs_nest_; ///< for each discovered nest

    /// \brief Parameters
    double control_dt_; ///< period of state machine tick and goal pub
    double joy_kx_, joy_ky_, joy_kz_, joy_kr_; ///< scale joystick to velocity
    bool joy_body_frame_; ///< interpret joy vels in body or world frame?

    /**
     * @brief      Loads a yaml vector param into an Eigen::Vector3d
     *
     * @param[in]  nh    ROS node handle (which namespace to look in?)
     * @param[in]  p     ROS parameter name
     * @param      v     Output Eigen::Vector3d
     */
    void loadParam(const ros::NodeHandle& nh, const std::string& p, Eigen::Vector3d& v);

    void discoverNests();

    void nestCb(const geometry_msgs::PoseStampedConstPtr& msg,
                const std::string& nestId);
    void worldCb(const geometry_msgs::PoseStampedConstPtr& msg);
    void stateCb(const snapstack_msgs::StateConstPtr& msg);
    void joyCb(const sensor_msgs::JoyConstPtr& msg);
    void obstacleCb(const geometry_msgs::PointStamped& msg);

    void tickCb(const ros::TimerEvent& event);

    void notifyTransitionCb(uint8_t last_snum, std::string last_sname,
                            uint8_t snum, std::string sname);
    void notifyTrajectoryCb(const acl::polytraj::Trajectory& traj);
    void notifyObstructionCb(const PolyTrajPtr& traj,
                const Eigen::Vector3d& obstacle, const Eigen::Affine3d& T_wb);
    void actuateGripperCb(bool pickup);
    void updateCoMOffsetCb(const Eigen::Vector3d& rbm);

    bool stateReqSrvCb(labdrone_msgs::SetHexSMStateReq::Request& req,
                       labdrone_msgs::SetHexSMStateReq::Response& res);

    bool getBatterySrvCb(labdrone_msgs::GetBattery::Request& req,
                         labdrone_msgs::GetBattery::Response& res);

    bool getSMDataSrvCb(labdrone_msgs::GetHexSMData::Request& req,
                        labdrone_msgs::GetHexSMData::Response& res);

  };

} // ns labdrone
} // ns creare
