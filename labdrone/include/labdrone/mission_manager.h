/**
 * @file mission_manager.h
 * @brief Manages all aspects of a mission using a state machine
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#pragma once

#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <vector>
#include <mutex>

#include <Eigen/Dense>

#include <polytraj/polytraj.h>
#include <traj_gen/traj_gen.hpp>

#include "labdrone/data_store.h"

namespace creare {
namespace labdrone {

  using PolyTrajPtr = std::shared_ptr<trajgen::PolyTrajGen<double, 3>>;

  // forward declarations
  class MissionManager;
  namespace state { class Base; }
  namespace state { class Payload; }

  // typedefs
  using MissionManagerPtr = MissionManager*;
  namespace state { using BasePtr = std::shared_ptr<state::Base>; }
  namespace state { using BaseConstPtr = std::shared_ptr<const state::Base>; }

  class MissionManager
  {
  public:

    /**
     * @brief      User-specified parameters for missions and flight behavior
     */
    struct Parameters {

      //
      // Room bounds
      //

      struct Bounds {
        double x_min, x_max;
        double y_min, y_max;
        double z_min, z_max;
      } bounds;

      //
      // Obstacle avoidance
      //

      double stoptraj_tf; ///< duration of stopping trajectory, [s]
      double stoptraj_vehradius; ///< inflation of vehicle from body origin, [m]
      double stoptraj_corrdt; ///< time discretization step for corridor constraint, [s]
      bool stoptraj_autoreturn; ///< auto reroute to starting dock

      //
      // Takeoff and Landing
      //

      double spinup_time;
      double nominal_alt; ///< nominal flight altitude / takeoff alt
      double takeoff_vel;
      double landing_fast_threshold;
      double landing_fast_dec;
      double landing_slow_dec;

      //
      // Flight Characteristics and Safety
      //

      double max_accel_xy;
      double max_accel_z;

      //
      // Trajectory Tracking
      //

      double traj_fast_vmax; ///< max 3d velocity when designing a fast traj
      double traj_fast_amax; ///< max 3d accel when designing a fast traj
      double traj_slow_vmax; ///< max 3d velocity when designing a slow traj
      double traj_slow_amax; ///< max 3d accel when designing a slow traj
      double traj_transact_amax; ///< max 3d accel when designing a very slow traj
      double traj_transact_vmax; ///< max 3d velocity when designing a very slow traj
      double traj_err_radius; ///< if UAV is outside of this ball, stop

      bool yawlaw_velocity_vector; ///< should yaw always align with vel vec?

      double yaw_rate; ///< (max) yaw rate used for heading tracking
      double yaw_init_err_thr; ///< [rad] once yaw err below, traj tracking begins

      //
      // Transaction with nest
      //

      double transaction_vmax; ///< max vel during transaction traj
      double transaction_amax; ///< max acc during transaction traj

      /// \brief Transaction points w.r.t nest
      Eigen::Vector3d x1; ///< initial alignment position
      Eigen::Vector3d x2; ///< by this point it should be well aligned
      Eigen::Vector3d x3; ///< at this point the gripper should be turned on

      double gripper_wait_sec; ///< how long to wait for gripper to actuate [s]
      double dropoff_z_shift; ///< shifts the altitude of x1,x2,x3 for dropoff
      double rbm_update_period; ///< period [s] to send com updates
      double rbm_ramp_duration; ///< duration [s] of ramp / rbm interpolation
      Eigen::Vector3d rbm_delta; ///< CoM offset delta w/ microplate in gripper
      Eigen::Vector3d nominal_rbM; ///< nominal CoM offset w/o microplate
      double nest_yaw_offset; ///< manual offset on desired yaw to line up with nest

      bool truth_available; ///< do we expect a ground truth pose signal?
    };

  private:
    using NotifyTransitionHandle = std::function<void(uint8_t,std::string,uint8_t,std::string)>;
    using NotifyTrajectoryHandle = std::function<void(const acl::polytraj::Trajectory&)>;
    using NotifyObstructionHandle = std::function<void(const PolyTrajPtr&, const Eigen::Vector3d&,
                                                                          const Eigen::Affine3d&)>;
    using ActuateGripperHandle = std::function<void(bool pickup)>;
    using UpdateCoMHandle = std::function<void(const Eigen::Vector3d&)>;

  public:
    MissionManager(const Parameters& params);
    ~MissionManager() = default;

    void getBatteryInfo();
    Error requestStateChange(uint8_t snum, const acl::polytraj::Trajectory& traj, Pose pose);
    void getStateData();
    uint8_t getStateNum();

    /**
     * @brief      Main tick of the state machine
     *
     * @param[in]  now   Current time in seconds
     *
     * @return     Autopilot goal of the current state and time
     */
    Goal tick(double now);

    /// \brief Notificiations from states
    enum class Notification { NewTrajectory, WaypointReached };
    void notify(const Notification& n) const;
    void setLoaded(bool loaded);

    /// \brief Callback registration
    void registerNotifyTransitionCb(NotifyTransitionHandle cb);
    void registerNotifyTrajectoryCb(NotifyTrajectoryHandle cb);
    void registerNotifyObstructionCb(NotifyObstructionHandle cb);
    void registerActuateGripperCb(ActuateGripperHandle cb);
    void registerUpdateCoMOffsetCb(UpdateCoMHandle cb);

    /// \brief Getters and Setters
    Data& setdata() { return data_; }
    const Data& data() const { return data_; }
    const Parameters& params() const { return params_; }
    const Goal& goal() const { return goal_; }

  private:
    std::mutex mm_mutex_;

    state::BasePtr state_; ///< our current state

    /// \brief Maps state number to state object
    std::map<uint8_t,
              std::function<state::BasePtr(const state::Payload&)>> state_map_;

    Goal goal_; ///< current goal to be implemented

    Parameters params_; ///< Mission parameters
    Data data_; ///< All data associated with the vehicle or mission

    void makeGoalSafe(Goal& goal);

    /// \brief Callbacks handlers
    NotifyTransitionHandle notifyTransition;
    NotifyTrajectoryHandle notifyTrajectory;
  public:
    NotifyObstructionHandle notifyObstruction;
    ActuateGripperHandle actuateGripper;
    UpdateCoMHandle updateCoM;
  };

} // ns labdrone
} // ns creare
