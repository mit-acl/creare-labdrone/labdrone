/**
 * @file utils.h
 * @brief Utilities
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 15 Oct 2019
 */

#pragma once

#include <algorithm>
#include <array>
#include <cmath>
#include <numeric>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <polytraj/polytraj.h>

#include "labdrone/data_store.h"

namespace creare {
namespace labdrone {
namespace utils {

/**
 * @brief      Clamp function to saturate between a low and high value
 *
 * @param[in]  val      The value to clamp
 * @param[in]  lower    The lower bound
 * @param[in]  upper    The upper bound
 * @param      clamped  Wether or not the value was clamped
 *
 * @tparam     T        The template type
 *
 * @return     The clamped value
 */
template<typename T>
static T clamp(const T& val, const T& lower, const T& upper, bool& clamped) {
  if (val < lower) {
    clamped = true;
    return lower;
  }

  if (val > upper) {
    clamped = true;
    return upper;
  }

  clamped = false;
  return val;
}

// ----------------------------------------------------------------------------

/**
 * @brief      Specialization of clamp without clamp indicator
 */
template<typename T>
static T clamp(const T& val, const T& lower, const T& upper) {
  bool clamped = false;
  return clamp(val, lower, upper, clamped);
}

// ----------------------------------------------------------------------------

/**
 * @brief      Saturate the current value of a signal by limit its
 *             rate of change, given its current (desired) value, 
 *             last value, and the timestep.
 *
 * @param[in]  dt        Timestep between current and last value
 * @param[in]  lRateLim  Lower rate limit
 * @param[in]  uRateLim  Upper rate limit
 * @param[in]  v0        The last value
 * @param      v1        The current (des) value, to be rate limited (output)
 */
template<typename T>
static void rateLimit(double dt, const T lRateLim, const T uRateLim,
                      const T v0, T& v1)
{
  // calculate the highest / lowest the components are allowed to attain
  const T upper = v0 + uRateLim * dt;
  const T lower = v0 + lRateLim * dt;

  // make sure current value does not exceed rate of change constraints
  if (v1 > upper) v1 = upper;
  if (v1 < lower) v1 = lower;
}

// ----------------------------------------------------------------------------

/**
 * @brief      Wrap angle so that it is in [-pi, pi]
 *
 * @param[in]  angle  The angle to wrap
 *
 * @return     The wrapped angle
 */
static double wrapToPi(double angle)
{
  if (angle >  M_PI) return angle - 2*M_PI;
  if (angle < -M_PI) return angle + 2*M_PI;
  return angle;
}

// ----------------------------------------------------------------------------

/**
 * @brief      Wrap angle so that it is in [0, 2*pi]
 *
 * @param[in]  angle  The angle to wrap
 *
 * @return     The wrapped angle
 */
static double wrapTo2Pi(double angle)
{
  if (angle > 2*M_PI) return angle - 2*M_PI;
  if (angle < 0)      return angle + 2*M_PI;
  return angle;
}

// ----------------------------------------------------------------------------

/**
 * @brief      Extract yaw angle from quaternion
 * 
 *             Taken from tf2::getYaw. see 
 *    http://docs.ros.org/jade/api/tf2/html/impl_2utils_8h_source.html#l00123
 *
 * @param[in]  q     Quaternion
 *
 * @return     Yaw angle
 */
inline
static double getYaw(const Eigen::Quaterniond& q)
{
  double yaw;

  double sqw;
  double sqx;
  double sqy;
  double sqz;

  sqx = q.x() * q.x();
  sqy = q.y() * q.y();
  sqz = q.z() * q.z();
  sqw = q.w() * q.w();

  // Cases derived from https://orbitalstation.wordpress.com/tag/quaternion/
  double sarg = -2 * (q.x()*q.z() - q.w()*q.y()) / (sqx + sqy + sqz + sqw); /* normalization added from urdfom_headers */

  if (sarg <= -0.99999) {
    yaw   = -2 * std::atan2(q.y(), q.x());
  } else if (sarg >= 0.99999) {
    yaw   = 2 * std::atan2(q.y(), q.x());
  } else {
    yaw   = std::atan2(2 * (q.x()*q.y() + q.w()*q.z()), sqw + sqx - sqy - sqz);
  }
  return yaw;
}

// ----------------------------------------------------------------------------

inline
static Eigen::Quaterniond fromYaw(double yaw)
{
  return Eigen::Quaterniond(Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ()));
}

// ----------------------------------------------------------------------------

/**
 * @brief      Express a point in the parent frame (A) of a given pose
 *
 * @param[in]  x_B   The point, in frame B
 * @param[in]  T_AB  The pose, frame B w.r.t A
 *
 * @return     The point expressed in frame A
 */
static Eigen::Vector3d expressWrtPose(const Eigen::Vector3d& x_B, const Pose& T_AB)
{
  return T_AB.q * x_B + T_AB.p;
}

// ----------------------------------------------------------------------------

static acl::polytraj::Trajectory createTrajectory(const Eigen::Vector3d& start,
  const std::vector<Waypoint>& waypoints, double vmax, double amax)
{
  // initialize the trajectory generator
  acl::polytraj::TrajectoryGenerator polytraj;

  // polytraj expects that first waypoint is starting point
  std::vector<Eigen::Vector3d> q;
  q.reserve(1 + waypoints.size());
  q.push_back(start);
  q.insert(q.end(), waypoints.begin(), waypoints.end());

  acl::polytraj::TrajGenOpts opts;
  opts.poly_order = 6;
  opts.objective_derivative = 3;
  opts.is_waypoint_soft = true;
  opts.is_single_corridor = true;
  opts.is_multi_corridor = false;
  opts.w_d = 10000;
  opts.N_safe_pnts = 2;
  opts.safe_r = 100;
  opts.verbose = false;

  // default to slow trajectories
  polytraj.setTimeAllocationMethod_VelocityRamp(vmax, amax);

  // generate a trajectory
  auto traj = polytraj.fromWaypoints(q, opts);
  return traj;
}

// ----------------------------------------------------------------------------

/**
 * @brief      Given an initial time/heading and a desired yawrate/heading,
 *             this class calculates the necessary yaw cmd for every time t
 *             after t0 to get into desired heading. Uses SLERP.
 */
class YawRegulator
{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  YawRegulator(const Eigen::Quaterniond& qdes, double yawrate)
  : yawrate_(yawrate)
  {
    // extract yaw only---no matter what user passes in
    qdes_ = utils::fromYaw(utils::getYaw(qdes));
  }
  ~YawRegulator() = default;

  void setInitialYaw(double t0, const Eigen::Quaterniond& q0)
  {
    t0_ = t0;
    q0_ = utils::fromYaw(utils::getYaw(q0)); // extract yaw only
    deltat_ = q0_.angularDistance(qdes_) / yawrate_;

    // determine sign of yawrate
    const auto qe = q0_.inverse() * qdes_;
    const Eigen::AngleAxisd aa(qe);
    sgn_ = aa.axis().z();
  }

  void getYawCmd(double t, double& Y, double& r)
  {
    const double alpha = std::min(t - t0_, deltat_) / deltat_;
    const Eigen::Quaterniond qcmd = q0_.slerp(alpha, qdes_);

    Y = utils::getYaw(qcmd);
    r = sgn_*yawrate_;
  }

  double getYawError(const Eigen::Quaterniond& q)
  {
    const auto qyaw = utils::fromYaw(utils::getYaw(q));
    return qyaw.angularDistance(qdes_);
  }

  double getDesiredYaw() const
  {
    return utils::getYaw(qdes_);
  }

private:
  Eigen::Quaterniond q0_, qdes_;
  double yawrate_; ///< heading rate used
  double t0_; ///< initial time, used to SLERP from q0 to qdes
  double deltat_; ///< time needed to SLERP based on max yawrate
  int sgn_; ///< sign of yawrate
};

// ----------------------------------------------------------------------------

/**
 * @brief      Given kinematic constraints and a path, this class overlays
 *             a smooth velocity profile (S-curve) onto the path to form
 *             a constant-jerk trajectory.
 *
 *             For math, see acl_joy/doc/velocityprofile.pdf
 */
class SmoothVelocityProfileTrajectory
{
public:
  struct State { double t, p, v, a, j; };
public:
  /**
   * @brief      Create trajectory from path
   *
   * @param[in]  ps    Starting point
   * @param[in]  pe    Ending point
   * @param[in]  v0    Initial velocity
   * @param[in]  a0    Initial acceleration
   * @param[in]  vmax  Maximum allowable velocity
   * @param[in]  amax  Maximum allowable acceleration
   * @param[in]  jmax  Maximum allowable jerk
   */
  SmoothVelocityProfileTrajectory(double ps, double pe, double v0, double a0,
                                        double vmax, double amax, double jmax)
    : ps_(ps), pe_(pe), v0_(v0), a0_(a0),
      vmax_user_(vmax), amax_(amax), jmax_(jmax)
    {
      // ensure model assumptions are met
      assert(vmax > 0);
      assert(amax > 0);
      assert(jmax > 0);
      assert(pe > ps);
      assert(v0 >= 0 && v0 <= vmax_user_);
      assert(a0 >= 0 && a0 <= amax_);

      // calculate actual total length of input path
      stotal_ = std::abs(pe_ - ps_);

      // compute a velocity profile on top of the path
      createVelocityProfile();
    }
  SmoothVelocityProfileTrajectory(double ps, double pe,
                  double vmax, double amax, double jmax)
    : SmoothVelocityProfileTrajectory(ps, pe, 0, 0, vmax, amax, jmax) {}
  ~SmoothVelocityProfileTrajectory() = default;

  /**
   * @brief      Evaluates the trajectory and its derivatives at
   *             the requested time.
   *
   * @param[in]  t     Time [s] to evaluate at
   *
   * @return     State (t, p, v, a, j)
   */
  State eval(double t)
  {
    assert(t >= 0);

    State s;
    s.t = t;

    if (t <= T_[0]) {
      const size_t i = 0;
      const double dt = t;
      s.p = v0_*dt + a0_*dt*dt/2 + j_[i]*dt*dt*dt/6;
      s.v = v0_ + a0_*dt + j_[i]*dt*dt/2;
      s.a = a0_ + j_[i]*dt;
      s.j = j_[i];
    } else if (t >= T_[NUM_SEGS-1]) {
      s.p = s_[NUM_SEGS-1];
      s.v = 0;
      s.a = 0;
      s.j = 0;
    } else {
      for (size_t i=1; i<NUM_SEGS; ++i) {
        if (T_[i-1] <= t && t <= T_[i]) {
          const double dt = t - T_[i-1];
          s.p = s_[i-1] + v_[i-1]*dt + a_[i-1]*dt*dt/2 + j_[i]*dt*dt*dt/6;
          s.v = v_[i-1] + a_[i-1]*dt + j_[i]*dt*dt/2;
          s.a = a_[i-1] + j_[i]*dt;
          s.j = j_[i];
          break;
        }
      }
    }

    // start path at user-specified starting point
    s.p += ps_;

    return s;
  }

  /**
   * @brief      Total time for trajectory completion.
   *
   * @return     The total time.
   */
  double getTotalTime() const { return T_[NUM_SEGS-1]; }

private:
  double ps_, pe_, v0_, a0_; ///< path info and initial conditions
  double vmax_user_; ///< user-specified max velocity
  double vmax_; ///< algorithm-selected maximum velocity
  double amax_, jmax_; ///< kinematic constraints
  double stotal_; ///< total path length

  static constexpr int NUM_SEGS = 7;
  std::array<double, NUM_SEGS> dt_; ///< duration of each segment, [s]
  std::array<double, NUM_SEGS> ds_; ///< length of each segment, [m]
  std::array<double, NUM_SEGS> v_; ///< velocity [m/s] at the end of each seg
  std::array<double, NUM_SEGS> a_; ///< accel [m/s/s] at the end of each seg
  std::array<double, NUM_SEGS> j_; ///< jerk [m/s/s/s] "input" at beg of ea seg

  std::array<double, NUM_SEGS> T_; ///< cumsum of time segments
  std::array<double, NUM_SEGS> s_; ///< cumsum of segment lengths

  void createVelocityProfile()
  {
    //
    // Calculate the maximum (unconstrained) attainable velocity
    //

    // Segment 1
    const double dt1 = (amax_ - a0_) / jmax_;
    const double ds1 = v0_*dt1 + a0_*dt1*dt1/2 + jmax_*dt1*dt1*dt1/6;
    const double v1 = v0_ + a0_*dt1 + jmax_*dt1*dt1/2;
    // Segment 7
    const double dt7 = amax_ / jmax_;
    const double ds7 = jmax_*dt7*dt7*dt7/6;

    // Solve for maximum attainable velocity via quadratic eq
    const double a = 1 / amax_;
    const double b = amax_ / jmax_;
    const double c = ds1 + ds7 - stotal_
                   - (5*amax_*amax_*amax_)/(24*jmax_*jmax_)
                   - v1*v1 / (2*amax_);
    const double vmax = (-b + std::sqrt(b*b - 4*a*c)) / (2*a);

    // Subject this maximum attainable velocity to the
    // velocity constraint given by the user.
    vmax_ = std::min(vmax_user_, vmax);

    //
    // Compute Kinematic Segments
    //

    // note that a constant jerk is the "input" to each kinematic segment

    size_t i = 0;

    // Segment 1: max jerk until max acceleration
    i = 0;
    j_[i] = jmax_;
    dt_[i] = dt1;
    ds_[i] = ds1;
    v_[i] = v1;
    a_[i] = amax_;

    // Prelude: Segment 3
    const double dt3 = amax_ / jmax_;
    const double dv3 = jmax_*dt3*dt3/2;

    // Segment 2: max acceleration until *almost* max velocity
    i = 1;
    j_[i] = 0;
    v_[i] = vmax_ - dv3;
    dt_[i] = (v_[i] - v_[i-1]) / amax_;
    ds_[i] = v_[i-1]*dt_[i] + a_[i-1]*dt_[i]*dt_[i]/2 + j_[i]*dt_[i]*dt_[i]*dt_[i]/6;
    a_[i] = amax_;

    // Segment 3: min jerk to null accel --> max velocity hit
    i = 2;
    j_[i] = -jmax_;
    dt_[i] = dt3;
    ds_[i] = v_[i-1]*dt_[i] + a_[i-1]*dt_[i]*dt_[i]/2 + j_[i]*dt_[i]*dt_[i]*dt_[i]/6;
    v_[i] = vmax_;
    a_[i] = 0;

    // Segment 4: cruise
    i = 3;
    j_[i] = 0;
    dt_[i] = 0; // calculate later
    ds_[i] = 0; // calculate later
    v_[i] = vmax_;
    a_[i] = 0;

    // Segment 5: min jerk until min accel
    i = 4;
    j_[i] = -jmax_;
    dt_[i] = amax_ / jmax_;
    ds_[i] = v_[i-1]*dt_[i] + a_[i-1]*dt_[i]*dt_[i]/2 + j_[i]*dt_[i]*dt_[i]*dt_[i]/6;
    v_[i] = v_[i-1] + j_[i]*dt_[i]*dt_[i]/2;
    a_[i] = -amax_;

    // Prelude: Segment 7
    // const double dt7 = amax_ / jmax_;
    const double dv7 = -jmax_*dt7*dt7/2;

    // Segment 6: min acceleration until *almost* terminal velocity
    i = 5;
    j_[i] = 0;
    v_[i] = -dv7;
    dt_[i] = (v_[i] - v_[i-1]) / -amax_;
    ds_[i] = v_[i-1]*dt_[i] + a_[i-1]*dt_[i]*dt_[i]/2 + j_[i]*dt_[i]*dt_[i]*dt_[i]/6;
    a_[i] = -amax_;

    // Segment 7: max jerk to null accel --> terminal velocity hit
    i = 6;
    j_[i] = jmax_;
    dt_[i] = dt7;
    ds_[i] = v_[i-1]*dt_[i] + a_[i-1]*dt_[i]*dt_[i]/2 + j_[i]*dt_[i]*dt_[i]*dt_[i]/6;
    v_[i] = 0;
    a_[i] = 0;

    // Finale: Segment 4 revisted
    const double ds_sum = std::accumulate(ds_.begin(), ds_.end(), 0.0);
    if (ds_sum < stotal_) {
      // the length of the cruise section is whatever length hasn't been used
      ds_[3] = stotal_ - ds_sum;
      dt_[3] = ds_[3] / vmax_;
    }

    // make sure that all of the times are positive, otherwise the kinematic
    // constraints cannot be enforced along the path
    assert(std::all_of(ds_.begin(), ds_.end(), [](double v){ return v>=0; }));

    // calculate cumulative sums
    std::partial_sum(dt_.begin(), dt_.end(), T_.begin());
    std::partial_sum(ds_.begin(), ds_.end(), s_.begin());
  }

};



} // ns utils
} // ns labdrone
} // ns creare
