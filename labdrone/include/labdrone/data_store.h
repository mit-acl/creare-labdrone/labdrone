/**
 * @file data_store.h
 * @brief Data store that is shared amongst mission manager and states
 * @author Parker Lusk <plusk@mit.edu>
 * @date 20 May 2020
 */

#pragma once

#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <polytraj/polytraj.h>

namespace creare {
namespace labdrone {

  /**
   * @brief      High-level goal command sent to autopilot. Each state must
   *             provide a goal, even if it is the same as the previous one.
   */
  struct Goal {
    Eigen::Vector3d p; ///< position
    Eigen::Vector3d v; ///< velocity
    Eigen::Vector3d a; ///< acceleration
    Eigen::Vector3d j; ///< jerk
    double Y, r; ///< yaw and yaw rate
    bool flying; ///< should this goal energize motors?
  };

  /**
   * @brief      Pose data for some 3D object
   */
  struct Pose {
    double t = -1; ///< time in seconds
    Eigen::Vector3d p; ///< position in meters
    Eigen::Quaterniond q; ///< body orientation

    Eigen::Affine3d toAffine() const
    {
      Eigen::Affine3d aff;
      aff.translation() = p;
      aff.linear() = q.toRotationMatrix();
      return aff;
    }
  };

  /**
   * @brief      Twist data for some 3D object
   */
  struct Twist {
    double t = -1; ///< time in seconds
    Eigen::Vector3d v; ///< linear velocity
    Eigen::Vector3d w; ///< angular velocity
  };

  /**
   * @brief Waypoints do not consider yaw, only 3d position
   */
  using Waypoint = Eigen::Vector3d;

  /**
   * @brief      Error information to send to base station
   */
  struct Error {
    static constexpr uint8_t NONE = 0;
    static constexpr uint8_t TRANSITION_DISALLOWED = 1;
    static constexpr uint8_t ROUTE_DISALLOWED = 2;
    static constexpr uint8_t OBSTRUCTED_ROUTE = 3;
    uint8_t num = NONE;
    std::string msg;
  };

  /**
   * @brief      Velocity command from joystick
   */
  struct Joy {
    // for definitions, see
    // http://wiki.ros.org/joy#Microsoft_Xbox_360_Wireless_Controller_for_Linux
    // axes
    static constexpr size_t LEFT_X = 0;
    static constexpr size_t LEFT_Y = 1;
    static constexpr size_t RIGHT_X = 3;
    static constexpr size_t RIGHT_Y = 4;
    // buttons
    static constexpr size_t A = 0;
    static constexpr size_t B = 1;
    static constexpr size_t X = 2;
    static constexpr size_t Y = 3;
    static constexpr size_t LB = 4;
    static constexpr size_t RB = 5;
    static constexpr size_t BACK = 6;
    static constexpr size_t START = 7;
    static constexpr size_t PWR = 8;
    double t; ///< time in seconds
    Eigen::Vector3d v; ///< velocity in the world frame
    double r; ///< angular velocity for yaw
    std::vector<bool> btns; ///< which buttons are pressed
  };

  /**
   * @brief      The data store available to all states for r/w
   */
  struct Data {
    Pose truth; ///< ground truth pose (i.e., mocap)
    Pose state; ///< pose from onboard estimator
    Twist twist; ///< twist from onboard estimator (expr in world)
    std::map<std::string, Pose> nests; ///< poses of any discovered nests

    acl::polytraj::Trajectory traj; ///< the loaded trajectory being flown
    int16_t wp_idx = -1; ///< current desired waypoint index (-1: not started)

    Joy joy; ///< joystick commands

    Pose initial_pose; ///< initial multirotor pose (in docked state)

    bool obstacle_detected = false; ///< is there an obstacle to react to?
    Pose obstacle; ///< pose of detected obstacle

    bool loaded = false; ///< indicates if tray is loaded in gripper
  };

} // ns labdrone
} // ns creare
