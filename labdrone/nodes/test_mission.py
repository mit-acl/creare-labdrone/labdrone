#!/usr/bin/env python

import sys
import argparse

import rospy
import rosparam

from geometry_msgs.msg import PoseStamped, Pose, Point
from labdrone_msgs.msg import HexSMState, HexError, NestData, RouteData
from labdrone_msgs.srv import SetHexSMStateReq, SetHexSMStateReqRequest
from polytraj_msgs.srv import GenerateTrajectory, GenerateTrajectoryRequest

class MissionRequest:

    def __init__(self):
        vehname = self._get_vehname()
        srv = '{}/StateReq'.format(vehname)
        rospy.wait_for_service(srv, timeout=2)
        self.srv = rospy.ServiceProxy(srv, SetHexSMStateReq)

        # nest poses from VICON
        self.sub_nests = {}
        self.nest_poses = {}
        self._discover_nests()

        # hex pose
        self.pose = None
        self.sub_pose = rospy.Subscriber('{}/world'.format(vehname), PoseStamped, self.poseCb)

        # nominal flight altitude
        self.nominal_alt = rospy.get_param('/{}/mission_manager/nominal_alt'.format(vehname))

    def _get_vehname(self):
        params = rosparam.list_params('/')
        return [p for p in params if 'mission_manager' in p][0].split('/')[1]

    def _discover_nests(self):
        topic_infos = rospy.get_published_topics()
        for tinfo in topic_infos:
            tname = tinfo[0]
            if 'nest' in tname:
                parts = tname.split('/')
                if len(parts) == 3 and parts[2] == "world":
                    nest = parts[1]
                    self.sub_nests[nest] = rospy.Subscriber(tname, PoseStamped,
                                            lambda msg, nest=nest: self.nestCb(msg, nest))

    def _handle_request(self, req):
        response = self.srv(req)

        if response.error.error != HexError.NONE:
            print()
            print("Mission manager responded with error")
            print()
            print(response)
            return False

        return True

    def _get_initial_pose(self):
        pose = Pose()
        pose.position.x = rospy.get_param('~initial_pose/position/x')
        pose.position.y = rospy.get_param('~initial_pose/position/y')
        pose.position.z = rospy.get_param('~initial_pose/position/z')
        pose.orientation.x = rospy.get_param('~initial_pose/orientation/x')
        pose.orientation.y = rospy.get_param('~initial_pose/orientation/y')
        pose.orientation.z = rospy.get_param('~initial_pose/orientation/z')
        pose.orientation.w = rospy.get_param('~initial_pose/orientation/w')
        return pose

    def _wait_for_pose_msg(self):
        while True:
            if self.pose is not None:
                break

    def _wait_for_nest_pose_msg(self, nest):
        while True:
            if nest in self.nest_poses:
                break

    def _set_initial_pose(self):
        self._wait_for_pose_msg()

        # set rosparam for landing
        rospy.set_param('~initial_pose/position/x', self.pose.pose.position.x)
        rospy.set_param('~initial_pose/position/y', self.pose.pose.position.y)
        rospy.set_param('~initial_pose/position/z', self.pose.pose.position.z)
        rospy.set_param('~initial_pose/orientation/x', self.pose.pose.orientation.x)
        rospy.set_param('~initial_pose/orientation/y', self.pose.pose.orientation.y)
        rospy.set_param('~initial_pose/orientation/z', self.pose.pose.orientation.z)
        rospy.set_param('~initial_pose/orientation/w', self.pose.pose.orientation.w)

    def _create_trajectory(self, waypoints):
        self._wait_for_pose_msg()

        srv = '/polytraj_server/generate'
        rospy.wait_for_service(srv, timeout=2)
        generate = rospy.ServiceProxy(srv, GenerateTrajectory)

        req = GenerateTrajectoryRequest()
        req.startpoint = self.pose.pose.position
        req.waypoints = waypoints
        res = generate(req)

        if not res.success:
            print("Error generating trajectory!!")
            print(res.msg)
            sys.exit(1)

        print(res)

        return res.trajectory

    def nestCb(self, msg, nest):
        self.nest_poses[nest] = msg

    def poseCb(self, msg):
        self.pose = msg

    def dock_takeoff(self):
        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.TAKE_OFF
        success = self._handle_request(req)
        if success:
            self._set_initial_pose()

    def dock_landing(self):
        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.DOCK_LANDING
        req.pose = self._get_initial_pose() # just needs alt at docked
        self._handle_request(req)

    def go_to_pickup(self):
        # hardcoded waypoints (excluding start position) for pickup fast travel
        waypoints = [Point(2, 0.5, self.nominal_alt)]

        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.GO_TO_PICKUP
        req.traj = self._create_trajectory(waypoints)
        self._handle_request(req)

    def pickup(self):
        nest = 'nest0'
        self._wait_for_nest_pose_msg(nest)

        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.PICKUP
        req.pose = self.nest_poses[nest].pose
        self._handle_request(req)

    def go_to_dropoff(self):
        # hardcoded waypoints (excluding start position) for dropoff fast travel
        waypoints = [Point(2, 0.5, self.nominal_alt)]

        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.GO_TO_DROP_OFF
        req.traj = self._create_trajectory(waypoints)
        self._handle_request(req)

    def dropoff(self):
        nest = 'nest0'
        self._wait_for_nest_pose_msg(nest)

        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.DROP_OFF
        req.pose = self.nest_poses[nest].pose
        self._handle_request(req)

    def go_to_dock(self):
        # hardcoded waypoints (excluding start position) to go back to initial
        # position (assumed to have been captured before takeoff s.t. it is at dock)
        pose0 = self._get_initial_pose()
        waypoints = [Point(pose0.position.x, pose0.position.y, self.nominal_alt)]

        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.GO_TO_DOCK
        req.traj = self._create_trajectory(waypoints)
        self._handle_request(req)
    
    def reroute(self):
        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.REROUTE_PENDING
        self._handle_request(req)

    def emergency_landing(self):
        req = SetHexSMStateReqRequest()
        req.state.state = HexSMState.EMERGENCY_LANDING
        self._handle_request(req)

if __name__ == '__main__':
  
    parser = argparse.ArgumentParser(add_help=True, description="Select a state request to test mission manager with")
    parser.add_argument('--takeoff', action="store_true")
    parser.add_argument('--landing', action="store_true")
    parser.add_argument('--go-to-pickup', action="store_true")
    parser.add_argument('--pickup', action="store_true")
    parser.add_argument('--go-to-dropoff', action="store_true")
    parser.add_argument('--dropoff', action="store_true")
    parser.add_argument('--go-to-dock', action="store_true")
    parser.add_argument('--reroute', action="store_true")
    parser.add_argument('--emergency-landing', action="store_true")

    args = parser.parse_args()

    rospy.init_node('test_mission')
    req = MissionRequest()

    if args.takeoff:
        req.dock_takeoff()

    elif args.landing:
        req.dock_landing()

    elif args.go_to_pickup:
        req.go_to_pickup()

    elif args.pickup:
        req.pickup()

    elif args.go_to_dropoff:
        req.go_to_dropoff()

    elif args.dropoff:
        req.dropoff()

    elif args.reroute:
        req.reroute()
    
    elif args.emergency_landing:
        req.emergency_landing()
    
    elif args.go_to_dock:
        req.go_to_dock()
        

    else:
        parser.print_help()
