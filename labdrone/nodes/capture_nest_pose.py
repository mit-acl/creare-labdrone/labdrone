#!/usr/bin/env python
"""
Captures the nest pose of a specific nest.

1. Insert nest marker fiducial into desired nest.
2. Make sure VICON is on and `nestmarker` object is loaded/active.
3. Run this node and the nest pose will be saved in rosparam server
4. Run the rest of the labdrone base station and the saved nest will be broadcast

Parker Lusk
plusk@mit.edu
27 July 2020
"""

import rospy

import argparse
import numpy as np

from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PoseStamped

class Capture:
  def __init__(self, tname, nestid):
    self.tname = tname
    self.nestid = nestid

    self.sub_pose = rospy.Subscriber("/{}/world".format(self.tname), PoseStamped, self.poseCb)
    
  def poseCb(self, msg):
    
    # Nest w.r.t world pose
    p = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
    q = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]

    # remember: 1-2-3 extrinsic (from msg, sxyz) <--> 3-2-1 intrinsic (to rosparam)
    sxyz = euler_from_quaternion(q, axes='sxyz')

    # assume nestmarker was level---zero out roll/pitch
    sxyz = (0.0, 0.0, sxyz[2])

    # build vector for rosparam
    xyzYPR = [p[0], p[1], p[2], sxyz[2], sxyz[1], sxyz[0]]

    rospy.set_param('nests/nest{}/xyzYPR'.format(self.nestid), xyzYPR)

    rospy.signal_shutdown("")


if __name__ == '__main__':
  parser = argparse.ArgumentParser(add_help=False, description="Capture a nest pose using the marker fiducial tray")
  parser.add_argument('--config-a', action="store_true")
  parser.add_argument('-i', '--id', type=int, help="nest id (`nestX`), X is an int", default=0)
  parser.add_argument('-n', '--name', type=str, help="nest marker vicon name", default="nestmarker")
  args = parser.parse_args()

  rospy.init_node('capture_nest_pose', anonymous=False)
  capture = Capture(args.name, args.id)
  rospy.spin()
