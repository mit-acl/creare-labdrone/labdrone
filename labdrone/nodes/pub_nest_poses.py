#!/usr/bin/env python

import rospy

import tf2_ros
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import PoseStamped, TransformStamped

def createROSmsgs(nests, frame_id='world'):
    for nestid in nests:
        nest = nests[nestid]

        # unpack values
        x, y, z, Y, P, R = nest['xyzYPR']

        # remember: 3-2-1 intrinsic (from user) <--> 1-2-3 extrinsic (input here)
        quat = quaternion_from_euler(R, P, Y, axes='sxyz')

        # create the message to be published
        nest['pose'] = PoseStamped()
        nest['pose'].header.frame_id = frame_id
        nest['pose'].pose.position.x = x
        nest['pose'].pose.position.y = y
        nest['pose'].pose.position.z = z
        nest['pose'].pose.orientation.x = quat[0]
        nest['pose'].pose.orientation.y = quat[1]
        nest['pose'].pose.orientation.z = quat[2]
        nest['pose'].pose.orientation.w = quat[3]

        # create the transform to be broadcast (tf2)
        nest['tf'] = TransformStamped()
        nest['tf'].header.frame_id = frame_id
        nest['tf'].child_frame_id = nestid
        nest['tf'].transform.translation.x = x
        nest['tf'].transform.translation.y = y
        nest['tf'].transform.translation.z = z
        nest['tf'].transform.rotation.x = quat[0]
        nest['tf'].transform.rotation.y = quat[1]
        nest['tf'].transform.rotation.z = quat[2]
        nest['tf'].transform.rotation.w = quat[3]        

        # add a publisher for this nest
        nest['pub'] = rospy.Publisher('/{}/world'.format(nestid), PoseStamped, queue_size=1)

    return nests

if __name__ == '__main__':
  rospy.init_node('pub_nest_poses', anonymous=False)

  br = tf2_ros.TransformBroadcaster()

  # load nests from param file and create necessary ROS counterparts
  nests = rospy.get_param('nests')
  nests = createROSmsgs(nests)

  print(nests)

  r = rospy.Rate(100)
  while not rospy.is_shutdown():

    for nestid in nests:
        nest = nests[nestid]

        # publish message
        nest['pose'].header.stamp = rospy.Time.now()
        nest['pub'].publish(nest['pose'])

        # broadcast transform
        nest['tf'].header.stamp = nest['pose'].header.stamp
        br.sendTransform(nest['tf'])

    r.sleep()
