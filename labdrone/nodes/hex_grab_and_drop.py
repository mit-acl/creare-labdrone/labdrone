#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor

import copy
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
#from acl_msgs.msg import ViconState
#from acl_msgs.msg import FloatStamped
from std_msgs.msg import Float32
from math import sin, cos
from tf.transformations import euler_from_quaternion
# local imports
from snapstack_msgs.msg import QuadGoal
import numpy as np

from labdrone import utils

import time
import tf2_ros

NOT_FLYING = 0
FLYING = 1
LANDING = 2
TAKE = 3


END=12


TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4
ATTITUDE = 5

LEFT_X = 0
LEFT_Y = 1
RIGHT_X = 3
RIGHT_Y = 4

GRABBING=0
DROPPING=1

MISSION_MODE=0
JOY_MODE=1

A = 0
B = 1
X = 2
Y = 3
RB = 5
BACK = 6
START = 7

CONTROL_DT = 0.01
MAX_ACCEL_XY = 2.0
MAX_ACCEL_Z = 0.8

# ROOM Bounds
XMIN = -4
XMAX = 4
YMIN = -4
YMAX = 4
ZMIN = 0
ZMAX = 4

START_COUNTER=1


# ball_x1=0.2
# ball_x2=0.2
# ball_x3=0.2
# ball_yaw=0.2

class QuadJoy:

    def __init__(self):
        #self.initialized=False
        self.status = NOT_FLYING
        self.transmitting = True
        self.wpType = DISABLE

        self.goal = QuadGoal()
        self.goal.xy_mode = self.goal.MODE_POS
        self.goal.z_mode = self.goal.MODE_POS
        # self.goal.waypointType = DISABLE
        self.recent_goal = QuadGoal()
        self.pose = Pose()
        self.pubGoal = rospy.Publisher('goal', QuadGoal, queue_size=1)
        self.pubGripper = rospy.Publisher('servo', Float32, queue_size=1)
        self.demo_mode = rospy.get_param('~demo', False)
        self.att_thrust_mode = rospy.get_param('~joyAttitude', False)
        self.spinup_time = rospy.get_param('cntrl/spinup_time',0.5)
        self.alt = 1.1
        self.im = 1 #index mission
        self.x4=np.array([0.0,0.0,0.0])
        self.x4_yaw=0.0

        self.extra_executed=False
        
        self.mode=MISSION_MODE
        self.part_mission=GRABBING

        self.yaw_initialized_for_dropping=False

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
        rospy.sleep(2)

        self.grab_done=False



    def gripperOpen(self):  
        self.pubGripper.publish(Float32(data=1.0))
        print("Open Gripper")


    def gripperLooseHold(self):
        self.pubGripper.publish(Float32(data=0.0))
        print("Loose Hold")


    def gripperTightHold(self): 
        self.pubGripper.publish(Float32(data=0.0))
        print("Tight Hold")

    def gripperNoPower(self): 
        self.pubGripper.publish(Float32(data=2))
        print("Publishing Gripper=2 (No Power)")
        


    def goalCB(self, data):
        self.recent_goal = data

    def sendGoal(self):
        #print("GOAL=", self.goal)
        # self.goal.waypointType = self.wpType
        if self.wpType == DISABLE:
            self.goal.cut_power = True
        else:
            self.goal.cut_power = False
        self.goal.header.stamp = rospy.get_rostime()

        self.pubGoal.publish(self.goal)

    def findTransform(self, name1, name2):
            trans = self.tfBuffer.lookup_transform(name1, name2, rospy.Time(0))
            x=np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z])
            (x_yaw, _, _)=euler_from_quaternion((trans.transform.rotation.x,trans.transform.rotation.y,trans.transform.rotation.z,trans.transform.rotation.w), "szyx")
            return(x, x_yaw)

    def nestCB(self,data):

        x_offset = 0.02
        y_offset = 0.0
        z_offset = 0.054 #was 0.064

        # if(self.part_mission==DROPPING):
        #     z_offset = 0.064

        try:
            (self.nest, self.nest_yaw) = self.findTransform('world', 'nest')
            (self.x3, self.x3_yaw) = self.findTransform('world', 'x3')
            (self.x2, self.x2_yaw) = self.findTransform('world', 'x2')
            (self.x1, self.x1_yaw) = self.findTransform('world', 'x1')

            self.nest_yaw=self.wrapPi(self.nest_yaw) 

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print "findTransform didn't work"
            return

        self.xextra=self.x1

        #Apply offsets
        self.x1  =    self.x1+  np.array([x_offset, y_offset, z_offset]);
        self.x2  =    self.x2+  np.array([x_offset, y_offset, z_offset]);
        self.x3  =    self.x3+  np.array([x_offset, y_offset, z_offset]);
        self.nest  =  self.nest+np.array([x_offset, y_offset, z_offset]);

        # print("self.x4=",self.x4);
        # print("self.x4_yaw=",self.x4_yaw);
        # print("self.x1=",self.x1);
        # print("self.x2=",self.x2);
        # print("self.x3=",self.x3);
        # print("self.xextra=",self.xextra);

        
    def print_mission_status(self):
        if self.im<len(self.x_all):
            print(self.name_all[self.im])
        else:
            pass
            #print("End of Mission")

    def wrapPi(self, x):
        x=(x+np.pi) % (2 * np.pi)
        if(x<0):
            x=x+2 * np.pi
        return x-np.pi    

    def getIndexOf(self, data):
        return self.name_all.index(data);

    def getNextIndexOf(self, data):
        return self.name_all.index(self.name_next_all[self.getIndexOf(data)]);

    def poseCB(self, data):
        #print("X1= ",self.x1)
        #print("x4= ",self.x4)
        #print "In poseCB"

        self.pose = data.pose
        self.position=np.array([data.pose.position.x, data.pose.position.y, data.pose.position.z])
        self.yaw=utils.quat2yaw(data.pose.orientation)
        
        if(self.status == NOT_FLYING):
            self.x0=np.array([data.pose.position.x, data.pose.position.y, data.pose.position.z+self.alt])
            #print("self.x0= ", self.x0)
            self.yaw_init=utils.quat2yaw(data.pose.orientation)

        if(self.status == FLYING and self.mode==MISSION_MODE):    

            self.v_max_yaw=0.6

            self.name_all=                [    "PHASE_0",         "WAIT_0",         "PHASE_1",         "WAIT_1",        "PHASE_2",   "PHASE_3",               "WAIT_3",        "PHASE_4"]    # From this phase the drone transitions...
            self.name_next_all=           [     "WAIT_0",        "PHASE_1",          "WAIT_1",        "PHASE_2",        "PHASE_3",    "WAIT_3",              "PHASE_4",         "WAIT_1"]    # ...to this phase  
            self.x_all=          np.array([      self.x0,          self.x0,           self.x1,          self.x1,          self.x2,     self.x3,                self.x3,          self.x1])   # Waypoint at the end of each phase
            self.yaw_all=        np.array([self.yaw_init,    self.yaw_init,     self.nest_yaw,    self.nest_yaw,    self.nest_yaw,     self.nest_yaw,    self.nest_yaw,    self.nest_yaw])  # Yaw angle at the end of each phase
            self.t_all=          np.array([            5,               2,                 10,               3,                10,          8,                       3,                10])  # Time spent on each phase 

            #self.v_all=          np.array([          0.5,                0,               0.5,              0.0,              0.2,        0.03,                      0,              0.1 ])  # Velocity in each phase (0 for waiting phases)
            # self.k_all=          np.zeros(self.x_all.size);
            # #Compute  self.k_all
            # for j in range(1,len(self.x_all)):
            #     if(self.v_all[j]>0):
            #             self.k_all[j]= self.v_all[j]/max(np.linalg.norm( self.x_all[j]- self.x_all[j -1]),  self.v_all[j])
            #     else: #waiting point
            #             self.k_all[j]=1/2.0

            self.print_mission_status()
            #print "Time=", self.time_from_init_phase(None)
            if(self.time_from_init_phase(None)>self.t_all[self.im]):
                self.im=self.getNextIndexOf(self.name_all[self.im]);

                self.time_from_init_phase(START_COUNTER)

                if(self.im==self.getIndexOf("PHASE_4")):
                    if(self.part_mission==DROPPING):
                        self.gripperOpen()
                    if(self.part_mission==GRABBING):
                        self.gripperTightHold()
                        self.goal.pos.z +=0.01 #Once it's grabbed, lift a little 
                        self.grab_done=True;

                if(self.im==self.getIndexOf("WAIT_1") and self.part_mission==DROPPING):
                    print("**************** END OF MISSION, PRESS X TO LAND ***********")
                    self.mode=JOY_MODE
                    self.im=END
                    return
                if(self.im==self.getIndexOf("WAIT_1") and self.grab_done==True):
                    self.part_mission=DROPPING

            # dist= self.x_all[self.im]- self.x_all[self.im-1];
            # x= self.x_all[self.im-1] + self.k_all[self.im]*(dist)*self.time_from_init_phase(None)         
            # v= self.k_all[self.im]*dist
 
            # dist_yaw= self.yaw_all[self.im]- self.yaw_all[self.im-1];   #desired_yaw-self.yaw_init
            # #self.kyaw=self.v_max_yaw/max(np.linalg.norm(dist_yaw), self.v_max_yaw)  
            # self.goal.yaw=self.yaw_all[self.im-1]+self.k_all[self.im]*dist_yaw*self.time_from_init_phase(None)
            # self.goal.dyaw = self.k_all[self.im]*dist_yaw

            #Set position and velocities
            x0=np.append(self.x_all[self.im-1], self.yaw_all[self.im-1])
            xf=np.append(self.x_all[self.im],   self.yaw_all[self.im])
            tf=self.t_all[self.im]
            t=self.time_from_init_phase(None)

            self.goal = self.getTriangularProfile(x0, xf, tf, t)
            self.sendGoal()

    def getTriangularProfile(self, x0, xf, tf, t): #Get a triangular velocity profile


        a_pos=((xf-x0)/2.0)*2*(1/((tf/2.0)**2)); #Solution of the equation x0+(xf-xf)/2 = x0 + v0t + 0.5*a*t2 (with v0=0)
        a_neg= -a_pos;

        v_peak=a_pos*(tf/2.0); #v in the peak of the triangle
        x_peak=x0 + (xf-x0)/2.0 # This is the same as x0 + 0*(tf/2.0) + 0.5*a_pos*((tf/2.0)**2)


        if(t<=(tf/2.0)):
            a=a_pos
            v=0.0 + a*t
            x=x0 + 0*t + 0.5*a*(t**2)
        else:
            a=a_neg
            v=v_peak + a*(t-tf/2.0)      
            x=x_peak + v_peak*(t-tf/2.0) + 0.5*a*(t-tf/2.0)**2; 

        goal=QuadGoal()
        goal.pos.x = x[0]
        goal.pos.y = x[1]
        goal.pos.z = x[2]
        goal.vel.x = v[0]
        goal.vel.y = v[1]
        goal.vel.z = v[2]
        goal.accel.x = a[0]
        goal.accel.y = a[1]
        goal.accel.z = a[2]

        goal.yaw= x[3]
        goal.dyaw= v[3]

        return goal



    def time_from_init_phase(self, data):
        if (data==START_COUNTER):
            self.t0=rospy.get_time()
        #print("Counter= ",time.time()-self.t0)
        return rospy.get_time()-self.t0


    def joyCB(self, data):
        #print("Entrando en JoyCallBack")
        #print("self.status=", self.status)
        if self.status == NOT_FLYING:
            #print("Here")
            self.goal.yaw = utils.quat2yaw(self.pose.orientation)

        if data.buttons[Y]:
            self.mode=JOY_MODE
        
        # takeoff
        #print(data)
        if data.buttons[A] and self.status == NOT_FLYING:
            print "Taking off"
            self.goal.pos.z=self.pose.position.z;
            print ("self.goal.pos.z=", self.goal.pos.z)
            if(self.part_mission==DROPPING):
                self.gripperTightHold()
            if(self.part_mission==GRABBING):
                self.gripperOpen()
            self.t0 = rospy.get_time()
            self.status = TAKE
            self.wpType = TAKEOFF
            rospy.loginfo("Waiting for spin up")


            # set initial goal to current pose
            self.goal.pos = copy.copy(self.pose.position)
            self.goal.vel.x = self.goal.vel.y = self.goal.vel.z = 0
            self.goal.yaw = utils.quat2yaw(self.pose.orientation)
            # if self.demo_mode:

        # emergency disable
        elif data.buttons[B] and self.status != NOT_FLYING:
            rospy.loginfo("Killing")
            self.status = NOT_FLYING
            self.wpType = DISABLE

        # landing
        elif data.buttons[X] and self.status == FLYING:
            self.status = LANDING
            # self.wpType = LAND
            self.goal.vel.x = 0
            self.goal.vel.y = 0
            self.goal.vel.z = 0
            self.goal.dyaw = 0
        
        #A, B, X, Y are not pressed, let's check the knobs. This part is also executed when knobs are in their eq. position
        elif self.status == FLYING: 
            self.wpType = 0

            if (self.mode==JOY_MODE):
                # get velocities from joystick
                dx = data.axes[RIGHT_Y] * 0.02 / CONTROL_DT
                dy = data.axes[RIGHT_X] * 0.02 / CONTROL_DT
                dz = data.axes[LEFT_Y] * 0.005 / CONTROL_DT
                dyaw = data.axes[LEFT_X] * 0.02 / CONTROL_DT

                # rate limit velocities
                dx = utils.rateLimit(dx, self.goal.vel.x, -MAX_ACCEL_XY,
                                     MAX_ACCEL_XY, CONTROL_DT)
                dy = utils.rateLimit(dy, self.goal.vel.y, -MAX_ACCEL_XY,
                                     MAX_ACCEL_XY, CONTROL_DT)
                dz = utils.rateLimit(dz, self.goal.vel.z, -MAX_ACCEL_Z,
                                     MAX_ACCEL_Z, CONTROL_DT)


                # saturate to room bounds
                gx = self.goal.pos.x
                gy = self.goal.pos.y
                gz = self.goal.pos.z
                xsat = [0]
                ysat = [0]
                zsat = [0]
                #print("Setting goal.pos= ",self.goal.pos.x)
                self.goal.pos.x = utils.saturate(gx + dx * CONTROL_DT,
                                                 max(XMAX, gx), min(XMIN, gx),
                                                 xsat)
                self.goal.pos.y = utils.saturate(gy + dy * CONTROL_DT,
                                                 max(YMAX, gy), min(YMIN, gy),
                                                 ysat)
                self.goal.pos.z = utils.saturate(gz + dz * CONTROL_DT,
                                                 max(ZMAX, gz), min(ZMIN, gz),
                                                 zsat)
                # Set velocities to zero if positions are saturated
                if (xsat[0] == -1 or xsat[0] == 1):
                    dx = utils.rateLimit(0.0, self.goal.vel.x,
                                         -MAX_ACCEL_XY, MAX_ACCEL_XY,
                                         CONTROL_DT)
                if (ysat[0] == -1 or ysat[0] == 1):
                    dy = utils.rateLimit(0.0, self.goal.vel.y,
                                         -MAX_ACCEL_XY, MAX_ACCEL_XY,
                                         CONTROL_DT)
                if (zsat[0] == -1 or zsat[0] == 1):
                    dz = utils.rateLimit(0.0, self.goal.vel.z,
                                         -MAX_ACCEL_Z, MAX_ACCEL_Z,
                                         CONTROL_DT)

                # set velocities

                self.goal.vel.x = dx
                self.goal.vel.y = dy
                self.goal.vel.z = dz

                self.goal.yaw = utils.wrap(self.goal.yaw + dyaw * CONTROL_DT)
                self.goal.dyaw = dyaw
                # self.goal.heading = 0
                # self.goal.dheading = 0

        if self.status == LANDING:
            if self.pose.position.z > 0.4:
                # fast landing
                self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.0035,
                                                 2.0, -0.1)
            else:
                # slow landing
                self.goal.pos.z = utils.saturate(self.goal.pos.z - 0.001,
                                                 2.0, -0.1)
            if self.goal.pos.z == -0.1:
                self.status = NOT_FLYING
                self.wpType = DISABLE
                rospy.loginfo("Landed!")


        if self.status == TAKE:
            if (rospy.get_time()-self.t0 > self.spinup_time):
                self.goal.pos.z = utils.saturate(self.goal.pos.z+0.0035, self.x0[2],-0.1)
                print("self.goal.pos.z= ", self.goal.pos.z)
                print ("Error=", np.abs(self.alt-self.pose.position.z))
                if (np.abs(self.x0[2]-self.pose.position.z) < 0.1 and self.goal.pos.z>=self.x0[2]):
                    self.status = FLYING
                    rospy.loginfo("Takeoff complete!")


        # stop transmitting data from joystick
        if data.buttons[BACK] and self.transmitting:
            self.transmitting = False
            self.goal.vel.x = 0
            self.goal.vel.y = 0
            self.goal.vel.z = 0
            self.goal.dyaw = 0
            for i in range(3):
                self.sendGoal()

        # start transmitting data from joystick
        if data.buttons[START] and not self.transmitting:
            self.transmitting = True
            self.goal = self.recent_goal

        # send thrust and attitude information directly to quad
        # goal.pos.x = roll
        # goal.pos.y = pitch
        # goal.vel.x = roll_rate (p)
        # goal.vel.y = pitch_rate (q)
        # goal.accel.x = throttle
        if self.att_thrust_mode and self.status == FLYING:
            print("EN ABAJO")
            self.wpType = ATTITUDE

            maxAng = np.pi / 4.0*1.2   # max commanded angle in
            rollCmd = -data.axes[RIGHT_X] * maxAng  # right_x
            pitchCmd = data.axes[RIGHT_Y] * maxAng  # right_y
            self.goal.dyaw = data.axes[LEFT_X] * .005  # left
            self.goal.yaw += self.goal.dyaw
            self.goal.pos.x = rollCmd
            self.goal.pos.y = pitchCmd
            # zero the commanded rates during joystick flying
            self.goal.vel.x = 0.0
            self.goal.vel.y = 0.0

            # get throttle
            self.goal.accel.x += data.axes[LEFT_Y]/1000
            self.goal.accel.x = utils.saturate(self.goal.accel.x ,1,0)

        if self.transmitting:
            self.sendGoal()
            

def startNode():
    c = QuadJoy()
    #print("Abajo")
    rospy.Subscriber("joy", Joy, c.joyCB)
    rospy.Subscriber("world", PoseStamped, c.poseCB)
    rospy.Subscriber("goal", QuadGoal, c.goalCB)
    rospy.Subscriber("/nest/world", PoseStamped, c.nestCB)


    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('joy')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=mQ01 $ rosrun quad_control joy.py")
        else:
            print "Starting joystick teleop node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass