#!/bin/bash

usage="$(basename "$0") [-h] [-s <N>] [-n <n>] [-p <poslist>]

where:
    -h           show this help text
    -s <N>       launches a simulation (of N vehicles) locally
    -n <n>       targets a different labdroneXX vehicle (default 01)
    -p <poslist> Start simulated drones at specific positions.  Must have a specific form:
                  \"('x:=<x0> y:=<y0> z:=<z0>' 'x:=<x1> y:=<y1> z:=<z1>' ... 'x:=<xn> y:=<yn> z:=<zn>')\"
                 where <xn>, <yn>, <zn> are numbers.  You may omit axes but must have a string for each vehicle.
                 These strings will be appended to the commandline for roslaunch labdrone sim.launch.
"

VEHTYPE=labdrone
VEHNUM_START=01
N=1 # number of vehicles
S= # if simulation, then S=s
VEH_POSITIONS=

while getopts 'hs:n:p:' option; do
  case "$option" in
    h)
      echo "$usage"
      exit
      ;;
    s)
      N=$OPTARG
      S=s
      ;;
    n)
      VEHNUM_START=$OPTARG
      ;;
    p)
      # Treat string as bash array of strings
      declare -a VEH_POSITIONS=$OPTARG
      ;;
   \?)
      printf "illegal option: -%s\n" "$OPTARG" >&2
      echo "$usage" >&2
      exit 1
      ;;
  esac
done

SESSION=labdrone # tmux session name
cmd="new-session -d -s $SESSION"

for idx in $(seq -f "%02g" 1 $N); do

  # Set vehnum sequentially, starting at specified start number
  VEHNUM=$(printf "%02d" $(($VEHNUM_START+$(($idx-1)))))

  VEH=$VEHTYPE$VEHNUM$S

  echo "Starting vehicle $VEH"

  if [ -z "$S" ]; then
      # check that an ssh connection can even be made
      ssh -q root@$VEH.local exit
      if [ $? -ne 0 ]; then
          echo -e "\033[0;31mNo connection to $VEH\033[0m"
          exit
      fi
  fi

  # window number (0-indexed)
  w=$(($idx-1))

  if [ $idx -eq "1" ]; then
    cmd="$cmd ; rename-window $VEH"
  else
    cmd="$cmd ; new-window -n $VEH"
  fi
  cmd="$cmd ; split-window -h"
  cmd="$cmd ; split-window -v"
  cmd="$cmd ; select-pane -t $SESSION:$w.0"
  cmd="$cmd ; split-window -v"
  cmd="$cmd ; select-pane -t $SESSION:$w.2"
  cmd="$cmd ; split-window -v"
  cmd="$cmd ; select-pane -t $SESSION:$w.1"
  cmd="$cmd ; split-window -v"
  cmd="$cmd ; select-pane -t $SESSION:$w.5"
  cmd="$cmd ; split-window -v"

done

# Create tmux session
tmux -2 $cmd

for idx in $(seq -f "%02g" 1 $N); do

  # Set vehnum sequentially, starting at specified start number
  VEHNUM=$(printf "%02d" $(($VEHNUM_START+$(($idx-1)))))

  # window number (0-indexed)
  w=$(($idx-1))

  # vehicle
  VEH=$VEHTYPE$VEHNUM$S

  for _pane in $(tmux list-pane -F '#P'); do
      if [ -z "$S" ]; then
          tmux send-keys -t $SESSION:$w.${_pane} "ssh root@$VEH.local" C-m
          sleep 1
      else
          tmux send-keys -t $SESSION:$w.${_pane} "export VEHTYPE=$VEHTYPE && export VEHNUM=$VEHNUM$S" C-m
      fi
  done

  if [ -z "$S" ]; then
      tmux send-keys -t $SESSION:$w.4 "systemctl status imu" C-m
  else
      if [ -z "$VEH_POSITIONS" ]; then
        # Generate start position sequentially
        pos="x:=$((2*$w))"
      else
        pos=${VEH_POSITIONS[$(($idx-1))]}
      fi
      echo "Position: $pos"
      tmux send-keys -t $SESSION:$w.4 "roslaunch labdrone sim.launch num:=$VEHNUM $pos" C-m
  fi
  tmux send-keys -t $SESSION:$w.5 "roslaunch snap snap.launch" C-m
  tmux send-keys -t $SESSION:$w.0 "roslaunch outer_loop cntrl.launch" C-m
  tmux send-keys -t $SESSION:$w.3 "roslaunch snap esc.launch" C-m
  sleep 2 # TODO: intead of this hack, run nest discovery occasionally
  tmux send-keys -t $SESSION:$w.1 "roslaunch labdrone mission_manager.launch" C-m
  # tmux send-keys -t $SESSION:$w.2 ""
  # tmux send-keys -t $SESSION:$w.6 ""

  tmux select-pane -t $SESSION:$w.1

done

tmux select-window -t $SESSION:0
tmux -2 attach-session -t $SESSION