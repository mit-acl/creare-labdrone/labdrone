/**
 * @file all.cpp
 * @brief Resolves static constexpr not being defined
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 June 2020
 */

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {
namespace state {

//\brief Define the SNAME static members (https://stackoverflow.com/q/8016780/2392520)
constexpr char Base::SNAME[];
constexpr char Takeoff::SNAME[];
constexpr char Wait::SNAME[];
constexpr char Hover::SNAME[];
constexpr char FollowTrajectory::SNAME[];
constexpr char FastTravel::SNAME[];
constexpr char Transaction::SNAME[];
constexpr char Manual::SNAME[];
constexpr char Landing::SNAME[];
constexpr char Shutdown::SNAME[];
constexpr char StartUp::SNAME[];
constexpr char DockTakeoff::SNAME[];
constexpr char DockLanding::SNAME[];
constexpr char Docked::SNAME[];
constexpr char WaitAtDock::SNAME[];
constexpr char GoToPickup::SNAME[];
constexpr char Pickup::SNAME[];
constexpr char WaitAtPickup::SNAME[];
constexpr char GoToDropoff::SNAME[];
constexpr char Dropoff::SNAME[];
constexpr char WaitAtDropoff::SNAME[];
constexpr char ReroutePending::SNAME[];
constexpr char GoToDock::SNAME[];
constexpr char EmergencyLanding::SNAME[];
constexpr char ObstacleDetected::SNAME[];

} // ns state
} // ns labdrone
} // ns creare
