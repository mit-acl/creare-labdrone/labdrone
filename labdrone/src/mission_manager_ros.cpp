/**
 * @file mission_manager_ros.cpp
 * @brief ROS wrapper for the Creare Labdrone Mission Manager
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#include "labdrone/mission_manager_ros.h"

namespace creare {
namespace labdrone {

namespace sp = std::placeholders;

MissionManagerROS::MissionManagerROS(const ros::NodeHandle& nh,
                                      const ros::NodeHandle& nhp)
  : nh_(nh), nhp_(nhp)
{

  //
  // Load paramters
  //

  MissionManager::Parameters p;

  // we should always expect ground truth data
  p.truth_available = true;

  // room bounds
  nh_.param<double>("/room_bounds/x_min", p.bounds.x_min, 0.0);
  nh_.param<double>("/room_bounds/x_max", p.bounds.x_max, 1.0);
  nh_.param<double>("/room_bounds/y_min", p.bounds.y_min, 0.0);
  nh_.param<double>("/room_bounds/y_max", p.bounds.y_max, 1.0);
  nh_.param<double>("/room_bounds/z_min", p.bounds.z_min, 0.0);
  nh_.param<double>("/room_bounds/z_max", p.bounds.z_max, 1.0);

  nh_.param<double>("cntrl/spinup/time", p.spinup_time, 2.0);
  nhp_.param<double>("control_dt", control_dt_, 0.01);
  nhp_.param<double>("nominal_alt", p.nominal_alt, 1.0);
  nhp_.param<double>("takeoff/vel", p.takeoff_vel, 0.35);
  nhp_.param<double>("landing/fast_threshold", p.landing_fast_threshold, 0.400);
  nhp_.param<double>("landing/fast_dec", p.landing_fast_dec, 0.0035);
  nhp_.param<double>("landing/slow_dec", p.landing_slow_dec, 0.001);
  nhp_.param<double>("safety/max_accel_xy", p.max_accel_xy, 0.5);
  nhp_.param<double>("safety/max_accel_z", p.max_accel_z, 0.8);
  nhp_.param<double>("safety/obstacle_stop/tf", p.stoptraj_tf, 3.0);
  nhp_.param<double>("safety/obstacle_stop/veh_radius", p.stoptraj_vehradius, 0.6);
  nhp_.param<double>("safety/obstacle_stop/corridor_dt", p.stoptraj_corrdt, 0.05);
  nhp_.param<bool>("safety/obstacle_stop/autoreturn", p.stoptraj_autoreturn, false);
  nhp_.param<double>("traj/fast/max_vel", p.traj_fast_vmax, 2.0);
  nhp_.param<double>("traj/fast/max_accel", p.traj_fast_amax, 2.0);
  nhp_.param<double>("traj/slow/max_vel", p.traj_slow_vmax, 0.3);
  nhp_.param<double>("traj/slow/max_accel", p.traj_slow_amax, 0.5);
  nhp_.param<double>("traj/transact/max_vel", p.traj_transact_vmax, 0.3);
  nhp_.param<double>("traj/transact/max_accel", p.traj_transact_amax, 0.5);
  nhp_.param<double>("traj/err_radius", p.traj_err_radius, 0.5);
  nhp_.param<double>("traj/yaw/rate", p.yaw_rate, 0.8);
  nhp_.param<double>("traj/yaw/init_err_thr", p.yaw_init_err_thr, 0.01);
  nhp_.param<bool>("traj/yaw/face_velocity", p.yawlaw_velocity_vector, false);
  loadParam(nhp_, "transaction/x1", p.x1);
  loadParam(nhp_, "transaction/x2", p.x2);
  loadParam(nhp_, "transaction/x3", p.x3);
  nhp_.param<double>("transaction/dropoff_z_shift", p.dropoff_z_shift, 0.01);
  nhp_.param<double>("transaction/max_vel", p.transaction_vmax, 0.1);
  nhp_.param<double>("transaction/max_accel", p.transaction_amax, 0.1);
  nhp_.param<double>("transaction/gripper_wait_sec", p.gripper_wait_sec, 2);
  nhp_.param<double>("transaction/rbm_update_period", p.rbm_update_period, 0.1);
  nhp_.param<double>("transaction/rbm_ramp_duration", p.rbm_ramp_duration, 3);
  loadParam(nhp_, "transaction/rbm_delta", p.rbm_delta);
  nhp_.param<double>("transaction/nest_yaw_offset", p.nest_yaw_offset, 0);
  loadParam(nh_, "snap/r_bM", p.nominal_rbM);
  nhp_.param<double>("joy/kx", joy_kx_, 2.0);
  nhp_.param<double>("joy/ky", joy_ky_, 2.0);
  nhp_.param<double>("joy/kz", joy_kz_, 0.5);
  nhp_.param<double>("joy/kr", joy_kr_, 2.0);
  nhp_.param<bool>("joy/body_frame", joy_body_frame_, false);

  //
  // Initialize Mission Manager
  //

  mission_manager_.reset(new MissionManager(p));
  mission_manager_->registerNotifyTransitionCb(
                        std::bind(&MissionManagerROS::notifyTransitionCb, this,
                                              sp::_1, sp::_2, sp::_3, sp::_4));
  //TODO What is this? Is this intended for obstruction notification?
  mission_manager_->registerNotifyTrajectoryCb(
                        std::bind(&MissionManagerROS::notifyTrajectoryCb, this,
                                              sp::_1));
  mission_manager_->registerNotifyObstructionCb(
                        std::bind(&MissionManagerROS::notifyObstructionCb, this,
                                              sp::_1, sp::_2, sp::_3));
  mission_manager_->registerActuateGripperCb(
                        std::bind(&MissionManagerROS::actuateGripperCb, this,
                                              sp::_1));
  mission_manager_->registerUpdateCoMOffsetCb(
                        std::bind(&MissionManagerROS::updateCoMOffsetCb, this,
                                              sp::_1));

  //
  // Timers
  //

  tim_tick_ = nh_.createTimer(ros::Duration(control_dt_),
                                      &MissionManagerROS::tickCb, this);

  //
  // Pub/sub communication
  //

  sub_world_ = nh_.subscribe("world", 1, &MissionManagerROS::worldCb, this);
  sub_state_ = nh_.subscribe("state", 1, &MissionManagerROS::stateCb, this);
  sub_joy_ = nh_.subscribe("joy", 1, &MissionManagerROS::joyCb, this);
  sub_obs_ = nh_.subscribe("obstacle", 1, &MissionManagerROS::obstacleCb, this);

  pub_cmdout_ = nh_.advertise<snapstack_msgs::Goal>("goal", 1);
  pub_gripper_ = nh_.advertise<std_msgs::Bool>("gripper", 1);
  pub_comoffset_ = nh_.advertise<geometry_msgs::Vector3>("comoffset", 1);
  pub_vizstoptraj_ = nh_.advertise<nav_msgs::Path>("viz_stoptraj", 1, true);
  pub_vizobstacle_ = nh_.advertise<visualization_msgs::MarkerArray>("viz_obstacle", 1, true);
  pub_viztraj_ = nh_.advertise<nav_msgs::Path>("viz_trajectory", 1, true);
  pub_vizwps_ = nh_.advertise<visualization_msgs::MarkerArray>("viz_waypoints", 1, true);
  pub_vizcorridor_ = nh_.advertise<visualization_msgs::MarkerArray>("viz_corridor", 1, true);

  // discover nest mocap topics to subscribe to (for debugging)
  discoverNests();

  //
  // Service server/client
  //

  srv_state_req_ = nh_.advertiseService("StateReq",
                                  &MissionManagerROS::stateReqSrvCb, this);
  srv_get_battery_ = nh_.advertiseService("GetBattery",
                                  &MissionManagerROS::getBatterySrvCb, this);
  srv_get_sm_data_ = nh_.advertiseService("GetSMData",
                                  &MissionManagerROS::getSMDataSrvCb, this);
  srv_client_state_notif_ = nh_.serviceClient<labdrone_msgs::HexSMStateNotification>(
                                  "StateNotification");
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void MissionManagerROS::loadParam(const ros::NodeHandle& nh,
                                  const std::string& p, Eigen::Vector3d& v)
{
  std::vector<double> sv;
  nh.getParam(p, sv);
  for (size_t i=0; i<sv.size(); ++i) {
    v(i) = sv[i];
  }
}

// ----------------------------------------------------------------------------

void MissionManagerROS::discoverNests()
{
  ros::master::V_TopicInfo topic_infos;
  ros::master::getTopics(topic_infos);

  const char delim[] = {'/'};
  for (const auto& tinfo : topic_infos) {

    // "/nest0/world" --> {"", "nest0", "world"}
    std::vector<std::string> elems;
    boost::algorithm::split(elems, tinfo.name, boost::algorithm::is_any_of(delim));

    if (elems.size() == 3 && elems[2] == "world"
          && boost::algorithm::istarts_with(elems[1], "nest"))
    {
      ROS_INFO_STREAM("Discovered nest: " << elems[1]);
      boost::function<void(const geometry_msgs::PoseStampedConstPtr&)> cb =
        [=](const geometry_msgs::PoseStampedConstPtr& msg) {
          nestCb(msg, elems[1]);
        };
      subs_nest_.push_back(nh_.subscribe(tinfo.name, 1, cb));
    }
  }
}

// ----------------------------------------------------------------------------
// ROS Callbacks
// ----------------------------------------------------------------------------

void MissionManagerROS::nestCb(const geometry_msgs::PoseStampedConstPtr& msg,
                              const std::string& nestId)
{
  mission_manager_->setdata().nests[nestId].t = msg->header.stamp.toSec();
  mission_manager_->setdata().nests[nestId].p.x() = msg->pose.position.x;
  mission_manager_->setdata().nests[nestId].p.y() = msg->pose.position.y;
  mission_manager_->setdata().nests[nestId].p.z() = msg->pose.position.z;
  mission_manager_->setdata().nests[nestId].q.x() = msg->pose.orientation.x;
  mission_manager_->setdata().nests[nestId].q.y() = msg->pose.orientation.y;
  mission_manager_->setdata().nests[nestId].q.z() = msg->pose.orientation.z;
  mission_manager_->setdata().nests[nestId].q.w() = msg->pose.orientation.w;
}

// ----------------------------------------------------------------------------

void MissionManagerROS::worldCb(const geometry_msgs::PoseStampedConstPtr& msg)
{
  mission_manager_->setdata().truth.t = msg->header.stamp.toSec();
  mission_manager_->setdata().truth.p.x() = msg->pose.position.x;
  mission_manager_->setdata().truth.p.y() = msg->pose.position.y;
  mission_manager_->setdata().truth.p.z() = msg->pose.position.z;
  mission_manager_->setdata().truth.q.x() = msg->pose.orientation.x;
  mission_manager_->setdata().truth.q.y() = msg->pose.orientation.y;
  mission_manager_->setdata().truth.q.z() = msg->pose.orientation.z;
  mission_manager_->setdata().truth.q.w() = msg->pose.orientation.w;
}

// ----------------------------------------------------------------------------

void MissionManagerROS::stateCb(const snapstack_msgs::StateConstPtr& msg)
{
  frame_id_ = msg->header.frame_id;
  mission_manager_->setdata().state.t = msg->header.stamp.toSec();
  mission_manager_->setdata().state.p.x() = msg->pos.x;
  mission_manager_->setdata().state.p.y() = msg->pos.y;
  mission_manager_->setdata().state.p.z() = msg->pos.z;
  mission_manager_->setdata().state.q.x() = msg->quat.x;
  mission_manager_->setdata().state.q.y() = msg->quat.y;
  mission_manager_->setdata().state.q.z() = msg->quat.z;
  mission_manager_->setdata().state.q.w() = msg->quat.w;
  mission_manager_->setdata().twist.v.x() = msg->vel.x;
  mission_manager_->setdata().twist.v.y() = msg->vel.y;
  mission_manager_->setdata().twist.v.z() = msg->vel.z;
}

// ----------------------------------------------------------------------------

void MissionManagerROS::joyCb(const sensor_msgs::JoyConstPtr& msg)
{
  Eigen::Vector3d v;
  v.x() = msg->axes[Joy::RIGHT_Y] * joy_kx_;
  v.y() = msg->axes[Joy::RIGHT_X] * joy_ky_;
  v.z() = msg->axes[Joy::LEFT_Y] * joy_kz_;

  // if joystick commands are to be interpreted as in the body frame,
  // then express them in the world frame for the autopilot
  if (joy_body_frame_) {
    v = mission_manager_->data().state.q * v;
  }

  mission_manager_->setdata().joy.t = msg->header.stamp.toSec();
  mission_manager_->setdata().joy.v = v;
  mission_manager_->setdata().joy.r = msg->axes[Joy::LEFT_X] * joy_kr_;
  mission_manager_->setdata().joy.btns.resize(msg->buttons.size());
  for (size_t i=0; i<msg->buttons.size(); ++i) {
    mission_manager_->setdata().joy.btns[i] = msg->buttons[i] != 0;
  }

  bool LB = mission_manager_->data().joy.btns[Joy::LB];
  bool RB = mission_manager_->data().joy.btns[Joy::RB];
  if (LB || RB) {
    auto err = mission_manager_->requestStateChange(state::Manual::SNUM, {}, {});
  }
}

// ----------------------------------------------------------------------------

void MissionManagerROS::obstacleCb(const geometry_msgs::PointStamped& msg)
{
  if (!mission_manager_->data().obstacle_detected) {

    labdrone::Pose pose;
    pose.t = msg.header.stamp.toSec();
    pose.p.x() = msg.point.x;
    pose.p.y() = msg.point.y;
    pose.p.z() = msg.point.z;
    pose.q = Eigen::Quaterniond::Identity();

    auto err = mission_manager_->requestStateChange(state::ObstacleDetected::SNUM, {}, pose);
  }
}

// ----------------------------------------------------------------------------
// Timer Callback Handlers
// ----------------------------------------------------------------------------

void MissionManagerROS::tickCb(const ros::TimerEvent& event)
{
  // have the mission manager tick the current mode; get goal
  auto goal = mission_manager_->tick(ros::Time::now().toSec());

  //
  // Publish goal message to outer loop
  //

  // Convert Goal to Goal ROS message
  snapstack_msgs::Goal goalmsg;
  goalmsg.mode_xy = snapstack_msgs::Goal::MODE_POSITION_CONTROL;
  goalmsg.mode_z = snapstack_msgs::Goal::MODE_POSITION_CONTROL;
  goalmsg.p.x = goal.p.x();
  goalmsg.p.y = goal.p.y();
  goalmsg.p.z = goal.p.z();
  goalmsg.v.x = goal.v.x();
  goalmsg.v.y = goal.v.y();
  goalmsg.v.z = goal.v.z();
  goalmsg.a.x = goal.a.x();
  goalmsg.a.y = goal.a.y();
  goalmsg.a.z = goal.a.z();
  goalmsg.j.x = 0;
  goalmsg.j.y = 0;
  goalmsg.j.z = 0;
  goalmsg.psi = goal.Y;
  goalmsg.dpsi = goal.r;
  goalmsg.power = goal.flying;
  pub_cmdout_.publish(goalmsg);
}

// ----------------------------------------------------------------------------
// Mission Manager Callback Handlers
// ----------------------------------------------------------------------------

void MissionManagerROS::notifyTransitionCb(
                            uint8_t last_snum, std::string last_sname,
                            uint8_t snum, std::string sname)
{
  ROS_WARN_STREAM("Transition: " << last_sname << " to " << sname);
  labdrone_msgs::HexSMStateNotification srv;
  //TODO populate other fields. New state and loaded status are the most important for basic control
  srv.request.hexData.state.state = snum;
  srv.request.hexData.loaded = mission_manager_->data().loaded;
  srv_client_state_notif_.call(srv);
}

// ----------------------------------------------------------------------------

void MissionManagerROS::notifyTrajectoryCb(const acl::polytraj::Trajectory& traj)
{
  ROS_WARN_STREAM("New trajectory: " << traj);

  // publish trajectory as a path for visualization
  static constexpr double DT = 0.01;
  nav_msgs::Path path = acl::polytraj_ros::sample_trajectory_into_path(traj, DT);
  pub_viztraj_.publish(path);

  // straight-line path and waypoints & safety corridor
  visualization_msgs::MarkerArray msgwps = 
                      acl::polytraj_ros::create_waypoint_path_markers(traj);
  visualization_msgs::MarkerArray msgcor =
                      acl::polytraj_ros::create_safety_corridor_markers(traj);

  pub_vizwps_.publish(msgwps);
  pub_vizcorridor_.publish(msgcor);
}

// ----------------------------------------------------------------------------

void MissionManagerROS::notifyObstructionCb(const PolyTrajPtr& traj,
                      const Eigen::Vector3d& obstacle, const Eigen::Affine3d& T_wb)
{

  ROS_WARN_STREAM("Obstacle detected at " << obstacle.transpose());

  nav_msgs::Path path;
  path.header.stamp = ros::Time::now();
  path.header.frame_id = "world";

  static constexpr double dc = 0.1;
  const double tf = mission_manager_->params().stoptraj_tf;
  for (double t_eval=0; t_eval <= tf; t_eval += dc) {
    const Eigen::Vector3d b_pos = traj->eval(t_eval, 0).transpose();
    const Eigen::Vector3d w_pos = T_wb * b_pos;

    geometry_msgs::PoseStamped pose_stamped;
    pose_stamped.pose.position.x = w_pos.x();
    pose_stamped.pose.position.y = w_pos.y();
    pose_stamped.pose.position.z = w_pos.z();
    path.poses.push_back(pose_stamped);
  }

  pub_vizstoptraj_.publish(path);

  //
  // Obstacle visualization
  //

  visualization_msgs::Marker obs;
  obs.header = path.header;
  obs.ns = "obstacle";
  obs.id = 0;
  obs.type = visualization_msgs::Marker::SPHERE;
  obs.action = visualization_msgs::Marker::ADD;
  obs.pose.position.x = obstacle.x();
  obs.pose.position.y = obstacle.y();
  obs.pose.position.z = obstacle.z();
  obs.pose.orientation.x = 0;
  obs.pose.orientation.y = 0;
  obs.pose.orientation.z = 0;
  obs.pose.orientation.w = 1;
  obs.scale.x = 0.1;
  obs.scale.y = 0.1;
  obs.scale.z = 0.1;
  obs.color.a = 1;
  obs.color.r = 1;
  obs.color.g = 1;
  obs.color.b = 0;

  visualization_msgs::Marker obsbuf;
  obsbuf.header = path.header;
  obsbuf.ns = "paddedobstacle";
  obsbuf.id = 1;
  obsbuf.type = visualization_msgs::Marker::SPHERE;
  obsbuf.action = visualization_msgs::Marker::ADD;
  obsbuf.pose.position.x = obstacle.x();
  obsbuf.pose.position.y = obstacle.y();
  obsbuf.pose.position.z = obstacle.z();
  obsbuf.pose.orientation.x = 0;
  obsbuf.pose.orientation.y = 0;
  obsbuf.pose.orientation.z = 0;
  obsbuf.pose.orientation.w = 1;
  obsbuf.scale.x = mission_manager_->params().stoptraj_vehradius;
  obsbuf.scale.y = mission_manager_->params().stoptraj_vehradius;
  obsbuf.scale.z = mission_manager_->params().stoptraj_vehradius;
  obsbuf.color.a = 0.2;
  obsbuf.color.r = 1;
  obsbuf.color.g = 0;
  obsbuf.color.b = 0;

  visualization_msgs::MarkerArray msg;
  msg.markers.push_back(obs);
  msg.markers.push_back(obsbuf);
  pub_vizobstacle_.publish(msg);
}

// ----------------------------------------------------------------------------

void MissionManagerROS::actuateGripperCb(bool pickup)
{
  std_msgs::Bool msg;
  msg.data = pickup;
  pub_gripper_.publish(msg);
}

// ----------------------------------------------------------------------------

void MissionManagerROS::updateCoMOffsetCb(const Eigen::Vector3d& rbm)
{
  geometry_msgs::Vector3 msg;
  msg.x = rbm.x();
  msg.y = rbm.y();
  msg.z = rbm.z();
  pub_comoffset_.publish(msg);
}

// ----------------------------------------------------------------------------
// Service Callback Handlers
// ----------------------------------------------------------------------------

bool MissionManagerROS::stateReqSrvCb(
          labdrone_msgs::SetHexSMStateReq::Request& req,
          labdrone_msgs::SetHexSMStateReq::Response& res)
{

  //
  // Unpack ROS data (if present)
  //

  // some states (i.e., FastTravel states) follow a pre-specified trajectory
  acl::polytraj::Trajectory traj = acl::polytraj_ros::deserialize_trajectory(req.traj);

  // some states require a pose of the object
  // being interacted with (e.g., nest, dock)
  labdrone::Pose pose;
  pose.p.x() = req.pose.position.x;
  pose.p.y() = req.pose.position.y;
  pose.p.z() = req.pose.position.z;
  pose.q.x() = req.pose.orientation.x;
  pose.q.y() = req.pose.orientation.y;
  pose.q.z() = req.pose.orientation.z;
  pose.q.w() = req.pose.orientation.w;

  //
  // Attempt a state change
  //

  auto err = mission_manager_->requestStateChange(req.state.state, traj, pose);

  // respond with error info (if any)
  res.error.error = err.num;
  res.error.msg = err.msg;
  return true;
}

// ----------------------------------------------------------------------------

bool MissionManagerROS::getBatterySrvCb(
          labdrone_msgs::GetBattery::Request& req,
          labdrone_msgs::GetBattery::Response& res)
{
  res.batteryPercent.data = 0.;
  res.batteryPercent.header.stamp = ros::Time::now();

  return true;
}

// ----------------------------------------------------------------------------

bool MissionManagerROS::getSMDataSrvCb(
          labdrone_msgs::GetHexSMData::Request& req,
          labdrone_msgs::GetHexSMData::Response& res)
{
  auto mm_data = mission_manager_->data();
  //auto state_data = mission_manager->getStateData();

//Need to convert
  // auto waypoints = mm_data.route.waypoints;
  // std::vector<geometry_msgs::Point> pathArray;
  // for (const auto& p : mm_data.route.waypoints) {
  //   geometry_msgs::Point point;
  //   point.x = p.x();
  //   point.y = p.y();
  //   point.z = p.z();
  //   pathArray.emplace_back(point);
  // }

  //unclear at the moment what the best method is for exposing this
  res.hexData.state.state = mission_manager_->getStateNum();
  // res.hexData.pathArray = pathArray;
  // res.hexData.pathIndex = mm_data.route.wp_idx;
  res.hexData.loaded = mm_data.loaded;
  res.hexData.position.x = mm_data.state.p.x();
  res.hexData.position.y = mm_data.state.p.y();
  res.hexData.position.z = mm_data.state.p.z();
  res.hexData.header.stamp = ros::Time::now();

  return true;
}

} // ns labdrone
} // ns creare
