/**
 * @file mission_manager_node.cpp
 * @brief Entry point for ROS Creare Labdrone Mission Manager
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#include <iostream>

#include <ros/ros.h>

#include "labdrone/mission_manager_ros.h"

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "mission_manager");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  creare::labdrone::MissionManagerROS node(nhtopics, nhparams);
  ros::spin();
  return 0;
}
