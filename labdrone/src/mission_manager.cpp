/**
 * @file mission_manager.cpp
 * @brief ROS wrapper for the Creare Labdrone Mission Manager
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 May 2020
 */

#include "labdrone/mission_manager.h"

#include "labdrone/states/all.h"

namespace creare {
namespace labdrone {

namespace sp = std::placeholders;

MissionManager::MissionManager(const Parameters& params)
: state_(new state::StartUp(this)), params_(params)
{
  state_map_ = {
    {state::Manual::SNUM, std::bind(state::createState<state::Manual>, this, sp::_1)},
    {state::StartUp::SNUM, std::bind(state::createState<state::StartUp>, this, sp::_1)},
    {state::Docked::SNUM, std::bind(state::createState<state::Docked>, this, sp::_1)},
    {state::DockTakeoff::SNUM, std::bind(state::createState<state::DockTakeoff>, this, sp::_1)},
    {state::WaitAtDock::SNUM, std::bind(state::createState<state::WaitAtDock>, this, sp::_1)},
    {state::GoToPickup::SNUM, std::bind(state::createState<state::GoToPickup>, this, sp::_1)},
    {state::Pickup::SNUM, std::bind(state::createState<state::Pickup>, this, sp::_1)},
    {state::GoToDropoff::SNUM, std::bind(state::createState<state::GoToDropoff>, this, sp::_1)},
    {state::WaitAtDropoff::SNUM, std::bind(state::createState<state::WaitAtDropoff>, this, sp::_1)},
    {state::Dropoff::SNUM, std::bind(state::createState<state::Dropoff>, this, sp::_1)},
    {state::GoToDock::SNUM, std::bind(state::createState<state::GoToDock>, this, sp::_1)},
    {state::DockLanding::SNUM, std::bind(state::createState<state::DockLanding>, this, sp::_1)},
    {state::Shutdown::SNUM, std::bind(state::createState<state::Shutdown>, this, sp::_1)},
    {state::ReroutePending::SNUM, std::bind(state::createState<state::ReroutePending>, this, sp::_1)},
    // {state::GCSCommFailure::SNUM, std::bind(state::createState<state::GCSCommFailure>, this, sp::_1)},
    // {state::ROSCommFailure::SNUM, std::bind(state::createState<state::ROSCommFailure>, this, sp::_1)},
    {state::EmergencyLanding::SNUM, std::bind(state::createState<state::EmergencyLanding>, this, sp::_1)},
    {state::ObstacleDetected::SNUM, std::bind(state::createState<state::ObstacleDetected>, this, sp::_1)},
  };
}

// ----------------------------------------------------------------------------

Goal MissionManager::tick(double now)
{
  const std::lock_guard<std::mutex> lock(mm_mutex_);
  // update predicates

  //
  // Tick the current state
  //

  state_->tick(now);

  //
  // Every state has an associated goal, even if identity
  //

  goal_ = state_->getGoal();

  //
  // Transition to a new state if available
  //

  // check if state requested a transition
  if (state_->next_state_ != nullptr) {
    // notify new state transition
    notifyTransition(state_->snum(), state_->sname(),
                    state_->next_state_->snum(), state_->next_state_->sname());

    state_ = state_->next_state_;
  }

  // make goal safe
  auto goal = goal_;
  makeGoalSafe(goal);


  return goal;
}

// ----------------------------------------------------------------------------

Error MissionManager::requestStateChange(
                  uint8_t snum, const acl::polytraj::Trajectory& traj, Pose pose)
{
  // will need to do some blocking on tick
  const std::lock_guard<std::mutex> lock(mm_mutex_);
  // find state by state num
  const auto& s = state_map_.find(snum);

  if (s == state_map_.end()) {
    Error err;
    err.num = Error::TRANSITION_DISALLOWED;
    err.msg = "Debug: could not find " + std::to_string(snum) + " in state map";
    return err;
  }

  // create requested next state with payload
  auto next = s->second({ .traj = traj, .pose = pose });

  auto result = state_->requestTransition(next);

  // indicate if request was valid & successful
  return result;
}

// ----------------------------------------------------------------------------

void MissionManager::getStateData()
{
  //  state_->getData();
}

uint8_t MissionManager::getStateNum()
{
  return state_->snum();
}
/*
bool MissionManager::getLoaded()
{
  return data_.loaded;
}
*/
// ----------------------------------------------------------------------------

void MissionManager::notify(const Notification& n) const
{
  switch (n) {
    case Notification::NewTrajectory:
      notifyTrajectory(data().traj);
      break;
    case Notification::WaypointReached:
      // std::cout << "Waypoint reached. Pursuing waypoint " << data().wp_idx << std::endl;
      break;
    default:
      break;
  }
}

void MissionManager::setLoaded(bool loaded)
{
  data_.loaded = loaded;
  return;
}

// ----------------------------------------------------------------------------

void MissionManager::registerNotifyTransitionCb(NotifyTransitionHandle cb)
{
  notifyTransition = cb;
}

// ----------------------------------------------------------------------------

void MissionManager::registerNotifyTrajectoryCb(NotifyTrajectoryHandle cb)
{
  notifyTrajectory = cb;
}

// ----------------------------------------------------------------------------

void MissionManager::registerNotifyObstructionCb(NotifyObstructionHandle cb)
{
  notifyObstruction = cb;
}

// ----------------------------------------------------------------------------

void MissionManager::registerActuateGripperCb(ActuateGripperHandle cb)
{
  actuateGripper = cb;
}

// ----------------------------------------------------------------------------

void MissionManager::registerUpdateCoMOffsetCb(UpdateCoMHandle cb)
{
  updateCoM = cb;
}


// ----------------------------------------------------------------------------
// Private Method
// ----------------------------------------------------------------------------

void MissionManager::makeGoalSafe(Goal& goal)
{
  // n.b.: this isn't particularly intelligent. It just instantaneously stops
  // the vehicle (i.e., infinite acceleration change). However, for
  // trajectories that barely stray outside of the bounds, this should be
  // sufficient. Besides, out of bounds waypoints should not be allowed.

  // for convenience
  const auto& x_min = params().bounds.x_min;
  const auto& x_max = params().bounds.x_max;
  const auto& y_min = params().bounds.y_min;
  const auto& y_max = params().bounds.y_max;
  const auto& z_min = params().bounds.z_min;
  const auto& z_max = params().bounds.z_max;

  bool unsafe_x = goal.p.x()<x_min || goal.p.x()>x_max;
  bool unsafe_y = goal.p.y()<y_min || goal.p.y()>y_max;
  bool unsafe_z = goal.p.z()<z_min || goal.p.z()>z_max;

  // clamp position of trajectory to inside edge of room
  goal.p.x() = utils::clamp(goal.p.x(), x_min, x_max);
  goal.p.y() = utils::clamp(goal.p.y(), y_min, y_max);
  goal.p.z() = utils::clamp(goal.p.z(), z_min, z_max);

  // get rid of higher-order motion in unsafe directions
  if (unsafe_x) goal.v.x() = goal.a.x() = goal.j.x() = 0;
  if (unsafe_y) goal.v.y() = goal.a.y() = goal.j.y() = 0;
  if (unsafe_z) goal.v.z() = goal.a.z() = goal.j.z() = 0;
}

} // ns labdrone
} // ns creare
