function subbag = windowBag(bag, ts, te)
%WINDOWBAG Returns bag data within some time window

subbag = struct;

fnames = fieldnames(bag);
for i = 1:length(fnames)
    fn = fnames{i};
    
    sidx = find(bag.(fn).t >= ts, 1);
    eidx = find(bag.(fn).t <= te, 1, 'last');
    
    subfnames = fieldnames(bag.(fn));
    for j = 1:length(subfnames)
        sfn = subfnames{j};
        subbag.(fn).(sfn) = bag.(fn).(sfn)(:, sidx:eidx);
    end
end

end

