clear, clc;

veh = 'labdrone01';
bagpath = '/media/plusk01/data/bags/creare/labdrone01/june/30-com-camp/2021-06-29-19-39-53.bag'; % w/o ramp
bag = readCreareBag(veh, bagpath);

bagpath = '/media/plusk01/data/bags/creare/labdrone01/june/30-com-camp/2021-06-29-18-31-14.bag'; % w/  ramp
bag2 = readCreareBag(veh, bagpath);

%% Main Plotting Code

% Add RPY representations
bag.world.RPY = flipud(quat2eul(bag.world.quaternion', 'ZYX')');
bag.state.RPY = flipud(quat2eul(bag.state.quat', 'ZYX')');

% Window the bag
ts = 65;
te = 90;
sbag = windowBag(bag, ts, te);

% Add RPY representations
bag2.world.RPY = flipud(quat2eul(bag2.world.quaternion', 'ZYX')');
bag2.state.RPY = flipud(quat2eul(bag2.state.quat', 'ZYX')');

% Window the bag
ts = 45.6;
te = 70.55;
sbag2 = windowBag(bag2, ts, te);

% in bag 1, gripper is closed at 69.073. In bag 2, it is at 49.667.
% Therefore, bag2 should be shifted by ~20 seconds for events to lineup.
tshift = 19.4060;

set(0,'DefaultLineLineWidth',2)


figure(1), clf;
grid on; hold on;
% plot(sbag.state.t, rad2deg(sbag.state.RPY(1,:)),'.')
plot(sbag.state.t, rad2deg(sbag.state.RPY(2,:)),'-')
% plot(sbag.state.t, sbag.state.RPY(3,:),'.')

plot(sbag2.state.t+tshift, rad2deg(sbag2.state.RPY(2,:)),'-')

plotEventLine(69.073,'k');
plotEventLine(72.37,'r');
plotEventLine(74.162,'g');

legend('Pitch w/o CoM ramp', 'Pitch w/ CoM ramp',...
    'Gripper closed, ramp start','Full load on gripper','CoM ramp finished')
ylabel('RPY [deg]');
xlabel('Time [s]');

title('Hex pitch during pickup, CoM compensation');

linkaxes(findall(0,'type','axes'),'x');
% xlim([ts te])

set(gca,'fontsize',18)

%% Helper Functions

function plotEventLine(ts,c)

for i = 1:size(ts,1)
    t = ts(i,:);
    xline(t, c, 'Alpha', 0.6, 'LineWidth',1.5);
end

end