clear, clc;

veh = 'labdrone01';
bagpath = '/media/plusk01/data/bags/creare/labdrone01/may/12-mission-pickup/2021-05-12-18-18-16.bag';
bag = readCreareBag(veh, bagpath);

%% Main Plotting Code

% Add RPY representations
bag.world.RPY = flipud(quat2eul(bag.world.quaternion', 'ZYX')');
bag.state.RPY = flipud(quat2eul(bag.state.quat', 'ZYX')');

global commoutages;
commoutages = [30.001 30.045;
               30.292 30.350;
               30.532 30.561];

% Window the bag
ts = 26;
te = 35;
sbag = windowBag(bag, ts, te);

set(0,'DefaultLineLineWidth',2)


figure(1), clf;
subplot(321); grid on; hold on;
title('VICON');
plot(sbag.world.t, sbag.world.position(1,:),'.')
plot(sbag.world.t, sbag.world.position(2,:),'.')
plot(sbag.world.t, sbag.world.position(3,:),'.')
plotCommOutageLines;
legend('x','y','z');
ylabel('Position');
subplot(323); grid on; hold on;
plot(sbag.world.t, sbag.world.quaternion(1,:),'.')
plot(sbag.world.t, sbag.world.quaternion(2,:),'.')
plot(sbag.world.t, sbag.world.quaternion(3,:),'.')
plot(sbag.world.t, sbag.world.quaternion(4,:),'.')
plotCommOutageLines;
legend('w', 'x','y','z');
ylabel('Quaternion');
subplot(325); grid on; hold on;
plot(sbag.world.t, sbag.world.RPY(1,:),'.')
plot(sbag.world.t, sbag.world.RPY(2,:),'.')
plot(sbag.world.t, sbag.world.RPY(3,:),'.')
plotCommOutageLines;
legend('R','P','Y');
ylabel('RPY');
% figure(1), clf;
subplot(322); grid on; hold on;
title('State');
plot(sbag.state.t, sbag.state.pos(1,:),'.')
plot(sbag.state.t, sbag.state.pos(2,:),'.')
plot(sbag.state.t, sbag.state.pos(3,:),'.')
plotCommOutageLines;
legend('x','y','z');
ylabel('Position');
subplot(324); grid on; hold on;
plot(sbag.state.t, sbag.state.quat(1,:),'.')
plot(sbag.state.t, sbag.state.quat(2,:),'.')
plot(sbag.state.t, sbag.state.quat(3,:),'.')
plot(sbag.state.t, sbag.state.quat(4,:),'.')
plotCommOutageLines;
legend('w', 'x','y','z');
ylabel('Quaternion');
subplot(326); grid on; hold on;
plot(sbag.state.t, sbag.state.RPY(1,:),'.')
plot(sbag.state.t, sbag.state.RPY(2,:),'.')
plot(sbag.state.t, sbag.state.RPY(3,:),'.')
plotCommOutageLines;
legend('R','P','Y');
ylabel('RPY');


figure(2), clf;
subplot(311); grid on; hold on;
title('State vs VICON');
plot(sbag.world.t, sbag.world.position(1,:),'.')
plot(sbag.world.t, sbag.world.position(2,:),'.')
plot(sbag.world.t, sbag.world.position(3,:),'.')
plot(sbag.state.t, sbag.state.pos(1,:),'.')
plot(sbag.state.t, sbag.state.pos(2,:),'.')
plot(sbag.state.t, sbag.state.pos(3,:),'.')
plotCommOutageLines;
legend('wx','wy','wz','sx','sy','sz');
ylabel('Position');
subplot(312); grid on; hold on;
plot(sbag.world.t, sbag.world.quaternion(1,:),'.')
plot(sbag.world.t, sbag.world.quaternion(2,:),'.')
plot(sbag.world.t, sbag.world.quaternion(3,:),'.')
plot(sbag.world.t, sbag.world.quaternion(4,:),'.')
plot(sbag.state.t, sbag.state.quat(1,:),'.')
plot(sbag.state.t, sbag.state.quat(2,:),'.')
plot(sbag.state.t, sbag.state.quat(3,:),'.')
plot(sbag.state.t, sbag.state.quat(4,:),'.')
plotCommOutageLines;
legend('ww','wx','wy','wz','sw','sx','sy','sz');
ylabel('Quaternion');
subplot(313); grid on; hold on;
plot(sbag.world.t, sbag.world.RPY(1,:),'.')
plot(sbag.world.t, sbag.world.RPY(2,:),'.')
plot(sbag.world.t, sbag.world.RPY(3,:),'.')
plot(sbag.state.t, sbag.state.RPY(1,:),'.')
plot(sbag.state.t, sbag.state.RPY(2,:),'.')
plot(sbag.state.t, sbag.state.RPY(3,:),'.')
plotCommOutageLines;
legend('wR','wP','wY','sR','sP','sY');
ylabel('RPY');


figure(3), clf;
subplot(211); grid on; hold on;
title('State vs VICON (twist)');
plot(sbag.world.t, sbag.twist.linear(1,:),'-.')
plot(sbag.world.t, sbag.twist.linear(2,:),'-.')
plot(sbag.world.t, sbag.twist.linear(3,:),'-.')
plot(sbag.state.t, sbag.state.vel(1,:),'-.')
plot(sbag.state.t, sbag.state.vel(2,:),'-.')
plot(sbag.state.t, sbag.state.vel(3,:),'-.')
plotCommOutageLines;
legend('wx','wy','wz','sx','sy','sz');
ylabel('Linear');
subplot(212); grid on; hold on;
plot(sbag.world.t, sbag.twist.angular(1,:),'-.')
plot(sbag.world.t, sbag.twist.angular(2,:),'-.')
plot(sbag.world.t, sbag.twist.angular(3,:),'-.')
plot(sbag.state.t, sbag.state.omega(1,:),'-.')
plot(sbag.state.t, sbag.state.omega(2,:),'-.')
plot(sbag.state.t, sbag.state.omega(3,:),'-.')
plotCommOutageLines;
legend('wx','wy','wz','sx','sy','sz');
ylabel('Angular');


linkaxes(findall(0,'type','axes'),'x');

%% Helper Functions

function plotCommOutageLines
global commoutages;

for i = 1:size(commoutages,1)
    co = commoutages(i,:);
    xline(co(1), 'r', 'Alpha', 0.2, 'LineWidth',1.5);
    xline(co(2), 'r', 'Alpha', 0.2, 'LineWidth',1.5);
end

end