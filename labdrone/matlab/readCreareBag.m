function data = readACLBag(veh, bagpath)
%READBAG Extracts messages into matrices without using custom msg defn

t = @(topic) ['/' veh '/' topic];

bag = rosbag(bagpath);

% Get IMU messages
bagsel = select(bag, 'Topic', t('imu'));
msgs = readMessages(bagsel,'DataFormat','struct'); imuMsg = [msgs{:}];
tsec = header([imuMsg.Header]);
accel = Vector3Message([imuMsg.Accel]);
gyro = Vector3Message([imuMsg.Gyro]);
imu = struct('t',bagsel.MessageList.Time','tt',tsec,'accel',accel,...
             'gyro',gyro);

% Get Motor messages
bagsel = select(bag, 'Topic', t('motors'));
msgs = readMessages(bagsel,'DataFormat','struct'); motorMsg = [msgs{:}];
tsec = header([motorMsg.Header]);
M1 = [motorMsg.M1]; M2 = [motorMsg.M2]; M3 = [motorMsg.M3];
M4 = [motorMsg.M4]; M5 = [motorMsg.M5]; M6 = [motorMsg.M6];
M7 = [motorMsg.M7]; M8 = [motorMsg.M8];
motors = [M1;M2;M3;M4;M5;M6;M7;M8];
motors = struct('t',bagsel.MessageList.Time','tt',tsec,'motors',motors);

% Get mocap messages
bagsel = select(bag, 'Topic', t('world'));
msgs = readMessages(bagsel,'DataFormat','struct');
poseStampedMsg = [msgs{:}]; poseMsg = [poseStampedMsg.Pose];
tsec = header([poseStampedMsg.Header]);
[pos, quat] = PoseMessage(poseMsg);
world = struct('t',bagsel.MessageList.Time','tt',tsec,'position',pos,...
               'quaternion',quat);

% Get mocap twist messages
bagsel = select(bag, 'Topic', t('mocap/twist'));
msgs = readMessages(bagsel,'DataFormat','struct');
twistStampedMsg = [msgs{:}]; twistMsg = [twistStampedMsg.Twist];
tsec = header([twistStampedMsg.Header]);
[linvel, angvel] = TwistMessage(twistMsg);
twist = struct('t',bagsel.MessageList.Time','tt',tsec,'linear',linvel,...
               'angular',angvel);

% Get state messages
bagsel = select(bag, 'Topic', t('state'));
msgs = readMessages(bagsel,'DataFormat','struct'); stateMsg=[msgs{:}];
tsec = header([stateMsg.Header]); tstate = statestamp(stateMsg);
pos = Vector3Message([stateMsg.Pos]);
vel = Vector3Message([stateMsg.Vel]);
quat = QuaternionMessage([stateMsg.Quat]);
omega = Vector3Message([stateMsg.W]);
state = struct('t',bagsel.MessageList.Time','tt',tsec,'tstate',tstate,...
               'pos',pos,'vel',vel,'quat',quat,'omega',omega);

% Get goal messages
bagsel = select(bag, 'Topic', t('goal'));
msgs = readMessages(bagsel,'DataFormat','struct'); goalMsg=[msgs{:}];
tsec = header([goalMsg.Header]);
p = Vector3Message([goalMsg.P]);
v = Vector3Message([goalMsg.V]);
a = Vector3Message([goalMsg.A]);
j = Vector3Message([goalMsg.J]);
yaw = [goalMsg.Psi];
dyaw = [goalMsg.Dpsi];
power = [goalMsg.Power];
goal = struct('t',bagsel.MessageList.Time','tt',tsec,'p',p,'v',v,'a',a,...
               'j',j,'yaw',yaw,'dyaw',dyaw,'power',power);

% Get log messages
bagsel = select(bag, 'Topic', t('log'));
msgs = readMessages(bagsel,'DataFormat','struct'); logMsg=[msgs{:}];
tsec = header([logMsg.Header]);
p = Vector3Message([logMsg.P]);
pref = Vector3Message([logMsg.PRef]);
perr = Vector3Message([logMsg.PErr]);
perrint = Vector3Message([logMsg.PErrInt]);
v = Vector3Message([logMsg.V]);
vref = Vector3Message([logMsg.VRef]);
verr = Vector3Message([logMsg.VErr]);
aff = Vector3Message([logMsg.AFf]);
afb = Vector3Message([logMsg.AFb]);
jff = Vector3Message([logMsg.JFf]);
jfb = Vector3Message([logMsg.JFb]);
q = QuaternionMessage([logMsg.Q]);
qref = QuaternionMessage([logMsg.QRef]);
rpy = Vector3Message([logMsg.Rpy]);
rpyref = Vector3Message([logMsg.RpyRef]);
w = Vector3Message([logMsg.W]);
wref = Vector3Message([logMsg.WRef]);
Fw = Vector3Message([logMsg.FW]);
% thrust = [logMsg.Thrust];
% throttle = [logMsg.Throttle];
power = [logMsg.Power];
log = struct('t',bagsel.MessageList.Time','tt',tsec,'p',p,'pref',pref,...
             'perr',perr,'perrint',perrint,...
             'v',v,'vref',vref,'verr',verr,'aff',aff,'afb',afb,...
             'jff',jff,'jfb',jfb,'q',q,'qref',qref,'rpy',rpy,'rpyref',rpyref,...
             'w',w,'wref',wref,'Fw',Fw,...
             'power',power);
           
% time sync
% mint = min([min(state.t), min(world.t), min(twist.t), min(imu.t), min(motors.t), min(goal.t), min(log.t)]);
mint = bag.StartTime;
state.t = state.t - mint;
world.t = world.t - mint;
twist.t = twist.t - mint;
imu.t = imu.t - mint;
motors.t = motors.t - mint;
goal.t = goal.t - mint;
log.t = log.t - mint;

% pack in struct
data.imu = imu;
data.motors = motors;
data.state = state;
data.world = world;
data.twist = twist;
data.goal = goal;
data.log = log;
end

function t = header(headerMsg)
stamp = [headerMsg.Stamp];
sec = [stamp.Sec];
nsec = [stamp.Nsec];
t = double(sec) + double(nsec)*1e-9;
end

function t = statestamp(stateMsg)
stamp = [stateMsg.StateStamp];
sec = [stamp.Sec];
nsec = [stamp.Nsec];
t = double(sec) + double(nsec)*1e-9;
end

function [pos, quat] = PoseMessage(poseMsg)
pos = Vector3Message([poseMsg.Position]);
quat = QuaternionMessage([poseMsg.Orientation]);
end

function [linvel, angvel] = TwistMessage(twistMsg)
linvel = Vector3Message([twistMsg.Linear]);
angvel = Vector3Message([twistMsg.Angular]);
end

function vec = Vector3Message(vectorMsg)
X = [vectorMsg.X]; Y = [vectorMsg.Y]; Z = [vectorMsg.Z];
vec = [X' Y' Z']';
end

function q = QuaternionMessage(quatMsg)
W = [quatMsg.W]; X = [quatMsg.X]; Y = [quatMsg.Y]; Z = [quatMsg.Z];
q = [W' X' Y' Z']';
end